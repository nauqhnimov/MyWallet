﻿namespace MyWallet.Enums
{
    public enum OutcomeCategory
    {
        Food,
        Travel,
        ElectricityBill,
        WaterBill,
        Gift,
        Shopping,
        StudyingFee,
        Party,
        Card,
        Rent,
        Loan,
        Relax,
        TravellingFee,
        Repayment,
        Medicine,
        HouseHold,
        Others,
    }
}
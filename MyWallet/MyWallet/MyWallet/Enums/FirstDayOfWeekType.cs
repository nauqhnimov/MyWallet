﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyWallet.Enums
{
    public enum FirstDayOfWeekType
    {
        Monday = 0,
        Saturday = 1,
        Sunday = 2,
    }
}

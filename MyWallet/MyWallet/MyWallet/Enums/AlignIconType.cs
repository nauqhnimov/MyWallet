﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyWallet.Enums
{
    public enum AlignIconType
    {
        /// <summary>
        /// icon left
        /// </summary>
        Left,

        /// <summary>
        /// icon right
        /// </summary>
        Right
    }
}

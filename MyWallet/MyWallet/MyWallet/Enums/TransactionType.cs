﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyWallet.Enums
{
    public enum TransactionType
    {
        None = 0,
        Income = 1,
        Outcome = 2,
    }
}

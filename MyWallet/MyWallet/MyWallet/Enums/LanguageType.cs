﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyWallet.Enums
{
    public enum LanguageType
    {
        Vietnamese = 0,
        English = 1,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyWallet.Enums
{
    public enum KeyParameters
    {
        Title,
        CategorySelected,
        TransactionType,
        ResultSearchFilter,
        Transactions,
        startday,
        endday,
        __NavigationMode,
        NextId,
        UpdateTransaction,
    }

    public enum PageMode
    {
        New,
        Edit,
        OnlyView,
        Delete,
    }
}

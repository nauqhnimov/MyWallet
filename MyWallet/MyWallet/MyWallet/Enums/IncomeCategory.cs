﻿namespace MyWallet.Enums
{
    public enum IncomeCategory
    {
        Salary,
        PartTimeJob,
        Due,
        Bonus,
        Business,
        Interest,
        Subsidy,
        RealEstate,
        Borrowing,
        Gift,
        Stock,
        Others,
    }
}
﻿using SQLite.Net;

namespace MyWallet.Interfaces.LocalDatabase
{
    public interface IDatabaseConnection
    {
        SQLiteConnection DbConnection(string databaseName);
        long GetSize(string databaseName);
    }
}

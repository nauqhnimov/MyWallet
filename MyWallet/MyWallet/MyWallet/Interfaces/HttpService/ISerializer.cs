﻿using System.IO;

namespace MyWallet.Interfaces.HttpService
{
    public interface ISerializer
    {
        T Deserialize<T>(string data, string url = null);

        T DeserializeFromJsonStream<T>(Stream jsonStream, string url = null);

        string Serialize<T>(T obj);
    }
}

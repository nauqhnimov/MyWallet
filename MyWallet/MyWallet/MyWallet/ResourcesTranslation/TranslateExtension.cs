﻿
using System;
using System.Globalization;
using System.Reflection;
using System.Resources;
using MyWallet.Enums;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.ResourcesTranslation
{
    [ContentProperty("Text")]
    public class TranslateExtension : IMarkupExtension
    {
        #region SourceName

        public const string ResourcePath = "MyWallet.ResourcesTranslation.";
        public const string ResourceTranslateVietnamese = "ResourceTranslateVietnamese";
        public const string ResourceTranslateEnglish = "ResourceTranslateEnglish";

        #endregion

        #region Properties

        private static int _language { get; set; }
        private static string _resourcePath { get; set; } = string.Empty;

        #endregion

        #region ResourceManager

        private static System.Resources.ResourceManager _resourceManager;

        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
        private static System.Resources.ResourceManager ResourceManager
        {
            get
            {
                if (!ReferenceEquals(_resourceManager, null) && _language == App.Settings.Language) return _resourceManager;

                switch (App.Settings.Language)
                {
                    case (int)LanguageType.Vietnamese:
                        _resourcePath = ResourcePath + ResourceTranslateVietnamese;
                        break;
                    case (int)LanguageType.English:
                        _resourcePath = ResourcePath + ResourceTranslateEnglish;
                        break;
                }

                _language = App.Settings.Language;


                var tempResourceManager =
                    new System.Resources.ResourceManager(
                        _resourcePath,
                        typeof(TranslateExtension).GetTypeInfo().Assembly);
                _resourceManager = tempResourceManager;

                return _resourceManager;
            }
        }

        #endregion

        #region Properties

        public string Text { get; set; }
        public bool AllCaps { get; set; }

        #endregion

        #region IMarkupExtension implementation

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Text == null)
                return null;

            //ResourceManager resourceManager =
            //    new ResourceManager(ResourcePath, typeof(TranslateExtension).GetTypeInfo().Assembly);

            var translation = ResourceManager.GetString(Text, CultureInfo.CurrentCulture) ?? Text;

            if (AllCaps)
                translation = translation.ToUpper();

            return translation;
        }

        public static string Get(string text, bool allCaps = false)
        {
            var translation = ResourceManager.GetString(text ?? "", CultureInfo.CurrentCulture) ?? (text ?? "");
            if (allCaps)
                translation = translation.ToUpper();
            return translation;
        }

        #endregion


    }
}

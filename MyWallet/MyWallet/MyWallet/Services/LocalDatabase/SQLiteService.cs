﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using MyWallet.Enums;
using MyWallet.Interfaces.LocalDatabase;
using MyWallet.Models;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace MyWallet.Services.LocalDatabase
{
    public class SQLiteService : ISQLiteService
    {
        #region Attributes

        const string DatabaseName = "mywallet";

        protected SQLiteConnection Database;

        protected static readonly object Locker = new object();

        public IDatabaseConnection DatabaseConnection { get; private set; }

        #endregion

        #region Constructors

        public SQLiteService(IDatabaseConnection databaseConnection)
        {
            DatabaseConnection = databaseConnection;
            Init(databaseConnection);
        }

        #endregion

        #region Inits

        private void Init(IDatabaseConnection databaseConnection)
        {
            if (Database != null) return;

            // Connect database
            Database = DatabaseConnection.DbConnection($"{DatabaseName}.db3");

            // Create database
            var listTable = new List<Type>
            {
                typeof(AppSettings),
                typeof(Transaction),
                typeof(TransactionTemp),
                //typeof(Setting),
                //typeof(Student),
                //typeof(Subject),
                //typeof(User),
                //typeof(Account)
            };

            foreach (var table in listTable)
            {
                CreateTable(table);
            }
        }

        #endregion

        #region Methods

        #region Create Database

        private void CreateTable<T>(T table) where T : Type
        {
            lock (Locker)
            {
                try
                {
                    Database.CreateTable(table);
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"CreateTable: {e}");
                }
            }
        }
        #endregion

        #region Gets

        /// <summary>
        /// Return data of multiple table.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="primaryKey"></param>
        /// <param name="isRecursive"></param>
        /// <returns></returns>
        public T GetWithChildren<T>(string primaryKey, bool isRecursive = false) where T : class, new()
        {
            lock (Locker)
            {
                try
                {
                    return Database.GetWithChildren<T>(primaryKey, isRecursive);
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"GetWithChildren - primaryKey: {e}");
                    return null;
                }
            }
        }

        public T Get<T>(string primarykey) where T : class, new()
        {
            lock (Locker)
            {
                try
                {
                    return Database.Get<T>(primarykey);
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"Get - primaryKey: {e}");
                    return null;
                }
            }
        }

        public T Get<T>(Expression<Func<T, bool>> predicate) where T : class, new()
        {
            try
            {
                lock (Locker)
                {
                    return Database.Table<T>().Where(predicate).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Get - Expression: {e}");
                return null;
            }
        }


        public IEnumerable<T> GetList<T>(Expression<Func<T, bool>> predicate) where T : class, new()
        {
            lock (Locker)
            {
                try
                {
                    return Database.Table<T>().Where(predicate).ToList();
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"Get - Expression: {e}");
                    return null;
                }
            }
        }

        #endregion

        #region Inserts

        public int Insert<T>(T obj)
        {
            lock (Locker)
            {
                try
                {
                    return Database.InsertOrReplace(obj);
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"SQLiteHelper - Insert: {e}");
                    return -1;
                }
            }
        }

        public int InsertAll<T>(IEnumerable<T> list)
        {
            lock (Locker)
            {
                try
                {
                    return Database.InsertOrReplaceAll(list);
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"SQLiteHelper - InsertAll: {e}");
                    return -1;
                }
            }
        }

        public void InsertWithChildren<T>(T obj, bool isRecursive = false)
        {
            lock (Locker)
            {
                try
                {
                    Database.InsertWithChildren(obj, isRecursive);
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"SQLiteHelper - InsertWithChildren: {e}");
                }
            }
        }

        #endregion

        #region Updates

        public int Update<T>(T obj)
        {
            lock (Locker)
            {
                try
                {
                    return Database.Update(obj);
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"SQLiteHelper - Update: {e}");
                    return -1;
                }
            }
        }

        public void UpdateWithChildren<T>(T obj)
        {
            lock (Locker)
            {
                try
                {
                    Database.UpdateWithChildren(obj);
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"SQLiteHelper - UpdateWithChildren {e}");
                }
            }
        }

        #endregion

        #region Deletes

        public int Delete<T>(string id)
        {
            lock (Locker)
            {
                try
                {
                    return Database.Delete<T>(id);
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"SQLiteHelper - Delete: {e}");
                    return -1;
                }
            }
        }

        public void Delete<T>(T obj, bool isRecursive = false)
        {
            lock (Locker)
            {
                try
                {
                    Database.Delete(obj, isRecursive);
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"SQLiteHelper - void Delete: {e}");
                }
            }
        }

        public int DeleteAll<T>()
        {
            lock (Locker)
            {
                try
                {
                    return Database.DeleteAll<T>();
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"SQLiteHelper - DeleteAll: {e}");
                    return -1;
                }
            }
        }

        public void DeleteAll<T>(IEnumerable<T> obj)
        {
            lock (Locker)
            {
                try
                {
                    Database.DeleteAll(obj);
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"SQLiteHelper - DeleteAll: {e}");
                }
            }
        }

        #endregion

        #endregion

        #region App Settings

        public AppSettings GetSettings()
        {
            lock (Locker)
            {
                try
                {
                    var appSettings = Database.Get<AppSettings>(a => a.Id == 0);

                    if (appSettings == null)
                    {
                        appSettings = new AppSettings
                        {
                            Id = 0, // set cứng Id = 0
                            Language = (int) LanguageType.Vietnamese,
                            FormatDate = "dd/MM/yyyy",
                            FirstOfWeek = FirstDayOfWeekType.Monday.ToString(),
                        };

                        Database.Insert(appSettings);
                    }

                    return appSettings;
                }
                catch (Exception e)
                {
#if DEBUG
                    Debug.WriteLine($"Get Setting Error: {e}");
#endif
                    var appSettings = new AppSettings
                    {
                        Id = 0, // set cứng Id = 0
                        Language = (int) LanguageType.Vietnamese,
                        FormatDate = "dd/MM/yyyy",
                        FirstOfWeek = FirstDayOfWeekType.Monday.ToString(),
                    };

                    Database.Insert(appSettings);

                    return appSettings;
                }
            }
        }

        #endregion
        
    }
}

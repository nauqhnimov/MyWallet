﻿using Newtonsoft.Json;

namespace MyWallet.Services.FacebookApiService
{
    public class FacebookProfileModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("picture")]
        public FacebookPictureModel Picture { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }
    }

    public class FacebookPictureModel
    {
        [JsonProperty("data")]
        public FacebookPictureDataModel Data { get; set; }
    }

    public class FacebookPictureDataModel
    {
        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("is_silhouette")]
        public bool IsSilhouette { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}

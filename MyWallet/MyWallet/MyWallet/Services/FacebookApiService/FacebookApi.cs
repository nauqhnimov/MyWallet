﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using MyWallet.Managers;
using MyWallet.ResourcesTranslation;
using MyWallet.Views.Popups;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace MyWallet.Services.FacebookApiService
{
    public class FacebookApi
    {
        private string _appId = "2016845645274089";
        private string _clientSecret = "cac0902fa8e96589f2d7448df2543206";
        private string _redirectUri = "https://www.facebook.com/connect/login_success.html";
        private readonly string _apiRequestUrl;

        private static FacebookApi _instance;
        public static FacebookApi Instance => _instance ?? (_instance = new FacebookApi());

        private WebView _webView;

        public FacebookApi()
        {
            //_apiRequestUrl =
            //    $"https://graph.facebook.com/oauth/access_token?client_id={_appId}&client_secret={_clientSecret}&redirect_uri={_redirectUri}&grant_type=client_credentials";

            _apiRequestUrl =
                $"https://www.facebook.com/dialog/oauth?client_id={_appId}&grant_type=client_credentials&display=popup&response_type=token&redirect_uri={_redirectUri}";
            //auth_type = reauthenticate
        }

        public async Task<WebView> GetFacebookAuthDialog()
        {
            await LoadingPopup.Instance.Show();
            _webView = new WebView { Source = _apiRequestUrl, IsVisible = false };
            _webView.Navigated += WebViewOnNavigated;

            return _webView;
        }

        private async void WebViewOnNavigated(object sender, WebNavigatedEventArgs e)
        {
            var accessToken = ExtractAccessTokenFromUrl(e.Url);

            //_webView.Navigated -= WebViewOnNavigated;

            if (accessToken == "")
            {
                // Nếu chưa đăng nhập ==> Hiện webview đăng nhập Facebook
                _webView.IsVisible = true;
                await LoadingPopup.Instance.Hide();
            }
            else
            {
                // Nếu đã đăng nhập ==> Ẩn webview đăng nhập Facebook
                _webView.IsVisible = false;
                await LoadingPopup.Instance.Show();

                var profile = new FacebookProfileModel();
                await Task.Run(async () =>
                {
                    profile = await GetFacebookProfileAsync(accessToken);
                });

                if (profile == null)
                {
                    await LoadingPopup.Instance.Hide();
                    //await MessagePopup.Instance.Show(TranslateExtension.Get("NetworkError"));
                    return;
                }

                var vm = PageManager.GetCurrentPageBaseViewModel();
                await LoadingPopup.Instance.Hide();
                await vm.OnFacebookLoginCallBack(profile);
            }
        }

        private string ExtractAccessTokenFromUrl(string url)
        {
            if (!url.Contains("access_token") || !url.Contains("&expires_in=")) return string.Empty;
            var accTokenWithExpires = url.Replace($"{_redirectUri}#access_token=", "");
            var accToken = accTokenWithExpires.Remove(accTokenWithExpires.IndexOf("&expires_in=", StringComparison.Ordinal));
            return accToken;

            //if (url.Contains("access_token") && url.Contains("&expires_in="))
            //{
            //    var at = url.Replace("https://www.facebook.com/connect/login_success.html#access_token=", "");

            //    if (Device.OS == TargetPlatform.WinPhone || Device.OS == TargetPlatform.Windows)
            //    {
            //        at = url.Replace("http://www.facebook.com/connect/login_success.html#access_token=", "");
            //    }

            //    var accessToken = at.Remove(at.IndexOf("&expires_in="));

            //    return accessToken;
            //}

            //return string.Empty;
        }

        private async Task<FacebookProfileModel> GetFacebookProfileAsync(string accessToken)
        {
            var requestUrl =
                "https://graph.facebook.com/v2.7/me/"
                + "?fields=first_name,last_name,picture.width(800).height(800),gender"
                + "&access_token=" + accessToken;

            try
            {
                var httpClient = new HttpClient();
                var userJson = await httpClient.GetStringAsync(requestUrl);
                var userProfile = JsonConvert.DeserializeObject<FacebookProfileModel>(userJson);
                return userProfile;
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine("[Login Error]: Login via Facebook Error: " + e);
#endif
                return null;
            }
        }
    }
}

﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace MyWallet.Services.GoogleApiService
{
    public class GoogleApi
    {
        private string _clientId = "19063112904-qp1o174ur3qsop6gp2oku474ctou64e3.apps.googleusercontent.com";
        private string _clientSecret = "XsCiV3tNBIKtgd80OPokQAa2";
        private string _redirectUri = "https://google.com/";
        private readonly string _apiRequestUrl;

        private static GoogleApi _instance;
        public static GoogleApi Instance => _instance ?? (_instance = new GoogleApi());

        private WebView _webView;

        public GoogleApi()
        {
            _apiRequestUrl =
                $"https://accounts.google.com/o/oauth2/v2/auth?response_type=code&scope=openid&redirect_uri={_redirectUri}&client_id={_clientId}";

        }

        public WebView GetGooglePlusAuthDialog()
        {
            //await LoadingPopup.Instance.Show();
            //_webView = new WebView { Source = _apiRequestUrl, IsVisible = false };
            _webView = new WebView { Source = _apiRequestUrl };
            _webView.Navigated += WebViewOnNavigated;

            return _webView;
        }

        private async void WebViewOnNavigated(object sender, WebNavigatedEventArgs e)
        {
            var code = ExtractCodeFromUrl(e.Url);

            if (code != "")
            {
                string accessToken = "";
                var profile = new GoogleProfileModel();

                await Task.Run(async () =>
                {
                    accessToken = await GetAccessTokenAsync(code);
                });

                if (string.IsNullOrEmpty(accessToken))
                {
                    // Đăng nhập gián đoạn
                    return;
                }

                await Task.Run(async () =>
                {
                    profile = await GetGoogleProfileAsync(accessToken);
                });
            }


            //{
            //    // Nếu chưa đăng nhập ==> Hiện webview đăng nhập Google+
            //    _webView.IsVisible = true;
            //    await LoadingPopup.Instance.Hide();
            //}
            //else
            //{
            //    // Nếu đã đăng nhập ==> Ẩn webview đăng nhập Google+
            //    _webView.IsVisible = false;
            //    await LoadingPopup.Instance.Show();

            //    var profile = new FacebookProfileModel();
            //    await Task.Run(async () =>
            //    {
            //        profile = await GetGoogleProfileAsync(code);
            //    });

            //    if (profile == null)
            //    {
            //        await LoadingPopup.Instance.Hide();
            //        await MessagePopup.Instance.Show(TranslateExtension.Get("NetworkError"));
            //        return;
            //    }

            //    var vm = PageManager.GetCurrentPageBaseViewModel();
            //    await LoadingPopup.Instance.Hide();
            //    await vm.OnFacebookLoginCallBack(profile);
            //}
        }

        private string ExtractCodeFromUrl(string url)
        {
            if (url.Contains("code="))
            {
                var attributes = url.Split('&');
                var code = attributes.FirstOrDefault(s => s.Contains("code="))?.Split('=')[1];
                return code;
            }
            return string.Empty;
        }

        private async Task<string> GetAccessTokenAsync(string code)
        {
            var requestUrl = "https://www.googleapis.com/oauth2/v4/token"
                             + "?code=" + code
                             + "&client_id=" + _clientId
                             + "&client_secret=" + _clientSecret
                             + "&redirect_uri=" + _redirectUri;
            try
            {
                var httpClient = new HttpClient();
                var response = await httpClient.PostAsync(requestUrl, null);
                var json = await response.Content.ReadAsStringAsync();
                var accessToken = JsonConvert.DeserializeObject<JObject>(json).Value<string>("access_token");
                return accessToken;
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine("[Login Error]: Login via Google+ Error: " + e);
#endif
                return "";
            }

        }

        private async Task<GoogleProfileModel> GetGoogleProfileAsync(string accessToken)
        {
            var requestUrl = $"https://www.googleapis.com/plus/v1/people/me?access_token={accessToken}";

            try
            {
                var httpClient = new HttpClient();
                var userJson = await httpClient.GetStringAsync(requestUrl);
                var userProfile = JsonConvert.DeserializeObject<GoogleProfileModel>(userJson);
                return userProfile;
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine("[Login Error]: Login via Facebook Error: " + e);
#endif
                return null;
            }
        }
    }
}

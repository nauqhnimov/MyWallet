﻿namespace MyWallet.Services.HttpService
{
    public static class ApiUrl
    {
        private const string HttpUrl = "https://mywalletfinal.azurewebsites.net/";

        private static string LinkApi(string link)
        {
            return $"{HttpUrl}api{link}";
        }

        #region API URL

        public static string UserSignup()
        {
            return LinkApi("/SignUp");
        }

        public static string UserLogin()
        {
            return LinkApi("/Login");
        }

        public static string UserLogout()
        {
            return LinkApi("/session/logout");
        }

        public static string PostTrans()
        {
            return LinkApi("/PostTrans");
        }

        public static string GetListTrans(string userName)
        {
            return LinkApi("/Transaction/" + userName);
        }

        #endregion
    }
}

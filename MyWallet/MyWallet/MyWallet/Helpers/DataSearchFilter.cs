﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using MyWallet.Enums;
using MyWallet.Managers;
using MyWallet.ResourcesTranslation;

namespace MyWallet.Helpers
{
    public class DataSearchFilter
    {
        public static ObservableCollection<string> GetAmountsFilter()
        {
            return new ObservableCollection<string>()
            {
                TranslateExtension.Get(TranslateManager.All),
                TranslateExtension.Get(TranslateManager.Over),
                TranslateExtension.Get(TranslateManager.Under),
                TranslateExtension.Get(TranslateManager.Between),
                TranslateExtension.Get(TranslateManager.Exact),
            };
        }

        public static ObservableCollection<string> GetTypeTransactionFilter()
        {
            return new ObservableCollection<string>()
            {
                TranslateExtension.Get(TranslateManager.All),
                TranslateExtension.Get(TranslateManager.AllExpense),
                TranslateExtension.Get(TranslateManager.AllIncome),
                TranslateExtension.Get(TranslateManager.CustomCategory),
            };
        }

        public static ObservableCollection<string> GetDatesFilter()
        {
            return new ObservableCollection<string>()
            {
                TranslateExtension.Get(TranslateManager.All),
                TranslateExtension.Get(TranslateManager.Before),
                TranslateExtension.Get(TranslateManager.After),
                TranslateExtension.Get(TranslateManager.Exact),
                TranslateExtension.Get(TranslateManager.Between),
            };
        }

        public static ObservableCollection<string> GetAllCategories()
        {
            return new ObservableCollection<string>()
            {
                TranslateExtension.Get(TranslateManager.All),
                TranslateExtension.Get(TranslateManager.AllExpense),
                TranslateExtension.Get(TranslateManager.AllIncome),
                TranslateExtension.Get(TranslateManager.CustomCategory),
            };
        }

        public static ObservableCollection<string> GetIncomeCategories()
        {
            return new ObservableCollection<string>(Enum.GetNames(typeof(IncomeCategory)));

            return new ObservableCollection<string>()
            {
                TranslateExtension.Get(TranslateManager.Salary),
                TranslateExtension.Get(TranslateManager.PartTimeJob),
                TranslateExtension.Get(TranslateManager.Due),
                TranslateExtension.Get(TranslateManager.Bonus),
                TranslateExtension.Get(TranslateManager.Business),
                TranslateExtension.Get(TranslateManager.Interest),
                TranslateExtension.Get(TranslateManager.Subsidy),
                TranslateExtension.Get(TranslateManager.RealEstate),
                TranslateExtension.Get(TranslateManager.Borrowing),
                TranslateExtension.Get(TranslateManager.Gift),
                TranslateExtension.Get(TranslateManager.Stock),
                TranslateExtension.Get(TranslateManager.Other),
            };
        }

        public static ObservableCollection<string> GetOutcomeCategories()
        {
            return new ObservableCollection<string>(Enum.GetNames(typeof(OutcomeCategory)));

            return new ObservableCollection<string>()
            {
                TranslateExtension.Get(TranslateManager.Food),
                TranslateExtension.Get(TranslateManager.Travel),
                TranslateExtension.Get(TranslateManager.ElectricityWaterBill),
                TranslateExtension.Get(TranslateManager.Gift),
                TranslateExtension.Get(TranslateManager.Shopping),
                TranslateExtension.Get(TranslateManager.StudyingFee),
                TranslateExtension.Get(TranslateManager.Party),
                TranslateExtension.Get(TranslateManager.Card),
                TranslateExtension.Get(TranslateManager.Loan),
                TranslateExtension.Get(TranslateManager.Relax),
                TranslateExtension.Get(TranslateManager.TravellingFee),
                TranslateExtension.Get(TranslateManager.Repayment),
                TranslateExtension.Get(TranslateManager.Medicine),
                TranslateExtension.Get(TranslateManager.HouseHold),
                TranslateExtension.Get(TranslateManager.Other),
            };
        }
    }
}

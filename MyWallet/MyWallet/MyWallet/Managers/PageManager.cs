﻿using System;
using MyWallet.ViewModels;
using System.Diagnostics;
using System.Linq;
using Xamarin.Forms;

namespace MyWallet.Managers
{
    public class PageManager
    {
        public static readonly string TestPage = "TestPage";

        public const string NavigationHomeUri = "https://quanvm.com/";
        public static readonly string NavigationPage = "NavigationPage";

        // Master Detail Page
        public static readonly string MenuPage = "MenuPage";
        public static readonly string MainPage = "MainPage";

        // Login Flow
        public static readonly string LoginPage = "LoginPage";
        public static readonly string SignUpPage = "SignUpPage";

        // Settings Page
        public static readonly string SettingsPage = "SettingsPage";

        //Transactionflow
        public static readonly string AddTransactionPage = "AddTransactionPage";
        public static readonly string AddNewIncomePage = "AddNewIncomePage";
        public static readonly string AddNewOutcomePage = "AddNewOutcomePage";
        public static readonly string NewTransactionPage = "NewTransactionPage";
        public static readonly string DetailTransactionPage = "DetailTransactionPage";

        //Statistics flow
        public static readonly string OutcomeStatisticsPage = "OutcomeStatisticsPage";
        public static readonly string IncomeStatisticsPage = "IncomeStatisticsPage";
        public static readonly string GeneralStatisticsPage = "GeneralStatisticsPage";
        public static readonly string StatisticsPage = "StatisticsPage";
        public static readonly string ChooseDatePage = "ChooseDatePage";

        //Search Filter Flow
        public static readonly string SearchFilterPage = "SearchFilterPage";

        public static readonly string ViewSearchFilterPage = "ViewSearchFilterPage";

        //Common Pages
        public static readonly string CategoriesTabbedPage = "CategoriesTabbedPage";
        public static readonly string CategoriesIncomePage = "CategoriesIncomePage";
        public static readonly string CategoriesOutcomePage = "CategoriesOutcomePage";




        // Get Main URI
        public static Uri MainPageUri()
        {
            return new Uri($"{NavigationHomeUri}{MenuPage}/{NavigationPage}/{MainPage}");
        }

        public static string MultiplePage(string[] pages)
        {
            var path = "";
            if (pages == null) return "";
            if (pages.Length < 1) return "";
            for (var i = 0; i < pages.Length; i++)
            {
                path += i == 0 ? pages[i] : "/" + pages[i];
            }
            return path;
        }

        public static Page GetCurrentPage()
        {
            return Application.Current.MainPage;
        }

        public static Page GetCurrentPage(bool withModal)
        {
            if (!withModal) return GetCurrentPage();
            try
            {
                var navPage = Application.Current.MainPage;
                var modalPage = navPage.Navigation.ModalStack.LastOrDefault();
                var foundedPage = modalPage ?? navPage;
                return foundedPage;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return null;
            }
        }

        public static BaseViewModel GetCurrentPageBaseViewModel()
        {
            return (BaseViewModel)GetCurrentPage(true).BindingContext;
        }
    }
}

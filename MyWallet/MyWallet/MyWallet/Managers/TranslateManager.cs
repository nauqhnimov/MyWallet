﻿namespace MyWallet.Managers
{
    public class TranslateManager
    {
        public static string AppName { get; } = "AppName";

        #region Login

        public static string EnterPassword { get; } = "EnterPassword";
        public static string EnterUserName { get; } = "EnterUserName";
        public static string Facebook { get; } = "Facebook";
        public static string ForgotPassword { get; } = "ForgotPassword";
        public static string Google { get; } = "Google";
        public static string Login { get; } = "Login";
        public static string OrLoginWith { get; } = "OrLoginWith";
        public static string Skip { get; } = "Skip";


        public static string Again { get; } = "Again";
        public static string Incorrect { get; } = "Incorrect";
        public static string IncorrectAndAgain { get; } = "IncorrectAndAgain";

        public static string PleaseEnterUsernameAndPassword { get; } = "PleaseEnterUsernameAndPassword";
        public static string PleaseEnterUserName { get; } = "PleaseEnterUserName";
        public static string PleaseEnterPassword { get; } = "PleaseEnterPassword";

        #endregion

        #region SignUp

        public static string SignUp { get; } = "SignUp";
        public static string EnterFullName { get; } = "EnterFullName";
        public static string AlreadyAccount { get; } = "AlreadyAccount";
        public static string PleaseEnterAllInfo { get; } = "PleaseEnterAllInfo";
        public static string PleaseEnterFullName { get; } = "PleaseEnterFullName";
        public static string CheckInternet { get; } = "CheckInternet";

        #endregion

        #region Settings Page

        public static string SelectLanguage { get; } = "SelectLanguage";
        public static string Display { get; } = "Display";
        public static string SelectDateFormat { get; } = "SelectDateFormat";
        public static string SelectFirstDayOfWeek { get; } = "SelectFirstDayOfWeek";
        public static string About { get; } = "About";
        public static string Directions { get; } = "Directions";
        public static string AboutMywallet { get; } = "AboutMywallet";
        public static string Monday { get; } = "Monday";
        public static string Saturday { get; } = "Saturday";
        public static string Sunday { get; } = "Sunday";
        public static string SetFirstDayOfWeek { get; } = "SetFirstDayOfWeek";

        #endregion

        #region MenuPage

        public static string Budgets { get; } = "Budgets";
        public static string CutsomCategories { get; } = "CutsomCategories";
        public static string Categories { get; } = "Categories";
        public static string Rating { get; } = "Rating";
        public static string Settings { get; } = "Settings";
        public static string Statistics { get; } = "Statistics";
        public static string Transactions { get; } = "Transactions";

        #endregion

        #region MainPage

        public static string Incomes { get; } = "Incomes";
        public static string Outcomes { get; } = "Outcomes";
        public static string Overview { get; } = "Overview";
        public static string Arrange => "Arrange";
        public static string PlusMark { get; } = "PlusMark";
        public static string SearchFilter { get; } = "SearchFilter";
        public static string SelectMonth { get; } = "SelectMonth";

        #endregion

        #region Search Filter Page

        public static string Amount { get; } = "Amount";
        public static string SelectAmount { get; } = "SelectAmount";
        public static string Type { get; } = "Type";
        public static string SelectType { get; } = "SelectType";
        public static string Category { get; } = "Category";
        public static string SelectCategory { get; } = "SelectCategory";
        public static string Note { get; } = "Note";
        public static string EnterNote { get; } = "EnterNote";
        public static string Times { get; } = "Times";
        public static string SelectTime { get; } = "SelectTime";
        public static string ViewResult { get; } = "ViewResult";
        public static string All { get; } = "All";
        public static string Over { get; } = "Over";
        public static string Under { get; } = "Under";
        public static string Between { get; } = "Between";
        public static string Exact { get; } = "Exact";
        public static string AllExpense { get; } = "AllExpense";
        public static string AllIncome { get; } = "AllIncome";
        public static string CustomCategory { get; } = "CustomCategory";
        public static string TitleExpense { get; } = "TitleExpense";
        public static string TitleIncome { get; } = "TitleIncome";
        public static string Date { get; } = "Date";
        public static string Before { get; } = "Before";
        public static string After { get; } = "After";
        public static string SelectDate { get; } = "SelectDate";

        #endregion

        #region View Search & Filter

        public static string ViewSearchFilter { get; } = "ViewSearchFilter";

        #endregion

        #region AddTransactionPage

        public static string AddTransaction = "AddTransaction";
        public static string Message { get; } = "Message";
        public static string NoCameraAvailable { get; } = "NoCameraAvailable";
        public static string CannotPickPhoto { get; } = "CannotPickPhoto";
        public static string Dismiss { get; } = "Dismiss";
        public static string TakePhoto { get; } = "TakePhoto";
        public static string PickPhoto { get; } = "PickPhoto";
        public static string PermissionsDenied { get; } = "PermissionsDenied";
        public static string UnableToTakePhotos { get; } = "UnableToTakePhotos";

        #endregion

        #region Data Transactions

        public static string Salary { get; } = "Salary";
        public static string PartTimeJob { get; } = "PartTimeJob";
        public static string Due { get; } = "Due";
        public static string Bonus { get; } = "Bonus";
        public static string Business { get; } = "Business";
        public static string Interest { get; } = "Interest";
        public static string Subsidy { get; } = "Subsidy";
        public static string RealEstate { get; } = "RealEstate";
        public static string Borrowing { get; } = "Borrowing";
        public static string Gift { get; } = "Gift";
        public static string Stock { get; } = "Stock";
        public static string Other { get; } = "Other";

        #region Outcomes

        public static string Food { get; } = "Food";
        public static string Travel { get; } = "Travel";
        public static string ElectricityWaterBill { get; } = "ElectricityWaterBill";
        public static string Shopping { get; } = "Shopping";
        public static string StudyingFee { get; } = "StudyingFee";
        public static string Party { get; } = "Party";
        public static string Card { get; } = "Card";
        public static string Loan { get; } = "Loan";
        public static string Relax { get; } = "Relax";
        public static string TravellingFee { get; } = "TravellingFee";
        public static string Repayment { get; } = "Repayment";
        public static string Medicine { get; } = "Medicine";
        public static string HouseHold { get; } = "HouseHold";

        #endregion

        #endregion


        #region Pop-up

        public static string Caution { get; } = "Caution";
        public static string Close { get; } = "Close";
        public static string Loading3dot { get; } = "Loading3dot";
        public static string Cancel { get; } = "Cancel";
        public static string Hide { get; } = "Hide";
        public static string WarningEnter { get; } = "WarningEnter";
        public static string InputHere { get; } = "InputHere";
        public static string Save { get; } = "Save";

        #region Input One Argument

        public static string From { get; } = "From";
        public static string To { get; } = "To";

        #endregion


        #endregion




    }
}

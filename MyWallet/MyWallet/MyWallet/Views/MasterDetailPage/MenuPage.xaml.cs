﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyWallet.ViewModels;
using Prism.Navigation;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.MasterDetailPage
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuPage : Xamarin.Forms.MasterDetailPage, IMasterDetailPageOptions
    {
		public MenuPage ()
		{
			InitializeComponent ();
		}

        public bool IsPresentedAfterNavigation => Device.Idiom != TargetIdiom.Phone;

        #region Properties

        private bool _isAppeared;

        #endregion

        protected override void OnAppearing()
        {
            base.OnAppearing();


            try
            {
                var bindingContext = BindingContext as BaseViewModel;

                if (!_isAppeared)
                    bindingContext?.OnFirstTimeAppear();

                _isAppeared = true;
                bindingContext?.OnAppearing();

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();


            if (BindingContext is BaseViewModel bindingContext)
                bindingContext.OnDisappearing();

        }

        public virtual void Cleanup() { }
    }
}
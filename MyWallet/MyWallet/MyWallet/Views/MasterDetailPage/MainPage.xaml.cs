﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyWallet.Controls;
using MyWallet.Models;
using MyWallet.ViewModels.MasterDetailPage;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.MasterDetailPage
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : BasePage
	{
	    private MainPageViewModel viewmodel;
		public MainPage ()
		{
			InitializeComponent ();
        }

        protected override void OnBindingContextChanged()
        {
            viewmodel = (MainPageViewModel) BindingContext;
        }

        private void Trans_Seletected(object sender, SelectedItemChangedEventArgs e)
        {
            lvTrans.SelectedItem = null;
        }

        private void Trans_Tapped(object sender, ItemTappedEventArgs e)
        {
            lvTrans.SelectedItem = null;

            Transaction transaction = (Transaction) e.Item;

            viewmodel?.GoToDetailTransaction(transaction);
        }

        private void SelectMonth(object sender, EventArgs e)
        {
            cpkMonth.Focus();
        }

        private void MonthSelected(object sender, EventArgs e)
        {
            var item = (CustomPicker) sender;
            var vm = (MainPageViewModel)BindingContext;
            vm?.UpdateAndShowTransByMonth(monthSelected: item.SelectedItem.ToString(), numberOfMonth: (item.SelectedIndex + 1));
        }
    }
}
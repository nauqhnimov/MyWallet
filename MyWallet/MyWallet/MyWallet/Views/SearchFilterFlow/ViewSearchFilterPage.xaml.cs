﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.SearchFilterFlow
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ViewSearchFilterPage : BasePage
	{
		public ViewSearchFilterPage ()
		{
			InitializeComponent ();
		}
	}
}
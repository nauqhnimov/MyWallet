﻿using System;
using MyWallet.Managers;
using MyWallet.ResourcesTranslation;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.SearchFilterFlow
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SearchFilterPage : BasePage
    {
		public SearchFilterPage ()
		{
			InitializeComponent ();
        }

        private void SelectAmount(object sender, EventArgs e)
        {
            customPickerAmount.Focus();
        }

        private void SelectCategory(object sender, EventArgs e)
        {
            customPickerCategory.Focus();
        }

        private void SelectDate(object sender, EventArgs e)
        {
            customPickerDate.Focus();
        }
    }
}
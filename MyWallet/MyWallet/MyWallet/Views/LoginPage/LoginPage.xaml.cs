﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyWallet.ViewModels.LoginPage;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.LoginPage
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : BasePage
	{
		public LoginPage ()
		{
			InitializeComponent ();
		}

	    protected override bool OnBackButtonPressed()
	    {
	        var vm = (LoginPageViewModel)this.BindingContext;
	        if (vm == null)
	            return false;

	        if (vm.WebViewOverlay != null && vm.WebViewOverlay.IsVisible)
	        {
	            vm.WebViewOverlay.IsVisible = false;
	            vm.IsVisibleWebView = false;
                return true;
	        }

	        return false;
	    }
    }
}
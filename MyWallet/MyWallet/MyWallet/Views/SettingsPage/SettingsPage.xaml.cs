﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.SettingsPage
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SettingsPage : BasePage
	{
		public SettingsPage ()
		{
			InitializeComponent ();

		    var picker = FormatTimePicker;

		    var df1 = DateTime.Now.ToString("dd/MM/yyyy");
		    var df2 = DateTime.Now.ToString("MM/dd/yyyy");
		    picker.Items.Add(df1);
		    picker.Items.Add(df2);
        }

	    private void SelectLanguage(object sender, EventArgs e)
	    {
	        LanguagePicker.Focus();

	    }

	    private void SelectFormatTime(object sender, EventArgs e)
	    {
	        FormatTimePicker.Focus();
	    }

	    private void SelectFirstDayOfWeek(object sender, EventArgs e)
	    {
	        cpkFirstDayOfWeek.Focus();
	    }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyWallet.Enums;
using MyWallet.ViewModels.CommonPages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.CommonPages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CategoriesIncomePage : BasePage
    {
		public CategoriesIncomePage ()
		{
			InitializeComponent ();
		}

	    private async void ListViewSelection_OnItemTapped(object sender, ItemTappedEventArgs e)
	    {
	        var item = (string)e.Item;
	        ListViewSelection.SelectedItem = null;
	        CategoriesTabbedPageViewModel vm = CategoriesTabbedPageViewModel.Instance;
            //var vm = (CategoriesTabbedPageViewModel)BindingContext;
	        await vm?.ListItemTapped(categorySelectedItem: item, transactionType: TransactionType.Income);
        }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyWallet.Controls;
using MyWallet.Enums;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.TransactionFlow
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddTransactionPage : BasePage
    {
        public AddTransactionPage()
        {
            InitializeComponent();

            InitFormat();
        }

        private void InitFormat()
        {
            lblCurrencyUnit.Text = (App.Settings.Language == (int)LanguageType.Vietnamese) ? "₫" : "$";
            dpkDateCreate.Format = App.Settings.FormatDate;
        }
    }
}
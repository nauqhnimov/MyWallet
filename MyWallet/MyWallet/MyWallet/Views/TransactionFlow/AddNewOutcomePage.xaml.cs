﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyWallet.Controls;
using MyWallet.Enums;
using MyWallet.ResourcesTranslation;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.TransactionFlow
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddNewOutcomePage : BasePage
    {
		public AddNewOutcomePage ()
		{
			InitializeComponent ();

		    var picker = LoaiPicker;

		    picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Food.ToString()));
		    picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Gift.ToString()));
		    picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Party.ToString()));
            picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Medicine.ToString()));
            picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Travel.ToString()));
            picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Shopping.ToString()));
		    picker.Items.Add(TranslateExtension.Get(OutcomeCategory.PhoneCard.ToString()));
            picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Travel.ToString()));
            picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Rent.ToString()));
            picker.Items.Add(TranslateExtension.Get(OutcomeCategory.ElectricityBill.ToString()));
		    picker.Items.Add(TranslateExtension.Get(OutcomeCategory.WaterBill.ToString()));
            picker.Items.Add(TranslateExtension.Get(OutcomeCategory.StudyingFee.ToString()));
            picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Loan.ToString()));
            picker.Items.Add(TranslateExtension.Get(OutcomeCategory.LoanPaying.ToString()));
            picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Others.ToString()));
        }

	    private void SoTien_OnCompleted(object sender, EventArgs e)
	    {
	        if (EntrySoTien.Text != null)
	        {
	            EntrySoTien.Text += " Đ";
	        }
	    }


	    private void EntrySoTien_OnFocused(object sender, FocusEventArgs e)
	    {
	        var entry = (CustomEntry)sender;
	        entry.Text = null;
	    }
    }
}
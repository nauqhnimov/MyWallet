﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyWallet.Controls;
using MyWallet.Enums;
using MyWallet.ResourcesTranslation;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.TransactionFlow
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddNewIncomePage : BasePage
    {
        public AddNewIncomePage()
        {
            InitializeComponent();

            var picker = LoaiPicker;
            picker.Items.Add(TranslateExtension.Get(IncomeCategory.Salary.ToString()));
            picker.Items.Add(TranslateExtension.Get(IncomeCategory.Bonus.ToString()));
            picker.Items.Add(TranslateExtension.Get(IncomeCategory.Subsidy.ToString()));
            picker.Items.Add(TranslateExtension.Get(IncomeCategory.Stock.ToString()));
            picker.Items.Add(TranslateExtension.Get(IncomeCategory.Due.ToString()));
            picker.Items.Add(TranslateExtension.Get(IncomeCategory.Interest.ToString()));
            picker.Items.Add(TranslateExtension.Get(IncomeCategory.Borrowing.ToString()));
            picker.Items.Add(TranslateExtension.Get(IncomeCategory.Others.ToString()));
        }

        private void SoTien_OnCompleted(object sender, EventArgs e)
        {
            if (EntrySoTien.Text != null)
            {
                EntrySoTien.Text += " Đ";
            }
        }


        private void EntrySoTien_OnFocused(object sender, FocusEventArgs e)
        {
            var entry = (CustomEntry)sender;
            entry.Text = null;
        }
    }
}
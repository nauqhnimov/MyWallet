﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyWallet.Enums;
using MyWallet.Models;
using MyWallet.ResourcesTranslation;
using Prism.Navigation;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.TransactionFlow
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailTransactionPage : BasePage
    {
        public Transaction ThongTin { get; set; }

        public DetailTransactionPage()
        {
            InitializeComponent();
        }

        public async void OnNavigatingTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("id"))
            {
                int Id = parameters.GetValue<int>("id");
                ThongTin = /*await App.Database.GetThongTinAsync(Id);*/ new Transaction();
                if (ThongTin.TransactionType == TransactionType.Outcome)
                {
                    EntrySoTien.TextColor = Color.FromHex("#E81120");
                    //var picker = LoaiPicker;
                    //picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Food.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Gift.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Party.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Medicine.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Travel.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Shopping.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Card.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Travel.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Rent.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(OutcomeCategory.ElectricityBill.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(OutcomeCategory.WaterBill.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(OutcomeCategory.StudyingFee.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Loan.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(OutcomeCategory.Others.ToString()));
                }
                else
                {
                    EntrySoTien.TextColor = Color.FromHex("#13CE66");
                    var picker = LoaiPicker;
                    //picker.Items.Add(TranslateExtension.Get(IncomeCategory.Salary.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(IncomeCategory.Bonus.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(IncomeCategory.Subsidy.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(IncomeCategory.Stock.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(IncomeCategory.Due.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(IncomeCategory.Interest.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(IncomeCategory.Borrowing.ToString()));
                    //picker.Items.Add(TranslateExtension.Get(IncomeCategory.Others.ToString()));
                }
            }
        }
    }
}
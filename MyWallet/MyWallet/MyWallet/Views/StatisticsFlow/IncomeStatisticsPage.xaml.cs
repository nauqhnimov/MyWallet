﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using MyWallet.Enums;
using MyWallet.Models;
using Prism.Navigation;
using SkiaSharp;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Entry = Xamarin.Forms.Entry;

namespace MyWallet.Views.StatisticsFlow
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class IncomeStatisticsPage : ContentPage, INavigationAware
	{
	    private List<Microcharts.Entry> Entries;
	    public double TongThu;
        
	    protected List<SKColor> listColors = new List<SKColor>(){SKColor.Parse("#009933"),SKColor.Parse("#33ccff"), SKColor.Parse("#006666"),
	        SKColor.Parse("#99ccff"),SKColor.Parse("#3333cc"),SKColor.Parse("#339933"),SKColor.Parse("#608000"),SKColor.Parse("#336699"),SKColor.Parse("#8080ff"),
	        SKColor.Parse("#ffff99"),SKColor.Parse("#666633") };

        public IncomeStatisticsPage ()
		{
			InitializeComponent ();

		   
        }

	    public void OnNavigatedFrom(NavigationParameters parameters)
	    {
	    }

	    public void OnNavigatingTo(NavigationParameters parameters)
	    {
	    }

        public async void OnNavigatedTo(NavigationParameters parameters)
        {
            DateTime StartedDay = new DateTime();
            DateTime EndedDay = new DateTime();
            List<Transaction> Transactions;

            if (parameters.ContainsKey(KeyParameters.startday.ToString()) && parameters.ContainsKey(KeyParameters.endday.ToString()) && parameters.ContainsKey(KeyParameters.Transactions.ToString()))
            {
                StartedDay = parameters.GetValue<DateTime>(KeyParameters.startday.ToString());
                EndedDay = parameters.GetValue<DateTime>(KeyParameters.endday.ToString());
                Transactions = parameters.GetValue<List<Transaction>>(KeyParameters.Transactions.ToString());

                Entries = await CreateEntries(Transactions, StartedDay, EndedDay);

                ChartThu.Chart = new DonutChart() { Entries = Entries };
                ChartThu.Chart.LabelTextSize = 35;
                LabelTongThu.Text = TongThu.ToString("N0", new CultureInfo("en-US")) + " Đ";
                if (TongThu == 0)
                {
                    layoutChart.IsVisible = false;
                    layoutMess.IsVisible = true;
                }
                else
                {
                    layoutChart.IsVisible = true;
                    layoutMess.IsVisible = false;
                }
            }
            //else
            //{
            //    StartedDay = new DateTime(2017, 12, 1);
            //    EndedDay = new DateTime(2017, 12, 31);
            //   }




        }


        public async Task<List<Microcharts.Entry>> CreateEntries(List<Transaction> transactions, DateTime batdau, DateTime ketthuc)
        {
            TongThu = new double();
            List<Microcharts.Entry> Entries = new List<Microcharts.Entry>();

            List<Transaction> a = transactions.Where(i => i.TransactionType != TransactionType.Outcome && i.CreatedDate.CompareTo(batdau) >= 0 & i.CreatedDate.CompareTo(ketthuc) <= 0).ToList();



            //lấy tất cả danh muc co khoang thu
            List<string> listDanhMuc = GetDanhMuc(a);

            for (var j = 0; j < listDanhMuc.Count; j++)
            {
                long sotien = TinhTongTien(a, listDanhMuc[j]);
                TongThu += sotien;

                Microcharts.Entry entry = new Microcharts.Entry(sotien);
                entry.Label = listDanhMuc[j];
                entry.ValueLabel = sotien.ToString("N0", new CultureInfo("en-US")) + " Đ";
                entry.Color = listColors[j];

                Entries.Add(entry);
            }
            return Entries;

        }

        /// <summary>
        /// Tính tổng sô tiền của các khoảng thu có loại là danhmuc trong listThongTin
        /// </summary>
        /// <param name="listThongTin"></param>
        /// <param name="danhmuc"></param>
        /// <returns></returns>
	    public long TinhTongTien(List<Transaction> listThongTin, string danhmuc)
        {
            long t = 0;
            for (var i = 0; i < listThongTin.Count; i++)
            {
                if (listThongTin[i].Category == danhmuc)
                {
                    string sotien;

                    //if (listThongTin[i].Amount[listThongTin[i].Amount.Length - 1].ToString().Equals("Đ"))
                    //{
                    //    sotien = listThongTin[i].Amount.Substring(0, listThongTin[i].Amount.Length - 2);
                    //}
                    //else
                    //{
                    //    sotien = listThongTin[i].Amount.Substring(0, listThongTin[i].Amount.Length);
                    //}
                    //string sotien = listThongTin[i].SoTien.Substring(0, listThongTin[i].SoTien.Length - 3);

                    sotien = listThongTin[i].Amount.Replace(@"[^0-9]+", "").Replace("Đ", "").Replace("đ", "").Replace("$", "").Replace(",", "").Replace(".", "").Replace(" ", "");

                    t += long.Parse(sotien);
                }

            }


            return t;
        }


        /// <summary>
        /// Lấy tất cả các danh mục khoảng thu hiện có trong listThongtin
        /// </summary>
        /// <param name="listThongtin"></param>
        /// <param name="listThongtin"></param>
        /// <returns></returns>
        public List<string> GetDanhMuc(List<Transaction> listThongtin)
        {
            List<string> listDanhMuc = new List<string>();
            for (var i = 0; i < listThongtin.Count; i++)
            {
                if (listDanhMuc.Contains(listThongtin[i].Category) == false)
                {
                    listDanhMuc.Add(listThongtin[i].Category);
                }
            }


            return listDanhMuc;
        }
    }
}
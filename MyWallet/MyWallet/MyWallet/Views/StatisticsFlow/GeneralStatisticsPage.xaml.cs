﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using MyWallet.Enums;
using MyWallet.Models;
using Prism.Navigation;
using SkiaSharp;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Entry = Microcharts.Entry;

namespace MyWallet.Views.StatisticsFlow
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GeneralStatisticsPage : ContentPage, INavigationAware
	{
	    private List<Microcharts.Entry> Entries;

	    public double SoDu;
	    double tongthu = 0, tongchi = 0;

        public GeneralStatisticsPage ()
		{
			InitializeComponent ();
        }


        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

	    public void OnNavigatingTo(NavigationParameters parameters)
	    {
	    }

        public async void OnNavigatedTo(NavigationParameters parameters)
	    {
	        DateTime StartedDay = new DateTime();
	        DateTime EndedDay = new DateTime();
	        List<Transaction> Transactions;
	        if (parameters.ContainsKey(KeyParameters.startday.ToString()) && parameters.ContainsKey(KeyParameters.endday.ToString()) && parameters.ContainsKey(KeyParameters.Transactions.ToString()))
	        {
	            StartedDay = parameters.GetValue<DateTime>(KeyParameters.startday.ToString());
	            EndedDay = parameters.GetValue<DateTime>(KeyParameters.endday.ToString());
                Transactions = parameters.GetValue<List<Transaction>>(KeyParameters.Transactions.ToString());

	            Entries = await CreateEntries(Transactions, StartedDay, EndedDay);

	            ChartThuChi.Chart = new BarChart { Entries = Entries };
	            LabelSodu.Text = SoDu.ToString("N0", new CultureInfo("en-US")) + " Đ";
	            ChartThuChi.Chart.LabelTextSize = 40;
	            if (SoDu < 0) LabelSodu.TextColor = Color.FromHex("#E81120");

	            if (tongchi == 0 && tongthu == 0)
	            {
	                layoutChart.IsVisible = false;
	                layoutMess.IsVisible = true;
	            }
	            else
	            {
	                layoutChart.IsVisible = true;
	                layoutMess.IsVisible = false;
                }
	        }



	    }

        public async Task<List<Microcharts.Entry>> CreateEntries(List<Transaction> transactions ,DateTime batdau, DateTime ketthuc)
        {
            SoDu = new double();
            tongchi = 0;
            tongthu = 0;


            List<Microcharts.Entry> Entries = new List<Microcharts.Entry>();

            List<Transaction> a = transactions.Where(i => i.CreatedDate.CompareTo(batdau) >= 0 & i.CreatedDate.CompareTo(ketthuc) <= 0).ToList();
            



            for (int i = 0; i < a.Count; i++)
            {
                //vào OnNavigatedTo 2 lần
                if (i == 0)
                {
                    tongchi = 0;
                    tongthu = 0;
                }
                string sotien;

                //if (a[i].Amount[a[i].Amount.Length - 1].ToString().Equals("Đ"))
                //{
                //    sotien = a[i].Amount.Substring(0, a[i].Amount.Length - 2);
                //}
                //else
                //{
                //    sotien = a[i].Amount.Substring(0, a[i].Amount.Length);
                //}

                //string sotien = a[i].SoTien.Substring(0, a[i].SoTien.Length - 3);
                sotien = a[i].Amount.Replace(@"[^0-9]+", "").Replace("Đ","").Replace("đ","").Replace("$","").Replace(",","").Replace(".", "").Replace(" ", "");


                if (a[i].TransactionType == TransactionType.Outcome)
                {
                    tongchi += long.Parse(sotien);
                }
                else
                {
                    tongthu += long.Parse(sotien);
                }
            }

            Microcharts.Entry entry1 = new Microcharts.Entry((float)tongthu);
            entry1.Label = "Thu";
            entry1.ValueLabel = tongthu.ToString("N0", new CultureInfo("en-US")) + " Đ";
            entry1.Color = SKColor.Parse("#13CE66");

            Entries.Add(entry1);

            Microcharts.Entry entry2 = new Microcharts.Entry((float)tongchi);
            entry2.Label = "Chi";
            entry2.ValueLabel = tongchi.ToString("N0", new CultureInfo("en-US")) + " Đ";
            entry2.Color = SKColor.Parse("#E81120");

            Entries.Add(entry2);

            SoDu = tongthu - tongchi;

            return Entries;

        }
    }
}
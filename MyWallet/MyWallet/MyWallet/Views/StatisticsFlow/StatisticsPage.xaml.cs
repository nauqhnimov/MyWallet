﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyWallet.Enums;
using MyWallet.Models;
using MyWallet.ViewModels.StatisticsFlow;
using Prism.Navigation;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.StatisticsFlow
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StatisticsPage : TabbedPage, INavigationAware
    {
        public StatisticsPage ()
        {
            InitializeComponent();
        }

        private StatisticsPageViewModel viewmodel;

        private List<Transaction> Transactions;

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            viewmodel = (StatisticsPageViewModel) BindingContext;
        }


        public void OnNavigatingTo(NavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            
            if (parameters.ContainsKey("startday") && parameters.ContainsKey("endday"))
            {
                var startday = parameters.GetValue<DateTime>("startday");
                var endday = parameters.GetValue<DateTime>("endday");

                Title = startday.ToString("d/M/yyyy") + " - " + endday.ToString("d/M/yyyy");

                foreach (var child in Children)
                {
                    (child as INavigatedAware)?.OnNavigatedTo(parameters);
                    (child?.BindingContext as INavigatedAware)?.OnNavigatedTo(parameters);
                }
            }
            else
            {
                Title = DateTime.Today.Month + "/" + DateTime.Today.Year;
                var p = new NavigationParameters();
                DateTime StartDay = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                DateTime EndDay = DateTime.Now;

                Transactions = viewmodel.GetTransactions(StartDay, EndDay);

                p.Add(KeyParameters.startday.ToString(), StartDay);
                p.Add(KeyParameters.endday.ToString(), EndDay);
                p.Add(KeyParameters.Transactions.ToString(), Transactions);
                p.Add(KeyParameters.__NavigationMode.ToString(), NavigationMode.New);

                foreach (var child in Children)
                {
                    (child as INavigatedAware)?.OnNavigatedTo(p);
                    (child?.BindingContext as INavigatedAware)?.OnNavigatedTo(p);
                }
            }
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
           
        }

        #region OnAppear

        protected override void OnAppearing()
        {
            if (App.StartDateForStatistic.Year != 1 && App.EndDateForStatistic.Year != 1)
            {
                DateTime startday = App.StartDateForStatistic;
                DateTime endday = App.EndDateForStatistic;
                
                Title = startday.ToString("d/M/yyyy") + " - " + endday.ToString("d/M/yyyy");

                Transactions = viewmodel.GetTransactions(startday, endday);

                var p = new NavigationParameters();
                p.Add(KeyParameters.startday.ToString(), startday);
                p.Add(KeyParameters.endday.ToString(), endday);
                p.Add(KeyParameters.Transactions.ToString(), Transactions);
                p.Add(KeyParameters.__NavigationMode.ToString(), NavigationMode.New);

                foreach (var child in Children)
                {
                    (child as INavigatedAware)?.OnNavigatedTo(p);
                    (child?.BindingContext as INavigatedAware)?.OnNavigatedTo(p);
                }

                App.StartDateForStatistic = new DateTime();
                App.EndDateForStatistic = new DateTime();
            }
        }

        #endregion
    }
}
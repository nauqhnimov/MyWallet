﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.StatisticsFlow
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChooseDatePage : ContentPage
    {
        public ChooseDatePage()
        {
            InitializeComponent();
        }
    }
}
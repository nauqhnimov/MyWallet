﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using MyWallet.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;

namespace MyWallet.Views
{
    public class BasePage : ContentPage
    {
        public Guid PageInstanceId { get; set; }

        public BasePage()
        {
            //InitializeComponent();
            PageInstanceId = Guid.NewGuid();
        }

        #region Properties

        private bool _isAppeared;

        #endregion

        #region OnAppear

        protected override void OnAppearing()
        {
            base.OnAppearing();

            try
            {
                var bindingContext = BindingContext as BaseViewModel;

                if (!_isAppeared)
                    bindingContext?.OnFirstTimeAppear();

                _isAppeared = true;
                bindingContext?.OnAppearing();

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            if (BindingContext is BaseViewModel bindingContext)
                bindingContext.OnDisappearing();

        }

        public virtual void Cleanup() { }

        #endregion
        
        #region BackButtonPress

        protected override bool OnBackButtonPressed()
        {
            var bindingContext = BindingContext as BaseViewModel;
            var result = bindingContext?.OnBackButtonPressed() ?? base.OnBackButtonPressed();
            return result;
        }

        public void OnSoftBackButtonPressed()
        {
            var bindingContext = BindingContext as BaseViewModel;
            bindingContext?.OnSoftBackButtonPressed();
        }

        public bool NeedOverrideSoftBackButton { get; set; } = false;

        #endregion
    }
}

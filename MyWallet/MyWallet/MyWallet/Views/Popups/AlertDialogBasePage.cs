﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Threading.Tasks;
//using Xamarin.Forms;

//namespace MyWallet.Views.Popups
//{
//    /// <summary>
//    /// The awesome Transparent Popup Page
//    /// sub-classed from Rg.Plugins.Popup
//    /// Customized for our usecase with
//    /// Generic data type support for the result
//    /// </summary>
//    /// <typeparam name="T"></typeparam>
//    public class AlertDialogBasePage<T> : PopupBasePage
//    {

//        // the task completion source
//        public TaskCompletionSource<T> PageClosedTaskCompletionSource { get; set; }

//        public AlertDialogBasePage(View contentBody)
//        {
//            //Content = contentBody;

//            // init the task completion source
//            PageClosedTaskCompletionSource = new System.Threading.Tasks.TaskCompletionSource<T>();

//            //this.BackgroundColor = new Color(0, 0, 0, 0.4);
//        }

//        // Method for animation child in PopupPage
//        // Invoced after custom animation end
//        protected override Task OnAppearingAnimationEnd()
//        {
//            return Content.FadeTo(1);
//        }

//        // Method for animation child in PopupPage
//        // Invoked before custom animation begin
//        protected override Task OnDisappearingAnimationBegin()
//        {
//            return Content.FadeTo(1);
//        }

//        protected override bool OnBackButtonPressed()
//        {
//            // Prevent back button pressed action on android
//            //return base.OnBackButtonPressed();
//            return true;
//        }

//        // Invoced when background is clicked
//        protected override bool OnBackgroundClicked()
//        {
//            // Prevent background clicked action
//            //return base.OnBackgroundClicked();
//            return false;
//        }
//    }

//}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MyWallet.Enums;
using MyWallet.Managers;
using MyWallet.Models;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.Popups
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InputTwoArgumentsPopup : PopupBasePage
	{
        private Task<ModelInputTwoArgumentsPopup> ResultTask => ResultTaskCompletionSource.Task;

        // the task completion source
        private TaskCompletionSource<ModelInputTwoArgumentsPopup> ResultTaskCompletionSource { get; set; }

	    private ModelInputTwoArgumentsPopup MultipleTextResult { get; set; }

        public InputTwoArgumentsPopup()
        {
            InitializeComponent();

            LabelUnitFrom.Text = (App.Settings.Language == (int)LanguageType.Vietnamese) ? "₫" : "$";
            LabelUnitTo.Text = (App.Settings.Language == (int)LanguageType.Vietnamese) ? "₫" : "$";
        }


        #region Instance

        private static InputTwoArgumentsPopup _instance;

        public static InputTwoArgumentsPopup Instance => _instance ?? (_instance = new InputTwoArgumentsPopup() { IsClosed = true });

        public async Task<ModelInputTwoArgumentsPopup> Show(string title = null, string placeHolder = null, string warningText = null,
            string cancelButtonText = null, ICommand cancelCommand = null, object cancelCommandParameter = null,
            string saveButtonText = null, ICommand saveCommand = null, object saveCommandParameter = null,
            bool isAutoClose = false, uint duration = 2000)
        {
            // Close Loading Popup if it is showing
            await LoadingPopup.Instance.Hide();

            ResultTaskCompletionSource = new System.Threading.Tasks.TaskCompletionSource<ModelInputTwoArgumentsPopup>();
            MultipleTextResult = new ModelInputTwoArgumentsPopup();

            await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
            {
                if (title != null)
                    LabelTitle.Text = title;

                if (placeHolder != null)
                {
                    EntryFrom.Placeholder = placeHolder;
                    EntryTo.Placeholder = placeHolder;
                }

                if (warningText != null)
                    LabelWarning.Text = warningText;

                if (cancelButtonText != null)
                    CancelButton.Text = cancelButtonText;

                CancelCommand = cancelCommand;
                CancelCommandParameter = cancelCommandParameter;

                if (saveButtonText != null)
                    SaveButton.Text = saveButtonText;

                SaveCommand = saveCommand;
                SaveCommandParameter = saveCommandParameter;

                IsAutoClose = isAutoClose;
                Duration = duration;

            });

            if (IsClosed)
            {
                IsClosed = false;

                if (isAutoClose && duration > 0)
                    AutoClosePopupAfter(duration);

                await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
                {
                    await App.Current.MainPage.Navigation.PushPopupAsync(this);
                });
            }

            var result = await this.ResultTask;

            return result;
        }

        #endregion

        #region Save Events

        private async void SavePopupEvent(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(EntryFrom.Text) || string.IsNullOrEmpty(EntryTo.Text))
            {
                LabelWarning.IsVisible = true;
                return;
            }

            await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
            {
                MultipleTextResult.EntryFrom = EntryFrom.Text;
                MultipleTextResult.EntryTo = EntryTo.Text;
                this.ResultTaskCompletionSource.SetResult(MultipleTextResult);

                await Navigation.PopPopupAsync();
            });

            // waiting for close animation finished
            await Task.Delay(300);

            SaveCommand?.Execute(SaveCommandParameter);

            //_popupId++;
            IsClosed = true;
        }

        #endregion

        #region Cancel Event

        protected async void CancelPopupEvent(object sender, EventArgs e)
        {
            await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
            {
                this.ResultTaskCompletionSource.SetResult(null);

                await Navigation.PopPopupAsync();
            });

            CancelCommand?.Execute(CancelCommandParameter);
            _popupId++;

            IsClosed = true;
        }

        #endregion

        #region CancelCommand

        public static readonly BindableProperty CancelCommandProperty =
            BindableProperty.Create(nameof(CancelCommand),
                typeof(ICommand),
                typeof(InputTwoArgumentsPopup),
                null,
                BindingMode.TwoWay);

        public ICommand CancelCommand
        {
            get => (ICommand)GetValue(CancelCommandProperty);
            set => SetValue(CancelCommandProperty, value);
        }

        public static readonly BindableProperty CancelCommandParameterProperty =
            BindableProperty.Create(nameof(CancelCommandParameter),
                typeof(object),
                typeof(InputTwoArgumentsPopup),
                null,
                BindingMode.TwoWay);

        public object CancelCommandParameter
        {
            get => GetValue(CancelCommandParameterProperty);
            set => SetValue(CancelCommandParameterProperty, value);
        }

        #endregion

        #region SaveCommand

        public static readonly BindableProperty SaveCommandProperty =
            BindableProperty.Create(nameof(SaveCommand),
                typeof(ICommand),
                typeof(InputTwoArgumentsPopup),
                null,
                BindingMode.TwoWay);

        public ICommand SaveCommand
        {
            get => (ICommand)GetValue(SaveCommandProperty);
            set => SetValue(SaveCommandProperty, value);
        }

        public static readonly BindableProperty SaveCommandParameterProperty =
            BindableProperty.Create(nameof(SaveCommandParameter),
                typeof(object),
                typeof(InputTwoArgumentsPopup),
                null,
                BindingMode.TwoWay);

        public object SaveCommandParameter
        {
            get => GetValue(SaveCommandParameterProperty);
            set => SetValue(SaveCommandParameterProperty, value);
        }

        #endregion


        #region RefreshUI

        public void RefreshUI()
        {
            InitializeComponent();
        }

        #endregion
    }
}
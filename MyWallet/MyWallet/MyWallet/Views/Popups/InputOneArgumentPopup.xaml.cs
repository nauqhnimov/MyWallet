﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MyWallet.Enums;
using MyWallet.Managers;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.Popups
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InputOneArgumentPopup : PopupBasePage
    {
        private Task<string> ResultTask => ResultTaskCompletionSource.Task;

        // the task completion source
        private TaskCompletionSource<string> ResultTaskCompletionSource { get; set; }

        public InputOneArgumentPopup()
        {
            InitializeComponent();

            LabelUnitFrom.Text = (App.Settings.Language == (int)LanguageType.Vietnamese) ? "₫" : "$";
        }
        

        #region Instance

        private static InputOneArgumentPopup _instance;

        public static InputOneArgumentPopup Instance => _instance ?? (_instance = new InputOneArgumentPopup() { IsClosed = true });

        public async Task<string> Show(string title = null, string placeHolder = null, string warningText = null, string valueInput = null,
            string cancelButtonText = null, ICommand cancelCommand = null, object cancelCommandParameter = null,
            string saveButtonText = null, ICommand saveCommand = null, object saveCommandParameter = null,
            bool isAutoClose = false, uint duration = 2000)
        {
            // Close Loading Popup if it is showing
            await LoadingPopup.Instance.Hide();

            ResultTaskCompletionSource = new System.Threading.Tasks.TaskCompletionSource<string>();

            await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
            {
                if (title != null)
                    LabelTitle.Text = title;

                if (placeHolder != null)
                    EntryInput.Placeholder = placeHolder;

                if (string.IsNullOrEmpty(valueInput))
                    EntryInput.Text = valueInput;

                if (warningText != null)
                    LabelWarning.Text = warningText;

                LabelWarning.IsVisible = false;

                if (cancelButtonText != null)
                    CancelButton.Text = cancelButtonText;

                CancelCommand = cancelCommand;
                CancelCommandParameter = cancelCommandParameter;

                if (saveButtonText != null)
                    SaveButton.Text = saveButtonText;

                SaveCommand = saveCommand;
                SaveCommandParameter = saveCommandParameter;

                IsAutoClose = isAutoClose;
                Duration = duration;

            });

            if (IsClosed)
            {
                IsClosed = false;

                if (isAutoClose && duration > 0)
                    AutoClosePopupAfter(duration);

                await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
                {
                    await App.Current.MainPage.Navigation.PushPopupAsync(this);
                });
            }

            var result = await this.ResultTask;

            return result;
        }

        #endregion

        #region Save Events

        private async void SavePopupEvent(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(EntryInput.Text))
            {
                LabelWarning.IsVisible = true;
                return;
            }

            await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
            {
                this.ResultTaskCompletionSource.SetResult(EntryInput.Text);

                await Navigation.PopPopupAsync();
            });

            // waiting for close animation finished
            await Task.Delay(300);

            SaveCommand?.Execute(SaveCommandParameter);

            //_popupId++;
            IsClosed = true;
        }

        #endregion

        #region Cancel Event

        protected async void CancelPopupEvent(object sender, EventArgs e)
        {
            await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
            {
                this.ResultTaskCompletionSource.SetResult(null);

                await Navigation.PopPopupAsync();
            });

            CancelCommand?.Execute(CancelCommandParameter);
            _popupId++;

            IsClosed = true;
        }

        #endregion

        #region CancelCommand

        public static readonly BindableProperty CancelCommandProperty =
            BindableProperty.Create(nameof(CancelCommand),
                typeof(ICommand),
                typeof(InputOneArgumentPopup),
                null,
                BindingMode.TwoWay);

        public ICommand CancelCommand
        {
            get => (ICommand)GetValue(CancelCommandProperty);
            set => SetValue(CancelCommandProperty, value);
        }

        public static readonly BindableProperty CancelCommandParameterProperty =
            BindableProperty.Create(nameof(CancelCommandParameter),
                typeof(object),
                typeof(InputOneArgumentPopup),
                null,
                BindingMode.TwoWay);

        public object CancelCommandParameter
        {
            get => GetValue(CancelCommandParameterProperty);
            set => SetValue(CancelCommandParameterProperty, value);
        }

        #endregion

        #region SaveCommand

        public static readonly BindableProperty SaveCommandProperty =
            BindableProperty.Create(nameof(SaveCommand),
                typeof(ICommand),
                typeof(InputOneArgumentPopup),
                null,
                BindingMode.TwoWay);

        public ICommand SaveCommand
        {
            get => (ICommand)GetValue(SaveCommandProperty);
            set => SetValue(SaveCommandProperty, value);
        }

        public static readonly BindableProperty SaveCommandParameterProperty =
            BindableProperty.Create(nameof(SaveCommandParameter),
                typeof(object),
                typeof(InputOneArgumentPopup),
                null,
                BindingMode.TwoWay);

        public object SaveCommandParameter
        {
            get => GetValue(SaveCommandParameterProperty);
            set => SetValue(SaveCommandParameterProperty, value);
        }

        #endregion


        #region RefreshUI

        public void RefreshUI()
        {
            InitializeComponent();
        }

        #endregion
    }
}
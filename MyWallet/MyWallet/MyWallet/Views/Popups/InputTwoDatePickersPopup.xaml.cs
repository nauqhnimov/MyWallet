﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using MyWallet.Managers;
using MyWallet.Models;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MyWallet.Views.Popups
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InputTwoDatePickersPopup : PopupBasePage
    {
        private Task<ModelInputTwoArgumentsPopup> ResultTask => ResultTaskCompletionSource.Task;

        // the task completion source
        private TaskCompletionSource<ModelInputTwoArgumentsPopup> ResultTaskCompletionSource { get; set; }

        private ModelInputTwoArgumentsPopup MultipleTextResult { get; set; }

        public InputTwoDatePickersPopup()
        {
            InitializeComponent();
        }


        #region Instance

        private static InputTwoDatePickersPopup _instance;

        public static InputTwoDatePickersPopup Instance => _instance ?? (_instance = new InputTwoDatePickersPopup() { IsClosed = true });

        public async Task<ModelInputTwoArgumentsPopup> Show(string title = null, string placeHolder = null, string warningText = null, ModelInputTwoArgumentsPopup valueInput = null,
            string cancelButtonText = null, ICommand cancelCommand = null, object cancelCommandParameter = null,
            string saveButtonText = null, ICommand saveCommand = null, object saveCommandParameter = null,
            bool isAutoClose = false, uint duration = 2000)
        {
            // Close Loading Popup if it is showing
            await LoadingPopup.Instance.Hide();

            ResultTaskCompletionSource = new System.Threading.Tasks.TaskCompletionSource<ModelInputTwoArgumentsPopup>();
            MultipleTextResult = new ModelInputTwoArgumentsPopup();

            await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
            {
                if (title != null)
                    LabelTitle.Text = title;

                DatePickerInputFrom.Format = App.Settings.FormatDate;
                DatePickerInputTo.Format = App.Settings.FormatDate;

                if (valueInput != null)
                {
                    var resultTextValueFrom = Regex.Replace(valueInput.EntryFrom.ToString(), @"[^0-9/]+", "/");
                    DatePickerInputFrom.Date = DateTime.Parse(resultTextValueFrom);

                    var resultTextValueTo = Regex.Replace(valueInput.EntryTo.ToString(), @"[^0-9/]+", "/");
                    DatePickerInputTo.Date = DateTime.Parse(resultTextValueTo);
                }

                if (warningText != null)
                    LabelWarning.Text = warningText;

                LabelWarning.IsVisible = false;

                if (cancelButtonText != null)
                    CancelButton.Text = cancelButtonText;

                CancelCommand = cancelCommand;
                CancelCommandParameter = cancelCommandParameter;

                if (saveButtonText != null)
                    SaveButton.Text = saveButtonText;

                SaveCommand = saveCommand;
                SaveCommandParameter = saveCommandParameter;

                IsAutoClose = isAutoClose;
                Duration = duration;

            });

            if (IsClosed)
            {
                IsClosed = false;

                if (isAutoClose && duration > 0)
                    AutoClosePopupAfter(duration);

                await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
                {
                    await App.Current.MainPage.Navigation.PushPopupAsync(this);
                });
            }

            var result = await this.ResultTask;

            return result;
        }

        #endregion

        #region Save Events

        private async void SavePopupEvent(object sender, EventArgs e)
        {
            if (DatePickerInputFrom.Date > DatePickerInputTo.Date || DatePickerInputFrom.Date == DatePickerInputTo.Date)
            {
                LabelWarning.IsVisible = true;
                return;
            }

            await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
            {
                MultipleTextResult.EntryFrom = DatePickerInputFrom.Date.ToString(App.Settings.FormatDate);
                MultipleTextResult.EntryTo = DatePickerInputTo.Date.ToString(App.Settings.FormatDate);
                this.ResultTaskCompletionSource.SetResult(MultipleTextResult);

                await Navigation.PopPopupAsync();
            });

            // waiting for close animation finished
            await Task.Delay(300);

            SaveCommand?.Execute(SaveCommandParameter);

            //_popupId++;
            IsClosed = true;
        }

        #endregion

        #region Cancel Event

        protected async void CancelPopupEvent(object sender, EventArgs e)
        {
            await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
            {
                this.ResultTaskCompletionSource.SetResult(null);

                await Navigation.PopPopupAsync();
            });

            CancelCommand?.Execute(CancelCommandParameter);
            _popupId++;

            IsClosed = true;
        }

        #endregion

        #region CancelCommand

        public static readonly BindableProperty CancelCommandProperty =
            BindableProperty.Create(nameof(CancelCommand),
                typeof(ICommand),
                typeof(InputTwoDatePickersPopup),
                null,
                BindingMode.TwoWay);

        public ICommand CancelCommand
        {
            get => (ICommand)GetValue(CancelCommandProperty);
            set => SetValue(CancelCommandProperty, value);
        }

        public static readonly BindableProperty CancelCommandParameterProperty =
            BindableProperty.Create(nameof(CancelCommandParameter),
                typeof(object),
                typeof(InputTwoDatePickersPopup),
                null,
                BindingMode.TwoWay);

        public object CancelCommandParameter
        {
            get => GetValue(CancelCommandParameterProperty);
            set => SetValue(CancelCommandParameterProperty, value);
        }

        #endregion

        #region SaveCommand

        public static readonly BindableProperty SaveCommandProperty =
            BindableProperty.Create(nameof(SaveCommand),
                typeof(ICommand),
                typeof(InputTwoDatePickersPopup),
                null,
                BindingMode.TwoWay);

        public ICommand SaveCommand
        {
            get => (ICommand)GetValue(SaveCommandProperty);
            set => SetValue(SaveCommandProperty, value);
        }

        public static readonly BindableProperty SaveCommandParameterProperty =
            BindableProperty.Create(nameof(SaveCommandParameter),
                typeof(object),
                typeof(InputTwoDatePickersPopup),
                null,
                BindingMode.TwoWay);

        public object SaveCommandParameter
        {
            get => GetValue(SaveCommandParameterProperty);
            set => SetValue(SaveCommandParameterProperty, value);
        }

        #endregion


        #region RefreshUI

        public void RefreshUI()
        {
            InitializeComponent();
        }

        #endregion
    }
}
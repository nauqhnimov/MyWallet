﻿using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Xamarin.Forms;

namespace MyWallet.Views.Popups
{
    public class PopupAnimation : MoveAnimation
    {
        public PopupAnimation()
        {
            PositionIn = MoveAnimationOptions.Right;
            PositionOut = MoveAnimationOptions.Right;
            DurationIn = 200;
            DurationOut = 200;
            EasingIn = Easing.CubicOut;
            EasingOut = Easing.CubicOut;
            HasBackgroundAnimation = true;
        }
    }


    public class PopupNoAnimation : ScaleAnimation
    {
        public PopupNoAnimation()
        {
            PositionIn = MoveAnimationOptions.Center;
            PositionOut = MoveAnimationOptions.Center;
            ScaleIn = 1;
            ScaleOut = 1;
            DurationIn = 0;
            DurationOut = 0;
            EasingIn = Easing.CubicOut;
            EasingOut = Easing.CubicOut;
            HasBackgroundAnimation = true;
        }
    }

    public class PopupAlphaAnimation : FadeAnimation
    {
        public PopupAlphaAnimation()
        {
            DurationIn = 100;
            DurationOut = 100;
            EasingIn = Easing.CubicIn;
            EasingOut = Easing.CubicOut;
            HasBackgroundAnimation = true;
        }
    }
}

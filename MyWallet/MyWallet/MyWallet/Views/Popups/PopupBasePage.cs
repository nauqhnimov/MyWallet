﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using MyWallet.Managers;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;

namespace MyWallet.Views.Popups
{
    public class PopupBasePage : PopupPage
    {
        public PopupBasePage()
        { }
        


        #region internal variables

        protected bool IsAutoClose;
        public bool IsClosed { get; protected set; }
        protected uint Duration;
        protected int _popupId;

        #endregion

        #region Page Event

        protected async void ClosePopupEvent(object sender, EventArgs e)
        {
            await ClosePopup();
        }

        protected void DoNothingEvent(object sender, EventArgs e)
        {
            // Do nothing
        }

        protected async void AutoClosePopupAfter(uint duration)
        {
            int id = _popupId;
            await Task.Delay((int)duration);

            // If the popup still appear after duration time then close the popup
            if (_popupId == id)
                await ClosePopup();
        }

        public async Task ClosePopup()
        {
            await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
            {
                await Navigation.PopPopupAsync();
            });

            ClosePopupCommand?.Execute(ClosePopupCommandParameter);
            _popupId++;

            IsClosed = true;
        }

        #endregion

        #region ClosePopupCommand

        public static readonly BindableProperty ClosePopupCommandProperty =
            BindableProperty.Create(nameof(ClosePopupCommand),
                typeof(ICommand),
                typeof(PopupBasePage),
                null,
                BindingMode.TwoWay);

        public ICommand ClosePopupCommand
        {
            get => (ICommand)GetValue(ClosePopupCommandProperty);
            set => SetValue(ClosePopupCommandProperty, value);
        }

        public static readonly BindableProperty ClosePopupCommandParameterProperty =
            BindableProperty.Create(nameof(ClosePopupCommandParameter),
                typeof(object),
                typeof(PopupBasePage),
                null,
                BindingMode.TwoWay);

        public object ClosePopupCommandParameter
        {
            get => GetValue(ClosePopupCommandParameterProperty);
            set => SetValue(ClosePopupCommandParameterProperty, value);
        }

        #endregion

        protected override void OnDisappearing()
        {
            IsClosed = true;
            base.OnDisappearing();
        }

        #region Override

        // Method for animation child in PopupPage
        // Invoked before custom animation begin
        protected override Task OnDisappearingAnimationBegin()
        {
            return Content.FadeTo(0.5);
        }

        // Method for animation child in PopupPage
        // Invoced after custom animation end
        protected override Task OnAppearingAnimationEnd()
        {
            return Content.FadeTo(1);
        }

        protected override bool OnBackButtonPressed()
        {
            // Prevent back button pressed action on android
            //return base.OnBackButtonPressed();
            return true;
        }

        // Invoced when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Prevent background clicked action
            //return base.OnBackgroundClicked();
            return false;
        }

        #endregion

    }
}

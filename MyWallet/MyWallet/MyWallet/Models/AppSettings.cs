﻿using System;
using System.Collections.Generic;
using System.Text;
using MyWallet.Enums;
using MyWallet.Managers;
using MyWallet.ResourcesTranslation;
using SQLite.Net.Attributes;

namespace MyWallet.Models
{
    public class AppSettings
    {
        [PrimaryKey]
        public int Id { get; set; } = 0;

        public int Language { get; set; } = (int)LanguageType.Vietnamese; // Set Vietnamese is default language

        public string FullName { get; set; }

        public string UserName { get; set; }

        public string FormatDate { get; set; } = "dd/MM/yyyy";

        public string FirstOfWeek { get; set; } = FirstDayOfWeekType.Monday.ToString();
    }
}

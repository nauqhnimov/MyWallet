﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyWallet.Models
{
    public class ModelInputTwoArgumentsPopup
    {
        public Object EntryFrom { get; set; }
        public Object EntryTo { get; set; }
    }
}

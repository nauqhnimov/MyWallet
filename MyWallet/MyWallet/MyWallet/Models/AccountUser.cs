﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using MyWallet.Enums;
using Newtonsoft.Json;
using SQLite.Net.Attributes;

namespace MyWallet.Models
{
    public class AccountUser
    {
        [PrimaryKey]
        public int Id { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }
        [JsonProperty("Info")]
        public string FullName { get; set; }

        public string CryptPassword(string password)
        {
            var key = "MyWallet";

            var inputArray = Encoding.UTF8.GetBytes(password);
            var tripleDES = new TripleDESCryptoServiceProvider
            {
                Key = Encoding.UTF8.GetBytes(key),
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            var cTransform = tripleDES.CreateEncryptor();
            var resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
    }
}

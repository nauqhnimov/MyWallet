﻿using System;
using System.Collections.Generic;
using System.Text;
using MyWallet.Enums;
using SQLite.Net.Attributes;

namespace MyWallet.Models
{
    public class Transaction
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Amount { get; set; }
        public string ColorText { get; set; }
        public string Category { get; set; }
        public string Note { get; set; }

        public DateTime CreatedDate { get; set; }

        public byte[] DescriptionPhoto { get; set; }

        public TransactionType TransactionType { get; set; } = (int) TransactionType.None;

        public string UserName { get; set; }

        //public bool IsAOutcome { get; set; }

        public Transaction()
        {
        }

        public Transaction(string amount, string category, string note, DateTime createdDate, byte[] descriptionPhoto, TransactionType transactionType)
        {
            Amount = amount;
            Category = category;
            Note = note;
            CreatedDate = createdDate;
            DescriptionPhoto = descriptionPhoto;
            //IsAOutcome = isAOutcome;
            TransactionType = transactionType;
        }

        public Transaction(int id, string amount, string category, string note, DateTime createdDate, byte[] descriptionPhoto, TransactionType transactionType)
        {
            Id = id;
            Amount = amount;
            Category = category;
            Note = note;
            CreatedDate = createdDate;
            DescriptionPhoto = descriptionPhoto;
            //IsAOutcome = isAOutcome;
            TransactionType = transactionType;
        }

        public Transaction(int id)
        {
            Id = id;
        }

        public Transaction Duplicate()
        {
            Transaction duplicatedTransaction = new Transaction(this.Id, this.Amount, this.Category, this.Note, this.CreatedDate, this.DescriptionPhoto , this.TransactionType);
            return duplicatedTransaction;
        }

        public bool IsNull()
        {
            if (string.IsNullOrEmpty(Amount) || this.Category == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }





        //private string _name;
        //private string _company;
        //private string _photoUrl;

        //public string Name
        //{
        //    get => _name;
        //    set
        //    {
        //        SetProperty(ref _name, value);
        //    }
        //}


        //public string Company
        //{
        //    get => _company;
        //    set => SetProperty(ref _company, value);
        //}


        //public string PhotoUrl
        //{
        //    get => _photoUrl;
        //    set => SetProperty(ref _photoUrl, value);
        //}

        //public object Clone()
        //{
        //    return this.MemberwiseClone();
        //}
    }
}

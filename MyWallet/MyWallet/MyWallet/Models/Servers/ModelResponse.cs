﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MyWallet.Models.Servers
{
    public class JArrayResponse
    {
        [JsonProperty("Error")]
        public string Error { get; set; }

        [JsonProperty("ErrorCode")]
        public int ErrorCode { get; set; }

        [JsonProperty("Message")]
        public string Message { get; set; }

        [JsonProperty("Result")]
        public JArray Result { get; set; }
    }

    public class JObjectResponse
    {
        [JsonProperty("Error")]
        public string Error { get; set; }

        [JsonProperty("ErrorCode")]
        public int ErrorCode { get; set; }

        [JsonProperty("Output")]
        public JObject Output { get; set; }

        [JsonProperty("Message")]
        public string Message { get; set; }

        [JsonProperty("Result")]
        public JObject Result { get; set; }
    }
}

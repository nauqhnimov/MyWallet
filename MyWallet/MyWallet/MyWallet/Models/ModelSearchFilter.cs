﻿using System;
using System.Collections.Generic;
using System.Text;
using MyWallet.Enums;

namespace MyWallet.Models
{
    public class ModelSearchFilter
    {
        public string Amount { get; set; }
        public string Category { get; set; }
        public string Note { get; set; }

        public string Date { get; set; }

        public string IsAOutcome { get; set; }
        public TransactionType TransactionType { get; set; }
    }
}

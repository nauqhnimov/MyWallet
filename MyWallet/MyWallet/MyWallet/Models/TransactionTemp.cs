﻿using System;
using MyWallet.Enums;
using SQLite.Net.Attributes;

namespace MyWallet.Models
{
    public class TransactionTemp : Transaction
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Amount { get; set; }
        public string ColorText { get; set; }
        public string Category { get; set; }
        public string Note { get; set; }

        public DateTime CreatedDate { get; set; }

        public byte[] DescriptionPhoto { get; set; }

        public TransactionType TransactionType { get; set; } = (int)TransactionType.None;

        public string UserName { get; set; }

        //public bool IsAOutcome { get; set; }

        public TransactionTemp()
        {
        }

        public TransactionTemp(Transaction trans)
        {
            Id = trans.Id;
            Amount = trans.Amount;
            Category = trans.Category;
            Note = trans.Note;
            CreatedDate = trans.CreatedDate;
            DescriptionPhoto = trans.DescriptionPhoto;
            TransactionType = trans.TransactionType;
            ColorText = trans.ColorText;
            UserName = trans.UserName;
        }

        public bool IsNull()
        {
            if (string.IsNullOrEmpty(Amount) || this.Category == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

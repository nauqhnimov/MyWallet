﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using MyWallet.Enums;
using MyWallet.Interfaces.LocalDatabase;
using MyWallet.Managers;
using MyWallet.ResourcesTranslation;
using Prism.Navigation;
using Prism.Services;

namespace MyWallet.ViewModels.SettingsPage
{
    public class SettingsPageViewModel : BaseViewModel
    {
        public SettingsPageViewModel(IPageDialogService dialogService, INavigationService navService,
            ISQLiteService sqLiteService)
            : base(dialogService: dialogService, navigationService: navService, sqLiteService: sqLiteService)
        {
            Title = TranslateExtension.Get(TranslateManager.Settings);

            //TapSkipCommand = new DelegateCommand(SkipExecute);
            //LoginCommand = new DelegateCommand(async () => { await LoginExecute(); });
            //LoginFBCommand = new Command(async () => await LoginFBExecute());
        }

        #region Properties

        private string _language;
        public string Language
        {
            get { return _language; }
            set
            {
                SetProperty(ref _language, value);
                ChangeLanguage();
            }
        }

        private string _dateFormat;
        public string DateFormat
        {
            get { return _dateFormat; }
            set
            {
                SetProperty(ref _dateFormat, value);
                if (string.IsNullOrEmpty(DateFormat))
                    return;
                UpdateFormatDate();
            }
        }

        private string _firstDayOfWeek;
        public string FirstDayOfWeek
        {
            get { return _firstDayOfWeek; }
            set
            {
                SetProperty(ref _firstDayOfWeek, value);
                if (string.IsNullOrEmpty(FirstDayOfWeek))
                    return;
                UpdateFirstDayOfWeek();
            }
        }

        private ObservableCollection<string> _listFirstDayOfWeek;// = new ObservableCollection<string>();
        public ObservableCollection<string> ListFirstDayOfWeek
        {
            get { return _listFirstDayOfWeek; }
            set { SetProperty(ref _listFirstDayOfWeek, value); }
        }

        #endregion

        #region OnFirstTimeAppear

        public override void OnFirstTimeAppear()
        {
            InitSettingPage();
        }

        #endregion

        #region InitSettingPage

        private void InitSettingPage()
        {
            var language = (LanguageType)Enum.ToObject(typeof(LanguageType), App.Settings.Language);
            //var test = (LanguageType) App.Settings.Language;
            Language = language.ToString();

            DateFormat = DateTime.Now.ToString(App.Settings.FormatDate);

            ListFirstDayOfWeek = new ObservableCollection<string>()
            {
                TranslateExtension.Get(TranslateManager.Monday),
                TranslateExtension.Get(TranslateManager.Saturday),
                TranslateExtension.Get(TranslateManager.Sunday)
            };
            FirstDayOfWeek = TranslateExtension.Get(App.Settings.FirstOfWeek);
        }

        #endregion

        #region UpdateFormatDate

        private void UpdateFormatDate()
        {
            App.Settings.FormatDate =
                DateFormat.Equals(DateTime.Now.ToString("dd/MM/yyyy")) ? "dd/MM/yyyy" : "MM/dd/yyyy";
            Database.Update(App.Settings);
        }

        #endregion

        #region UpdateFirstDayOfWeek

        private void UpdateFirstDayOfWeek()
        {
            var index = ListFirstDayOfWeek.IndexOf(FirstDayOfWeek);
            App.Settings.FirstOfWeek = Enum.ToObject(typeof(FirstDayOfWeekType), index).ToString();
            Database.Update(App.Settings);
        }

        #endregion

        #region ChangeLanguage

        private async void ChangeLanguage()
        {
            if (string.IsNullOrEmpty(Language))
                return;

            var languageType = (LanguageType)Enum.Parse(typeof(LanguageType), Language);

            if (App.Settings.Language == (int) languageType)
                return;
            

            App.Settings.Language = (int) languageType;

            Database.Update(App.Settings);

            await NavigationService.NavigateAsync(PageManager.MainPageUri());
        }

        #endregion


    }
}

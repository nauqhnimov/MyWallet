﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using MyWallet.Enums;
using MyWallet.Helpers;
using MyWallet.Interfaces.HttpService;
using MyWallet.Interfaces.LocalDatabase;
using MyWallet.Managers;
using MyWallet.Models;
using MyWallet.Models.Servers;
using MyWallet.ResourcesTranslation;
using MyWallet.Services.HttpService;
using Plugin.Connectivity;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace MyWallet.ViewModels.MasterDetailPage
{
    public class MainPageViewModel : BaseViewModel
    {
        public static MainPageViewModel Instance { get; private set; }
        public MainPageViewModel(INavigationService navService, ISQLiteService sqLiteService, IHttpRequest httpRequest, IPageDialogService dialogService)
            : base(navigationService: navService, sqLiteService: sqLiteService, httpRequest: httpRequest, dialogService: dialogService)
        {
            Title = "Main Page";

            SearchFilterCommand = new DelegateCommand(async () => await SearchFilterCommandExecute());
            SyncWalletCommand = new DelegateCommand(async () => await SyncWalletCommandExecute());

            AddNewTransactionsCommand = new DelegateCommand(async () => await AddNewTransactionsCommandExecute());

            Instance = this;
        }

        #region Properties

        public int _totalTransactions;

        private ObservableCollection<Transaction> _transactions;
        public ObservableCollection<Transaction> Transactions
        {
            get => _transactions;
            set
            {
                SetProperty(ref _transactions, value);
                _totalTransactions =  Transactions.Count;
                if (_transactions != null && value != null)
                    UpdateIncomesAndOutcomes();
            }
        }

        private ObservableCollection<string> _months;
        public ObservableCollection<string> Months
        {
            get => _months;
            set => SetProperty(ref _months, value);
        }

        private string _monthSelected;
        public string MonthSelected
        {
            get => _monthSelected;
            set => SetProperty(ref _monthSelected, value);
        }

        private string _incomes;
        public string Incomes
        {
            get => _incomes;
            set => SetProperty(ref _incomes, value);
        }

        private string _outcomes;
        public string Outcomes
        {
            get => _outcomes;
            set => SetProperty(ref _outcomes, value);
        }

        #endregion

        #region OnFirstTimeAppear

        public override async void  OnFirstTimeAppear()
        {
            DisplayThisMonth();
            UpdateIncomesAndOutcomes();

            var keyMonths = new ObservableCollection<string>(Enum.GetNames(typeof(EnumMonths)));

            var months = new ObservableCollection<string>();
            foreach (var month in keyMonths)
                months.Add(TranslateExtension.Get(month));

            Months = new ObservableCollection<string>(months);

            MonthSelected = TranslateExtension.Get(((EnumMonths) (DateTime.Now.Month - 1)).ToString());

            await GetListTransFromServer();
        }

        #endregion

        #region OnAppearing

        public override async void OnAppearing()
        {
            if (!string.IsNullOrEmpty(App.Settings.UserName) && CrossConnectivity.Current.IsConnected && App.TempTransactions.Any())
            {
                while (App.TempTransactions.Any())
                {
                    var item = App.TempTransactions.FirstOrDefault();
                    await PublishTrans(transactionTemp: item);
                }
            }
        }

        #endregion

        #region GetListTransFromServer

        private async Task GetListTransFromServer()
        {
            await Task.Run(async () =>
            {
                var url = ApiUrl.GetListTrans(App.Settings.UserName);
                
                var response = await HttpRequest.GetTaskAsync<JArrayResponse>(url);

                await GetListTransFromServerCallBack(response);
            });
        }

        private async Task GetListTransFromServerCallBack(JArrayResponse response)
        {
            if (response == null)
            {
                await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                    TranslateExtension.Get(TranslateManager.CheckInternet),
                    null, TranslateExtension.Get(TranslateManager.Close));
            }
            else
            {
                if (!response.Error.StringToBool())
                {
                    var result = response.Result.ToYourObject<ObservableCollection<Transaction>>();

                    Database.DeleteAll<Transaction>();

                    Database.InsertAll(result);

                    DisplayThisMonth();
                }
            }
        }

        #endregion

        #region PostTrans

        private async Task PublishTrans(TransactionTemp transactionTemp)
        {
            await Task.Run(async () =>
            {
                var url = ApiUrl.PostTrans();

                var httpContent = transactionTemp.ObjectToStringContent();
                var response = await HttpRequest.PostTaskAsync<JObjectResponse>(url, httpContent);

                PublishTransCallBack(response);

            });
        }

        private void PublishTransCallBack(JObjectResponse response)
        {
            if (response == null)
            {
            }
            else
            {
                if (!response.Error.StringToBool())
                {
                    var result = response.Result.ToYourObject<TransactionTemp>();

                    App.TempTransactions.Remove(result);
                }
            }
        }

        #endregion

        #region Override Navigate back to

        public override void OnNavigatedBackTo(NavigationParameters parameters)
        {
            if (parameters != null && parameters.ContainsKey(KeyParameters.UpdateTransaction.ToString()))
            {
                var trans = (Transaction)parameters[KeyParameters.UpdateTransaction.ToString()];
                UpdateTransToDisplay(newTrans: trans);
            }

        }

        #endregion

        #region SyncWalletCommand

        public ICommand SyncWalletCommand { get; }

        private async Task SyncWalletCommandExecute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            if (string.IsNullOrEmpty(App.Settings.UserName))
            {
                await NavigationService.NavigateAsync(
                    new Uri($"{PageManager.NavigationHomeUri}{PageManager.LoginPage}"));
            }
            else
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                        TranslateExtension.Get(TranslateManager.CheckInternet),
                        null, TranslateExtension.Get(TranslateManager.Close));
                }
                else if (App.TempTransactions.Any())
                {
                    while (App.TempTransactions.Any())
                    {
                        var item = App.TempTransactions.FirstOrDefault();
                        await PublishTrans(transactionTemp: item);
                    }
                }
            }

            IsBusy = false;
        }

        #endregion

        #region SearchFilterCommand

        public ICommand SearchFilterCommand { get; }

        private async Task SearchFilterCommandExecute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            await NavigationService.NavigateAsync(PageManager.SearchFilterPage);

            IsBusy = false;
        }

        #endregion

        #region AddNewTransactionsCommand

        public ICommand AddNewTransactionsCommand { get; }

        private async Task AddNewTransactionsCommandExecute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            var navParam = new NavigationParameters
            {
                {KeyParameters.NextId.ToString(), Transactions.Count},
            };

            await NavigationService.NavigateAsync(PageManager.AddTransactionPage, parameters: navParam);

            IsBusy = false;
        }

        #endregion

        #region DisplayThisMonth

        private void DisplayThisMonth()
        {
            Transactions = new ObservableCollection<Transaction>(Database.GetList<Transaction>(t => t.Id >= 0)
                .Where(t => t.CreatedDate.Month == DateTime.Now.Month && t.CreatedDate.Year == DateTime.Now.Year)
                .OrderByDescending(t => t.CreatedDate));

            //Transactions = new ObservableCollection<Transaction>(Transactions
            //    .Where(t => t.CreatedDate.Month == DateTime.Now.Month && t.CreatedDate.Year == DateTime.Now.Year));
        }

        #endregion

        #region GoToDetailTransaction

        public async void GoToDetailTransaction(Transaction transaction)
        {
            if (transaction != null)
            {
                NavigationParameters param = new NavigationParameters
                {
                    { nameof(Transaction), transaction}
                };
                await NavigationService.NavigateAsync(PageManager.DetailTransactionPage, param);
            }
            
        }

        #endregion

        #region UpdateTransToDisplay

        private void UpdateTransToDisplay(Transaction newTrans)
        {
            if (newTrans.CreatedDate.Month == DateTime.Now.Month && newTrans.CreatedDate.Year == DateTime.Now.Year)
            {
                Transactions.Add(newTrans);
                Transactions = new ObservableCollection<Transaction>(Transactions.OrderByDescending(t => t.CreatedDate));
            }
        }

        #endregion

        #region UpdateAndShowTransByMonth

        public void UpdateAndShowTransByMonth(string monthSelected, int numberOfMonth)
        {
            MonthSelected = monthSelected;

            Transactions = new ObservableCollection<Transaction>(Database.GetList<Transaction>(t => t.Id >= 0)
                .Where(t => t.CreatedDate.Month == numberOfMonth && t.CreatedDate.Year == DateTime.Now.Year)
                .OrderByDescending(t => t.CreatedDate));
        }

        #endregion

        #region Update Incomes & Outcomes

        private void UpdateIncomesAndOutcomes()
        {
            var unit = (App.Settings.Language == (int)LanguageType.Vietnamese) ? "₫" : "$";

            var incomeSum = Transactions.Where(t => t.TransactionType == TransactionType.Income)
                .Sum(t => Double.Parse(Regex.Replace(t.Amount, @"[^0-9/]+", "")));
            Incomes = incomeSum.ToString("N0", new CultureInfo("en-US")) + " " + unit;

            var outcomeSum = Transactions.Where(t => t.TransactionType == TransactionType.Outcome)
                .Sum(t => Double.Parse(Regex.Replace(t.Amount, @"[^0-9/]+", "")));
            Outcomes = outcomeSum.ToString("N0", new CultureInfo("en-US")) + " " + unit;
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MyWallet.Enums;
using MyWallet.Interfaces.LocalDatabase;
using MyWallet.Managers;
using MyWallet.Models;
using MyWallet.ResourcesTranslation;
using Prism.Commands;
using Prism.Navigation;

namespace MyWallet.ViewModels.MasterDetailPage
{
    public class MenuPageViewModel : BaseViewModel
    {
        public MenuPageViewModel(INavigationService navService, ISQLiteService sqLiteService)
            : base(navigationService: navService, sqLiteService: sqLiteService)
        {
            Title = "Menu";

            LoginCommand = new DelegateCommand(async () => { await LoginExecute(); });

            OpenMainPageCommand = new DelegateCommand(async() => await OpenMainPageCommandExecute());

            OpenCategoriesCommand = new DelegateCommand(async () => await OpenCategoriesCommandExecute());

            SettingsCommand = new DelegateCommand(SettingsCommandExecute);

            OpenStaticsCommand = new DelegateCommand(async () =>
            {
                OpenStaticsExecute();
            });

            LogoutCommand = new DelegateCommand(async () => { await LogoutCommandExecute(); });

            //LoginOrUserName = !string.IsNullOrEmpty(App.Settings.FullName)
            //    ? App.Settings.FullName : TranslateExtension.Get(TranslateManager.Login);
        }

        #region Properties

        private string _loginOrUserName;
        public string LoginOrUserName
        {
            get => _loginOrUserName;
            set => SetProperty(ref _loginOrUserName, value);
        }

        #endregion

        #region OnFirstTimeAppear

        public override async void OnFirstTimeAppear()
        {
            LoginOrUserName = !string.IsNullOrEmpty(App.Settings.FullName)
                ? App.Settings.FullName : TranslateExtension.Get(TranslateManager.Login);
        }

        #endregion


        #region Login

        public ICommand LoginCommand { get; }

        private async Task LoginExecute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            if (string.IsNullOrEmpty(App.Settings.UserName))
                await NavigationService.NavigateAsync(
                    new Uri($"{PageManager.NavigationHomeUri}{PageManager.LoginPage}"));

            IsBusy = false;
        }

        #endregion

        #region OpenMainPageCommand

        public ICommand OpenMainPageCommand { get; }

        private async Task OpenMainPageCommandExecute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            await NavigationService.NavigateAsync($"{PageManager.NavigationPage}/{PageManager.MainPage}");

            IsBusy = false;
        }

        #endregion

        #region OpenCategoriesCommand

        public ICommand OpenCategoriesCommand { get; }

        private async Task OpenCategoriesCommandExecute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            var navparameters = new NavigationParameters
            {
                {KeyParameters.Title.ToString(), TranslateExtension.Get(TranslateManager.Categories)},
                {PageMode.OnlyView.ToString(), true},
            };
            await NavigationService.NavigateAsync($"{PageManager.NavigationPage}/{PageManager.CategoriesTabbedPage}", parameters: navparameters);

            IsBusy = false;
        }

        #endregion

        #region OpenStaticsCommand

        public ICommand OpenStaticsCommand { get; }

        private async void OpenStaticsExecute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            //await NavigationService.NavigateAsync(PageManager.StatisticsPage);
            await NavigationService.NavigateAsync($"{PageManager.NavigationPage}/{PageManager.StatisticsPage}");


           IsBusy = false;
        }

        #endregion

        #region Settings

        public ICommand SettingsCommand { get; }
        private async void SettingsCommandExecute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            await NavigationService.NavigateAsync($"{PageManager.NavigationPage}/{PageManager.SettingsPage}");

            IsBusy = false;
        }

        #endregion

        #region LogoutCommand

        public ICommand LogoutCommand { get; set; }
        private async Task LogoutCommandExecute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            Database.DeleteAll<AppSettings>();
            Database.DeleteAll<Transaction>();
            Database.DeleteAll<TransactionTemp>();

            App.Settings = Database.GetSettings();

            await NavigationService.NavigateAsync(new Uri($"{PageManager.NavigationHomeUri}{PageManager.LoginPage}"));

            IsBusy = false;
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MyWallet.Enums;
using MyWallet.Helpers;
using MyWallet.Managers;
using MyWallet.Models;
using MyWallet.ResourcesTranslation;
using MyWallet.Views.Popups;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace MyWallet.ViewModels.SearchFilterFlow
{
    public class SearchFilterPageViewModel : BaseViewModel
    {
        public SearchFilterPageViewModel(INavigationService navService, IPageDialogService pageDialogService)
            : base(navigationService: navService, dialogService: pageDialogService)
        {
            Title = TranslateExtension.Get(TranslateManager.SearchFilter);
            SearchFilterCommand = new DelegateCommand(async () =>
            {
                await SearchFilterCommandExecute();
            });
        }

        #region OnAppearing

        public override void OnAppearing()
        {
            if (ShowCategory == CategorySelected &&
                CategorySelected == TranslateExtension.Get(TranslateManager.CustomCategory))
            {
                ShowCategory = CategorySelected = _flagCategorySelected;
            }
        }

        #endregion

        #region Override Navigate back to
        
        public override void OnNavigatedBackTo(NavigationParameters parameters)
        {
            if (parameters != null && parameters.ContainsKey(KeyParameters.CategorySelected.ToString()))
            {
                CategorySelected = (string)parameters[KeyParameters.CategorySelected.ToString()];
            }

            if (parameters != null && parameters.ContainsKey(KeyParameters.TransactionType.ToString()))
            {
                _transactionType = (TransactionType)parameters[KeyParameters.TransactionType.ToString()];
            }

        }

        #endregion

        #region Properties

        private bool _isLoading = false;

        private TransactionType _transactionType;

        #region Amount

        private bool _needToShowPopup = true;

        private ObservableCollection<string> _amountFilterOptions = DataSearchFilter.GetAmountsFilter();
        public ObservableCollection<string> AmountFilterOptions
        {
            get => _amountFilterOptions;
            set => SetProperty(ref _amountFilterOptions, value);
        }

        private string _amount;
        public string Amount
        {
            get => _amount;
            set => SetProperty(ref _amount, value);
        }

        private string _amountFilter;
        public string AmountFilter
        {
            get { return _amountFilter; }
            set
            {
                SetProperty(ref _amountFilter, value);
                if (_isLoading)
                    return;
                if (_needToShowPopup)
                    UpdateShowAmount();
                else
                    _needToShowPopup = !_needToShowPopup;
            }
        }

        private string _showAmount;
        public string ShowAmount
        {
            get => _showAmount;
            set => SetProperty(ref _showAmount, value);
        }

        private ModelInputTwoArgumentsPopup _resultInputTwoArguments;
        public ModelInputTwoArgumentsPopup ResultInputTwoArguments
        {
            get => _resultInputTwoArguments;
            set => SetProperty(ref _resultInputTwoArguments, value);
        }

        #endregion

        #region Category Filter

        private ObservableCollection<string> _categoryFilterOptions = DataSearchFilter.GetTypeTransactionFilter();
        public ObservableCollection<string> CategoryFilterOptions
        {
            get => _categoryFilterOptions;
            set => SetProperty(ref _categoryFilterOptions, value);
        }

        private string _categorySelected;
        public string CategorySelected
        {
            get => _categorySelected;
            set
            {
                SetProperty(ref _categorySelected, value);
                UpdateShowCategories();
                UpdateTransactionType();
            } 
        }

        private string _showCategory;
        public string ShowCategory
        {
            get => _showCategory;
            set => SetProperty(ref _showCategory, value);
        }

        #endregion

        #region Date Filter

        private ObservableCollection<string> _dateFilterOptions = DataSearchFilter.GetDatesFilter();
        public ObservableCollection<string> DateFilterOptions
        {
            get => _dateFilterOptions;
            set => SetProperty(ref _dateFilterOptions, value);
        }

        private string _dateSelected;
        public string DateSelected
        {
            get => _dateSelected;
            set
            {
                SetProperty(ref _dateSelected, value);
                if (_isLoading)
                    return;
                if (_needToShowPopup)
                    UpdateShowDates();
                else
                    _needToShowPopup = !_needToShowPopup;
            }
        }

        private string _date;
        public string Date
        {
            get => _date;
            set => SetProperty(ref _date, value);
        }

        private string _showDate;
        public string ShowDate
        {
            get => _showDate;
            set => SetProperty(ref _showDate, value);
        }

        private ModelInputTwoArgumentsPopup _resultInputTwoDates;
        public ModelInputTwoArgumentsPopup ResultInputTwoDates
        {
            get => _resultInputTwoDates;
            set => SetProperty(ref _resultInputTwoDates, value);
        }

        #endregion

        #region Note

        private string _note;
        public string Note
        {
            get => _note;
            set => SetProperty(ref _note, value);
        }

        #endregion

        #endregion

        #region UpdateShowAmount

        private async void UpdateShowAmount()
        {
            if (string.IsNullOrEmpty(AmountFilter))
                return;

            if (AmountFilter == TranslateExtension.Get(TranslateManager.All))
            {
                ShowAmount = AmountFilter;
                return;
            }

            _isLoading = true;

            if (AmountFilter == TranslateExtension.Get(TranslateManager.Between))
            {
                ResultInputTwoArguments = await InputTwoArgumentsPopup.Instance.Show(title: $"{AmountFilter}");

                AmountFilter = (ResultInputTwoArguments == null) ? RevertAmountFilter() : AmountFilter;
                ShowAmount = (ResultInputTwoArguments == null)
                    ? ShowAmount
                    : $"{AmountFilter} {ResultInputTwoArguments.EntryFrom} - {ResultInputTwoArguments.EntryTo}";
                _isLoading = false;
                return;
            }

            var result = await InputOneArgumentPopup.Instance.Show(title: $"{AmountFilter}", valueInput: Amount);
            Amount = string.IsNullOrEmpty(result) ? Amount : result;
            AmountFilter = string.IsNullOrEmpty(result) ? RevertAmountFilter() : AmountFilter;
            ShowAmount = ($"{AmountFilter} {Amount}").Trim();

            _isLoading = false;

        }

        #endregion

        #region RevertAmountFilter

        private string RevertAmountFilter()
        {
            _needToShowPopup = false;
            if (string.IsNullOrEmpty(ShowAmount))
                return String.Empty;
            var arrayWords = ShowAmount.Split(' ');
            return $"{arrayWords[0]} {arrayWords[1]}";
        }

        #endregion

        #region UpdateShowCategories

        private string _flagCategorySelected = string.Empty;

        private async void UpdateShowCategories()
        {
            if (string.IsNullOrEmpty(CategorySelected))
                return;

            if (CategorySelected == TranslateExtension.Get(TranslateManager.CustomCategory))
            {
                var navparameters = new NavigationParameters
                {
                    {
                        KeyParameters.Title.ToString(), CategorySelected
                    },
                };
                await NavigationService.NavigateAsync(PageManager.CategoriesTabbedPage, parameters: navparameters);

                ShowCategory = CategorySelected;

                return;
            }

            ShowCategory = CategorySelected;
            _flagCategorySelected = ShowCategory;
        }

        #endregion

        #region UpdateTransactionType

        private void UpdateTransactionType()
        {
            if (CategorySelected == TranslateExtension.Get(TranslateManager.All))
                _transactionType = TransactionType.None;
            if (CategorySelected == TranslateExtension.Get(TranslateManager.AllExpense))
                _transactionType = TransactionType.Outcome;
            if (CategorySelected == TranslateExtension.Get(TranslateManager.AllIncome))
                _transactionType = TransactionType.Income;
        }

        #endregion

        #region UpdateShowDates

        private async void UpdateShowDates()
        {
            if (string.IsNullOrEmpty(DateSelected))
                return;

            if (DateSelected == TranslateExtension.Get(TranslateManager.All))
            {
                ShowDate = DateSelected;
                return;
            }

            _isLoading = true;

            if (DateSelected == TranslateExtension.Get(TranslateManager.Between))
            {
                ResultInputTwoDates = await InputTwoDatePickersPopup.Instance.Show(title: $"{DateSelected}");

                DateSelected = (ResultInputTwoDates == null) ? RevertDateFilter() : DateSelected;
                if (ResultInputTwoDates != null)
                    Date = $"{ResultInputTwoDates.EntryFrom} - {ResultInputTwoDates.EntryTo}";
                ShowDate = (ResultInputTwoDates == null)
                    ? ShowDate
                    : $"{DateSelected} {Date}";
                _isLoading = false;
                return;
            }

            var result = await InputOneDatePickerPopup.Instance.Show(title: $"{DateSelected}", valueInput: Date);
            Date = string.IsNullOrEmpty(result) ? Date : result;
            DateSelected = string.IsNullOrEmpty(result) ? RevertDateFilter() : DateSelected;
            ShowDate = ($"{DateSelected} {Date}").Trim();
            _isLoading = false;

        }

        #endregion

        #region RevertDateFilter

        private int _lenghOfDate = 10;
        private int _lenghOfTwoDates = 23;

        private string RevertDateFilter()
        {
            _needToShowPopup = false;

            var dateTime = (!string.IsNullOrEmpty(ShowDate) && ShowDate.Contains("/"))
                ? ShowDate.Remove(ShowDate.Length - ((ShowDate.Contains("-") ? _lenghOfTwoDates : _lenghOfDate) + 1))
                : ShowDate;
            dateTime = dateTime?.Trim();
            return (string.IsNullOrEmpty(dateTime) ? null : dateTime);
        }

        #endregion

        #region ContinueCommand

        public ICommand SearchFilterCommand { get; }

        private async Task SearchFilterCommandExecute()
        {
            #region Check Empty

            if (string.IsNullOrEmpty(ShowAmount) || string.IsNullOrEmpty(ShowCategory) ||
                String.IsNullOrEmpty(ShowDate))
            {
                await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                    TranslateExtension.Get(TranslateManager.PleaseEnterAllInfo),
                    null, TranslateExtension.Get(TranslateManager.Close));
                return;
            }

            #endregion

            var searchFilterModel = new ModelSearchFilter()
            {
                Amount = ShowAmount,
                Category = ShowCategory,
                Date = ShowDate,
                Note = Note,
                TransactionType = _transactionType,
            };

            var navParam = new NavigationParameters
            {
                {KeyParameters.ResultSearchFilter.ToString(), searchFilterModel},
            };

            await NavigationService.NavigateAsync(PageManager.ViewSearchFilterPage, parameters: navParam);
        }

        #endregion
    }
}

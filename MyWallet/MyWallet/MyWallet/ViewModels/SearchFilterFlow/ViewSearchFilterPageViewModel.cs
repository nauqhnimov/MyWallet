﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MyWallet.Enums;
using MyWallet.Interfaces.LocalDatabase;
using MyWallet.Managers;
using MyWallet.Models;
using MyWallet.ResourcesTranslation;
using Prism.Navigation;
using Prism.Services;

namespace MyWallet.ViewModels.SearchFilterFlow
{
    public class ViewSearchFilterPageViewModel : BaseViewModel
    {
        public ViewSearchFilterPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, ISQLiteService sqLiteService)
            : base(navigationService: navigationService, dialogService: pageDialogService, sqLiteService: sqLiteService)
        {
            Title = TranslateExtension.Get(TranslateManager.ViewSearchFilter);
        }

        #region OnAppearing

        public override void OnAppearing()
        {
           
        }

        #endregion

        #region OnFirstTimeAppear

        public override void OnFirstTimeAppear()
        {

        }

        #endregion

        #region Override Navigate back to

        public override async void OnNavigatedNewTo(NavigationParameters parameters)
        {
            if (parameters != null && parameters.ContainsKey(KeyParameters.ResultSearchFilter.ToString()))
            {
                ParamModelSearchFilter = (ModelSearchFilter)parameters[KeyParameters.ResultSearchFilter.ToString()];

                await SearchFilterExcute(ParamModelSearchFilter);
            }
        }

        public override void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {

        }

        #endregion

        #region Properties

        private ModelSearchFilter _paramModelSearchFilter;
        public ModelSearchFilter ParamModelSearchFilter 
        {
            get { return _paramModelSearchFilter; }
            set { SetProperty(ref _paramModelSearchFilter, value); } 
        }

        private ObservableCollection<Transaction> _viewTransactions;
        public ObservableCollection<Transaction> ViewTransactions 
        {
            get { return _viewTransactions; }
            set { SetProperty(ref _viewTransactions, value); } 
        }

        #endregion

        #region SearchFilterExcute

        private async Task SearchFilterExcute(ModelSearchFilter modelSearchFilter)
        {
            var trans = new ObservableCollection<Transaction>(Database.GetList<Transaction>(t => t.Id >= 0));

            trans = await ExecuteByAmount(transactions: trans, amountFilter: modelSearchFilter.Amount);

            trans = await ExecuteByCategory(transactions: trans, categoryFilter: modelSearchFilter.Category);

            trans = await ExecuteByNote(transactions: trans, noteFilter: modelSearchFilter.Note);

            trans = await ExecuteByCreateDate(transactions: trans, dateFilter: modelSearchFilter.Date);

            ViewTransactions = new ObservableCollection<Transaction>(trans.OrderByDescending(t => t.CreatedDate));
        }

        #endregion

        #region ExecuteByAmount

        private async Task<ObservableCollection<Transaction>> ExecuteByAmount(ObservableCollection<Transaction> transactions, string amountFilter)
        {
            if (string.IsNullOrEmpty(amountFilter) || amountFilter == TranslateExtension.Get(TranslateManager.All))
                return transactions;

            var result = new ObservableCollection<Transaction>();

            if (!transactions.Any())
                return transactions;

            await Task.Run(() =>
            {
                var value = amountFilter.Replace(TranslateExtension.Get(TranslateManager.Between), "")
                    .Replace(TranslateExtension.Get(TranslateManager.Exact), "")
                    .Replace(TranslateExtension.Get(TranslateManager.Over), "")
                    .Replace(TranslateExtension.Get(TranslateManager.Under), "")
                    .Replace(" ", "");

                var number = Double.Parse(Regex.Replace(value, @"[^0-9/]+", ""));

                if (amountFilter.StartsWith(TranslateExtension.Get(TranslateManager.Exact)))
                {
                    result = new ObservableCollection<Transaction>(transactions.Where(t => t.Amount.Equals(value)));
                }
                else if (amountFilter.StartsWith(TranslateExtension.Get(TranslateManager.Over)))
                {
                    result = new ObservableCollection<Transaction>(
                        transactions.Where(t => (Double.Parse(Regex.Replace(t.Amount, @"[^0-9/]+", ""))) > number));
                }
                else if (amountFilter.StartsWith(TranslateExtension.Get(TranslateManager.Under)))
                {
                    result = new ObservableCollection<Transaction>(
                        transactions.Where(t => (Double.Parse(Regex.Replace(t.Amount, @"[^0-9/]+", ""))) < number));
                }
                else if (amountFilter.StartsWith(TranslateExtension.Get(TranslateManager.Between)))
                {
                    var value1 = value.Split('-')[0];
                    var value2 = value.Split('-')[1];

                    var number1 = Double.Parse(Regex.Replace(value1, @"[^0-9/]+", ""));
                    var number2 = Double.Parse(Regex.Replace(value2, @"[^0-9/]+", ""));

                    result = new ObservableCollection<Transaction>(
                        transactions.Where(t =>
                            ((Double.Parse(Regex.Replace(t.Amount, @"[^0-9/]+", ""))) > number1) &&
                            (Double.Parse(Regex.Replace(t.Amount, @"[^0-9/]+", ""))) < number2));
                }
            });

            return result;
        }

        #endregion

        #region ExecuteByCategory

        private async Task<ObservableCollection<Transaction>> ExecuteByCategory(ObservableCollection<Transaction> transactions, string categoryFilter)
        {
            if (string.IsNullOrEmpty(categoryFilter) || categoryFilter == TranslateExtension.Get(TranslateManager.All))
                return transactions;

            var result = new ObservableCollection<Transaction>();

            if (!transactions.Any())
                return transactions;

            await Task.Run(() =>
            {
                if (categoryFilter == TranslateExtension.Get(TranslateManager.AllIncome))
                {
                    result = new ObservableCollection<Transaction>(transactions.Where(t =>
                        t.TransactionType == TransactionType.Income));
                }
                else if (categoryFilter == TranslateExtension.Get(TranslateManager.AllExpense))
                {
                    result = new ObservableCollection<Transaction>(transactions.Where(t =>
                        t.TransactionType == TransactionType.Outcome));
                }
                else 
                {
                    result = new ObservableCollection<Transaction>(
                        transactions.Where(t => t.Category.Equals(categoryFilter)));
                }
            });

            return result;
        }

        #endregion

        #region ExecuteByNote

        private async Task<ObservableCollection<Transaction>> ExecuteByNote(
            ObservableCollection<Transaction> transactions, string noteFilter)
        {
            if (string.IsNullOrEmpty(noteFilter))
                return transactions;

            var result = new ObservableCollection<Transaction>();

            if (!transactions.Any())
                return transactions;

            await Task.Run(() =>
            {
                result = new ObservableCollection<Transaction>(
                    transactions.Where(t => t.Note.Equals(noteFilter)));
            });

            return result;
        }

        #endregion

        #region ExecuteByCreateDate

        private async Task<ObservableCollection<Transaction>> ExecuteByCreateDate(ObservableCollection<Transaction> transactions, string dateFilter)
        {
            if (string.IsNullOrEmpty(dateFilter) || dateFilter == TranslateExtension.Get(TranslateManager.All))
                return transactions;

            var result = new ObservableCollection<Transaction>();

            if (!transactions.Any())
                return transactions;

            await Task.Run(() =>
            {
                var value = dateFilter.Replace(TranslateExtension.Get(TranslateManager.Between), "")
                    .Replace(TranslateExtension.Get(TranslateManager.Exact), "")
                    .Replace(TranslateExtension.Get(TranslateManager.Over), "")
                    .Replace(TranslateExtension.Get(TranslateManager.Under), "")
                    .Replace(" ", "");

                var dateString = Regex.Replace(value, @"[^0-9/]+", "/");
                var date = DateTime.Parse(dateString);

                if (dateFilter.StartsWith(TranslateExtension.Get(TranslateManager.Exact)))
                {
                    result = new ObservableCollection<Transaction>(transactions.Where(t => t.CreatedDate == date));
                }
                else if (dateFilter.StartsWith(TranslateExtension.Get(TranslateManager.Over)))
                {
                    result = new ObservableCollection<Transaction>(transactions.Where(t => t.CreatedDate > date));
                }
                else if (dateFilter.StartsWith(TranslateExtension.Get(TranslateManager.Under)))
                {
                    result = new ObservableCollection<Transaction>(transactions.Where(t => t.CreatedDate < date));
                }
                else if (dateFilter.StartsWith(TranslateExtension.Get(TranslateManager.Between)))
                {
                    var value1 = value.Split('-')[0];
                    var value2 = value.Split('-')[1];

                    var dateString1 = Regex.Replace(value1, @"[^0-9/]+", "/");
                    var date1 = DateTime.Parse(dateString1);

                    var dateString2 = Regex.Replace(value2, @"[^0-9/]+", "/");
                    var date2 = DateTime.Parse(dateString2);

                    //var number1 = Double.Parse(Regex.Replace(value1, @"[^0-9/]+", ""));
                    //var number2 = Double.Parse(Regex.Replace(value2, @"[^0-9/]+", ""));

                    result = new ObservableCollection<Transaction>(transactions.Where(t =>
                        (date1 < t.CreatedDate) && (t.CreatedDate < date2)));
                }
            });

            return result;
        }

        #endregion

    }
}

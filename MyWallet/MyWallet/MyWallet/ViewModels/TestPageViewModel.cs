﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MyWallet.Managers;
using MyWallet.Views.Popups;
using Prism.Navigation;
using Xamarin.Forms;

namespace MyWallet.ViewModels
{
    public class TestPageViewModel : BaseViewModel
    {
        public TestPageViewModel(INavigationService navService)
            : base(navigationService: navService)
        {
            Title = "Menu";
            LoginFBCommand = new Command(async () => await LoginFBExecute());
        }

        #region LoginFB

        public Command LoginFBCommand { get; set; }
        private async Task LoginFBExecute()
        {
            await NavigationService.NavigateAsync($"{PageManager.NavigationPage}/{PageManager.SearchFilterPage}");
        }

        #endregion

    }
}

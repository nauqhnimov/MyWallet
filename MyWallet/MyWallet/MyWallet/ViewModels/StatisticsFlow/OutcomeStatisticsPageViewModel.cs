﻿using System;
using System.Collections.Generic;
using System.Text;
using Prism.Navigation;
using Prism.Services;

namespace MyWallet.ViewModels.StatisticsFlow
{
    public class OutcomeStatisticsPageViewModel: BaseViewModel
    {
        public OutcomeStatisticsPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
        }
    }
}

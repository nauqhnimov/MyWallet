﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using MyWallet.Interfaces.LocalDatabase;
using MyWallet.Managers;
using MyWallet.Models;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace MyWallet.ViewModels.StatisticsFlow
{
    public class StatisticsPageViewModel:BaseViewModel
    {
        public StatisticsPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, ISQLiteService databaService)
            : base(navigationService: navigationService, dialogService: pageDialogService, sqLiteService: databaService)
        {
            SetDayCommand = new DelegateCommand(OnSetDayExcute);
        }

        private const int InvalidYear = 1;

        private void OnSetDayExcute()
        {
            NavigationService.NavigateAsync(PageManager.ChooseDatePage, animated: true);
        }

        public ICommand SetDayCommand { get; set; }


        public override void OnNavigatedBackTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("startday") && parameters.ContainsKey("endday"))
            {

            }
        }

        public override void OnNavigatedNewTo(NavigationParameters parameters)
        {
            
        }

        public  List<Transaction> GetTransactions(DateTime startDate = new DateTime(), DateTime endDate = new DateTime() )
        {
            List<Transaction> Transactions = Database.GetList<Transaction>(i => i.Id >= 0).ToList();

            if (startDate.Year == InvalidYear && endDate.Year == InvalidYear)
            {
                return new List<Transaction>();
            }
            else if (startDate.Year == InvalidYear)
            {
                return GetTransactionsBefore(Transactions, endDate);
            }
            else if (endDate.Year == InvalidYear)
            {
                return GetTransactionsAfter(Transactions, startDate);
            }
            else
            {
                return GetTransactionsBettwen(Transactions, startDate, endDate);
            }
            
        }

        private List<Transaction> GetTransactionsBettwen(List<Transaction> transactions, DateTime startDate , DateTime endDate)
        {
            //return new List<Transaction>(Database.GetList<Transaction>(i => i.Id >=0
            //    /*i.CreatedDate.Ticks > startDate.Ticks && i.CreatedDate.Ticks < endDate.Ticks*/));

            return transactions.Where(i => i.CreatedDate.CompareTo(startDate) >= 0 && i.CreatedDate.CompareTo(endDate) <= 0).ToList();
        }

        private List<Transaction> GetTransactionsBefore(List<Transaction> transactions, DateTime endDate )
        {
            //return new List<Transaction>(Database.GetList<Transaction>(i => i.CreatedDate.CompareTo(endDate) < 0));
            return transactions.Where(i => i.CreatedDate.CompareTo(endDate) <= 0).ToList();
        }

        private List<Transaction> GetTransactionsAfter(List<Transaction> transactions, DateTime startDate)
        {
            //return new List<Transaction>(Database.GetList<Transaction>(i => i.CreatedDate.CompareTo(startDate) > 0 ));
            return transactions.Where(i => i.CreatedDate.CompareTo(startDate) >= 0).ToList();
        }

    }
}

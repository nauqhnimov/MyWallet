﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using MyWallet.Managers;
using MyWallet.ResourcesTranslation;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace MyWallet.ViewModels.StatisticsFlow
{
    public class ChooseDatePageViewModel :BaseViewModel
    {
        private DateTime _startDay;

        public DateTime StartDay
        {
            get => _startDay;
            set => SetProperty(ref _startDay, value);
        }

        private DateTime _endDay;

        public DateTime EndDay
        {
            get => _endDay;
            set => SetProperty(ref _endDay, value);
        }


        public ChooseDatePageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            StartDay = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            EndDay = DateTime.Now;


            ViewChartCommand = new DelegateCommand(OnViewChartExcute);
        }

        private async void OnViewChartExcute()
        {
            if (IsBusy) return;
            IsBusy = true;

            if (StartDay.CompareTo(EndDay) < 0)
            {
                var p = new NavigationParameters();
                p.Add("startday", StartDay);
                p.Add("endday", EndDay);
                App.StartDateForStatistic = this.StartDay;
                App.EndDateForStatistic = EndDay;
                //await NavigationService.NavigateAsync($"{PageManager.NavigationPage}/{PageManager.StatisticsPage}", p);
                //await NavigationService.NavigateAsync(PageManager.StatisticsPage, p);
                await NavigationService.GoBackAsync(p);
            }
            else
            {
                await DialogService.DisplayAlertAsync(TranslateExtension.Get("Message"),
                    TranslateExtension.Get("StartDayEndDayInvalid"), TranslateExtension.Get("Dismiss"));
            }

            IsBusy = false;
        }

        public ICommand ViewChartCommand { get; set; }
    }
}

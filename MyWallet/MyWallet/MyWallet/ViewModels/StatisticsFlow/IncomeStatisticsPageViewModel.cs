﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyWallet.Interfaces.LocalDatabase;
using MyWallet.Models;
using Prism.Navigation;
using Prism.Services;

namespace MyWallet.ViewModels.StatisticsFlow
{
    public class IncomeStatisticsPageViewModel : BaseViewModel
    {
        private List<Transaction> _listKhoangThu;

        public List<Transaction> ListKhoangThu
        {
            get => _listKhoangThu;
            set => SetProperty(ref _listKhoangThu, value);
        }

        public IncomeStatisticsPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            ListKhoangThu = new List<Transaction>();
        }

        public async override void OnNavigatedNewTo(NavigationParameters parameters)
        {


            if (parameters.ContainsKey("startday") && parameters.ContainsKey("endday"))
            {
                DateTime StartedDay = parameters.GetValue<DateTime>("startday");
                DateTime EndedDay = parameters.GetValue<DateTime>("endday");

              

            }




        }
    }
}

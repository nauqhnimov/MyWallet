﻿using System;
using System.Collections.Generic;
using System.Text;
using Microcharts;
using Microcharts.Forms;
using Prism.Navigation;
using Prism.Services;

namespace MyWallet.ViewModels.StatisticsFlow
{
    public class GeneralStatisticsPageViewModel :BaseViewModel
    {
        public GeneralStatisticsPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
          
        }

        #region OnNavigateNewTo

        public override void OnNavigatedNewTo(NavigationParameters parameters)
        {
            base.OnNavigatedNewTo(parameters);

           
        }

        #endregion

        #region property

        private Microcharts.BarChart _chart;

        public BarChart Chart
        {
            get => _chart;
            set => SetProperty(ref _chart, value);
        }

        private List<Entry> _entries;

        public List<Entry> Entries
        {
            get => _entries;
            set => SetProperty(ref _entries, value);
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using MyWallet.Enums;
using MyWallet.Helpers;
using Prism.Navigation;

namespace MyWallet.ViewModels.CommonPages
{
    public class CategoriesIncomePageViewModel : BaseViewModel
    {
        public CategoriesIncomePageViewModel(INavigationService navService)
            : base(navigationService: navService)
        {
        }

        #region OnFirstTimeAppear

        public override void OnFirstTimeAppear()
        {
            UpdateSource();
        }

        #endregion

        #region Properties

        private ObservableCollection<string> _incomeCategories;
        public ObservableCollection<string> IncomeCategories
        {
            get => _incomeCategories;
            set => SetProperty(ref _incomeCategories, value);
        }

        #endregion

        #region UpdateSource

        private void UpdateSource()
        {
            IncomeCategories = DataSearchFilter.GetIncomeCategories();
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MyWallet.Enums;
using Prism.Navigation;

namespace MyWallet.ViewModels.CommonPages
{
    public class CategoriesTabbedPageViewModel : BaseViewModel
    {
        public static CategoriesTabbedPageViewModel Instance { get; private set; }

        public CategoriesTabbedPageViewModel(INavigationService navService)
            : base(navigationService: navService)
        {
            Instance = this;
        }

        #region Override Navigate new to

        public override void OnNavigatedNewTo(NavigationParameters parameters)
        {
            if (parameters != null && parameters.ContainsKey(KeyParameters.Title.ToString()))
            {
                Title = (string)parameters[KeyParameters.Title.ToString()];
            }

            if (parameters != null && parameters.ContainsKey(Enums.PageMode.OnlyView.ToString()))
            {
                PageMode = (bool)parameters[Enums.PageMode.OnlyView.ToString()];
            }

        }

        #endregion

        #region Properties

        private bool _pageMode;
        public bool PageMode
        {
            get => _pageMode;
            set => SetProperty(ref _pageMode, value);
        }

        #endregion

        #region ListItemTapped

        public async Task ListItemTapped(string categorySelectedItem, TransactionType transactionType)
        {
            var navparameters = new NavigationParameters
            {
                {KeyParameters.CategorySelected.ToString(), categorySelectedItem},
                {KeyParameters.TransactionType.ToString(), transactionType},
            };
            await NavigationService.GoBackAsync(parameters: navparameters);
        }

        #endregion

        #region OnBackButtonPressed

        public override bool OnBackButtonPressed()
        {
            Task.Run(async () =>
            {
                var navparameters = new NavigationParameters
                {
                    {
                        KeyParameters.CategorySelected.ToString(), null
                    },
                };
                await NavigationService.GoBackAsync(parameters: navparameters);
            });

            return true;
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using MyWallet.Enums;
using MyWallet.Helpers;
using MyWallet.Managers;
using Prism.Navigation;

namespace MyWallet.ViewModels.CommonPages
{
    public class CategoriesOutcomePageViewModel : BaseViewModel
    {
        public CategoriesOutcomePageViewModel(INavigationService navService)
            : base(navigationService: navService)
        {
        }

        #region OnFirstTimeAppear

        public override void OnFirstTimeAppear()
        {
            UpdateSource();
        }

        #endregion

        #region Properties

        private ObservableCollection<string> _outcomeCategories;
        public ObservableCollection<string> OutcomeCategories
        {
            get => _outcomeCategories;
            set => SetProperty(ref _outcomeCategories, value);
        }

        #endregion

        #region UpdateSource

        private void UpdateSource()
        {
            OutcomeCategories = DataSearchFilter.GetOutcomeCategories();
        }

        #endregion

    }
}

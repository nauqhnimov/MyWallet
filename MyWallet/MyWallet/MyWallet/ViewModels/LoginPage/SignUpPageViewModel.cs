﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MyWallet.Helpers;
using MyWallet.Interfaces.HttpService;
using MyWallet.Interfaces.LocalDatabase;
using MyWallet.Managers;
using MyWallet.Models;
using MyWallet.Models.Servers;
using MyWallet.ResourcesTranslation;
using MyWallet.Services.HttpService;
using MyWallet.Views.Popups;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace MyWallet.ViewModels.LoginPage
{
    public class SignUpPageViewModel : BaseViewModel
    {
        public SignUpPageViewModel(IPageDialogService dialogService, INavigationService navService,
            ISQLiteService sqLiteService, IHttpRequest httpRequest)
            : base(dialogService: dialogService, navigationService: navService, sqLiteService: sqLiteService,
                httpRequest: httpRequest)
        {
            Title = TranslateExtension.Get(TranslateManager.SignUp);

            SignUpCommand = new DelegateCommand(async () => { await SignUpCommandExecute(); });
            LoginCommand = new Command(async () => await LoginCommandExecute());
        }

        #region Properties

        private string _fullName;
        public string FullName
        {
            get { return _fullName; }
            set { SetProperty(ref _fullName, value); }
        }

        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set { SetProperty(ref _userName, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        #endregion

        #region SignUpCommand

        public ICommand SignUpCommand { get; set; }
        private async Task SignUpCommandExecute()
        {
            //Check Empty
            if (await IsCheckEmpty())
                return;

            // Call API SignUp
            await LoadingPopup.Instance.Show(TranslateExtension.Get("SigningUp"));

            await CallApiSignUp();

            await LoadingPopup.Instance.Hide();
        }

        #endregion

        #region CallApiSignUp

        private async Task CallApiSignUp()
        {
            await Task.Run(async () =>
            {
                var url = ApiUrl.UserSignup();

                var user = new AccountUser
                {
                    FullName = FullName,
                    UserName = UserName,
                    Password = Password,
                };
                //param.Password = param.CryptPassword(Password);

                var httpContent = user.ObjectToStringContent();
                var response = await HttpRequest.PostTaskAsync<JObjectResponse>(url, httpContent);
                await SignUpCallBack(response);
            });
        }

        private async Task SignUpCallBack(JObjectResponse response)
        {
            if (response == null)
            {
                // login fail
                await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                    TranslateExtension.Get(TranslateManager.CheckInternet),
                    null, TranslateExtension.Get(TranslateManager.Close));
                //await MessagePopup.Instance.Show(TranslateExtension.Get("Username_PasswordIncorrect"));
            }
            else
            {
                if (!response.Error.StringToBool())
                {
                    var result = response.Result.ToYourObject<AccountUser>();

                    UpdateAppSettings(result);

                    await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
                    {
                        await NavigationService.NavigateAsync(PageManager.MainPageUri());
                    });

                }
                else
                {
                    await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
                    {
                        if (response.ErrorCode == 403)
                            await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                                TranslateExtension.Get(TranslateManager.AlreadyAccount),
                                null, TranslateExtension.Get(TranslateManager.Close));
                    });
                }
            }
        }

        #endregion

        #region UpdateAndSaveAppSettings

        private void UpdateAppSettings(AccountUser accountUser)
        {
            App.Settings.FullName = accountUser.FullName;
            App.Settings.UserName = accountUser.UserName;

            Database.Update(App.Settings);
        }

        #endregion

        #region CheckEmpty

        public async Task<bool> IsCheckEmpty()
        {
            if (string.IsNullOrWhiteSpace(UserName) && string.IsNullOrEmpty(Password) && string.IsNullOrEmpty(FullName))
            {
                await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                    TranslateExtension.Get(TranslateManager.PleaseEnterAllInfo),
                    null, TranslateExtension.Get(TranslateManager.Close));

                return true;
            }

            if (string.IsNullOrWhiteSpace(FullName))
            {
                await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                    TranslateExtension.Get(TranslateManager.PleaseEnterFullName),
                    null, TranslateExtension.Get(TranslateManager.Close));

                return true;
            }

            if (string.IsNullOrWhiteSpace(UserName))
            {
                await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                    TranslateExtension.Get(TranslateManager.PleaseEnterUserName),
                    null, TranslateExtension.Get(TranslateManager.Close));

                return true;
            }

            if (string.IsNullOrEmpty(Password))
            {
                await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                    TranslateExtension.Get(TranslateManager.PleaseEnterPassword),
                    null, TranslateExtension.Get(TranslateManager.Close));

                return true;
            }

            return false;
        }

        #endregion

        #region LoginCommand

        public Command LoginCommand { get; set; }
        private async Task LoginCommandExecute()
        {
            await NavigationService.GoBackAsync();
        }

        #endregion

    }
}

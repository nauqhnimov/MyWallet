﻿using System.Threading.Tasks;
using System.Windows.Input;
using MyWallet.Helpers;
using MyWallet.Interfaces.HttpService;
using MyWallet.Interfaces.LocalDatabase;
using MyWallet.Managers;
using MyWallet.Models;
using MyWallet.Models.Servers;
using MyWallet.ResourcesTranslation;
using MyWallet.Services.FacebookApiService;
using MyWallet.Services.HttpService;
using MyWallet.Views.Popups;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace MyWallet.ViewModels.LoginPage
{
    public class LoginPageViewModel : BaseViewModel
    {
        public LoginPageViewModel(IPageDialogService dialogService, INavigationService navService, ISQLiteService sqLiteService, IHttpRequest httpRequest) 
            : base(dialogService: dialogService, httpRequest: httpRequest,
                  navigationService: navService, sqLiteService: sqLiteService)
        {
            Title = TranslateExtension.Get(TranslateManager.Login);

            TapSkipCommand = new DelegateCommand(SkipExecute);
            LoginCommand = new DelegateCommand(async () => { await LoginExecute(); });
            LoginFBCommand = new Command(async () => await LoginFBExecute());
            SignUpCommand = new Command(async () => await SignUpCommandExecute());
        }

        #region Properties

        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set { SetProperty(ref _userName, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private WebView _webViewOverlay;
        public WebView WebViewOverlay
        {
            get => _webViewOverlay;
            set => SetProperty(ref _webViewOverlay, value);
        }

        private bool _isVisibleWebView = false;
        public bool IsVisibleWebView
        {
            get => _isVisibleWebView;
            set => SetProperty(ref _isVisibleWebView, value);
        }

        #endregion

        #region LoginCommand

        public ICommand LoginCommand { get; set; }
        private async Task LoginExecute()
        {
            //Check Empty
            if (await IsCheckEmpty())
                return;

            // Call API Login
            await LoadingPopup.Instance.Show(TranslateExtension.Get("LoggingIn"));

            await CallApiLogin();

            await LoadingPopup.Instance.Hide();
            //if (CheckAccount())
            //{
            //    UpdateAppSettings();

            //    await NavigationService.NavigateAsync(PageManager.MainPageUri());

            //}
            //else
            //{
            //    UserName = "";
            //    Password = "";
            //    await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
            //        TranslateExtension.Get(TranslateManager.IncorrectAndAgain),
            //        null, TranslateExtension.Get(TranslateManager.Close));
            //}
        }

        #endregion

        #region CallApiLogin

        private async Task CallApiLogin()
        {
            await Task.Run(async () =>
            {
                //await LoadingPopup.Instance.Show();

                var url = ApiUrl.UserLogin();

                var user = new AccountUser
                {
                    UserName = UserName,
                    Password = Password,
                };
                //param.Password = param.CryptPassword(Password);

                var httpContent = user.ObjectToStringContent();
                var response = await HttpRequest.PostTaskAsync<JObjectResponse>(url, httpContent);
                await LoginCallBack(response);

                //await LoadingPopup.Instance.Hide();
            });
        }

        private async Task LoginCallBack(JObjectResponse response)
        {
            if (response == null)
            {
                // login fail
                await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                    TranslateExtension.Get(TranslateManager.CheckInternet),
                    null, TranslateExtension.Get(TranslateManager.Close));
                //await MessagePopup.Instance.Show(TranslateExtension.Get("Username_PasswordIncorrect"));
            }
            else
            {
                if (!response.Error.StringToBool())
                {
                    var result = response.Result.ToYourObject<AccountUser>();

                    UpdateAppSettings(result);

                    await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
                    {
                        await NavigationService.NavigateAsync(PageManager.MainPageUri());
                    });
                }
                else
                {
                    UserName = "";
                    Password = "";
                    await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                        TranslateExtension.Get(TranslateManager.IncorrectAndAgain),
                        null, TranslateExtension.Get(TranslateManager.Close));

                    //await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
                    //{
                    //    //await MessagePopup.Instance.Show(response.Message);
                    //});
                }
            }
        }

        #endregion

        #region CheckAccount && Check Empty

        #region CheckEmpty

        public async Task<bool> IsCheckEmpty()
        {
            if (string.IsNullOrWhiteSpace(UserName) && string.IsNullOrEmpty(Password))
            {
                await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                    TranslateExtension.Get(TranslateManager.PleaseEnterUsernameAndPassword),
                    null, TranslateExtension.Get(TranslateManager.Close));

                return true;
            }

            if (string.IsNullOrWhiteSpace(UserName))
            {
                await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                    TranslateExtension.Get(TranslateManager.PleaseEnterUserName),
                    null, TranslateExtension.Get(TranslateManager.Close));

                return true;
            }

            if (string.IsNullOrEmpty(Password))
            {
                await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                    TranslateExtension.Get(TranslateManager.PleaseEnterPassword),
                    null, TranslateExtension.Get(TranslateManager.Close));

                return true;
            }

            return false;
        }

        #endregion

        public bool CheckAccount()
        {
            if (UserName == "Nauqhnim Ov" && Password == "12345678")
                return true;
            return false;
        }

        #endregion

        #region UpdateAndSaveAppSettings

        private void UpdateAppSettings(AccountUser accountUser)
        {
            App.Settings.FullName = accountUser.FullName;
            App.Settings.UserName = accountUser.UserName;

            Database.Update(App.Settings);
        }

        #endregion

        #region LoginFB

        public Command LoginFBCommand { get; set; }
        private async Task LoginFBExecute()
        {
            //await NavigationService.NavigateAsync(
            //    new Uri("http://www.nauqhnimov.com/NavigationPage/LoginFB",
            //        UriKind.Absolute) /*, new NavigationParameters($"?flag=FB")*/);

            await Task.Run(async () =>
            {
                IsVisibleWebView = true;
                WebViewOverlay = await FacebookApi.Instance.GetFacebookAuthDialog();
            });

        }

        public override async Task OnFacebookLoginCallBack(FacebookProfileModel profile)
        {
            IsVisibleWebView = false;

            var fullName = $"{profile.FirstName} {profile.LastName}";
            var user = new AccountUser
            {
                UserName = profile.Id,
                Password = profile.Id,
                FullName = fullName,
            };

            UpdateAppSettings(user);

            await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
            {
                await NavigationService.NavigateAsync(PageManager.MainPageUri());
            });

            /*await Task.Run(async () =>
            {
                await LoadingPopup.Instance.Show();

                var url = ApiUrl.UserSignup();

                var fullName = $"{profile.FirstName} {profile.LastName}";

                var user = new AccountUser
                {
                    UserName = profile.Id,
                    Password = profile.Id,
                    FullName = fullName,
                };
                //param.Password = param.CryptPassword(Password);

                var httpContent = user.ObjectToStringContent();
                var response = await HttpRequest.PostTaskAsync<JObjectResponse>(url, httpContent);
                await SignUpFbCallBack(response, user);

                await LoadingPopup.Instance.Hide();
            });*/

            #region Cmt Code

            //var user = new AccountUser()
            //{

            //};
            //UpdateAppSettings(result);

            //await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
            //{
            //    await NavigationService.NavigateAsync(PageManager.MainPageUri());
            //});

            //await MessagePopup.Instance.Show(
            //    "Username = " + profile.FirstName + " " + profile.LastName
            //    + "\nID = " + profile.Id
            //    + "\nGender = " + profile.Gender
            //    + "\nAvatar = " + profile.Picture.Data.Url);

            #endregion

        }

        private async Task SignUpFbCallBack(JObjectResponse response, AccountUser accountUser)
        {
            if (response == null)
            {
                // login fail
                await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                    TranslateExtension.Get(TranslateManager.CheckInternet),
                    null, TranslateExtension.Get(TranslateManager.Close));
                //await MessagePopup.Instance.Show(TranslateExtension.Get("Username_PasswordIncorrect"));
            }
            else
            {
                if (!response.Error.StringToBool())
                {
                    var result = response.Result.ToYourObject<AccountUser>();

                    UpdateAppSettings(result);

                    await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
                    {
                        await NavigationService.NavigateAsync(PageManager.MainPageUri());
                    });

                }
                else
                {
                    await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
                    {
                        if (response.ErrorCode == 403)
                        {
                            await CallApiLoginForFb(accountUser: accountUser);
                        }
                    });
                }
            }
        }

        #region CallApiLoginFb

        private async Task CallApiLoginForFb(AccountUser accountUser)
        {
            await Task.Run(async () =>
            {
                await LoadingPopup.Instance.Show();

                var url = ApiUrl.UserLogin();

                var user = new AccountUser
                {
                    UserName = accountUser.UserName,
                    Password = accountUser.Password,
                };

                var httpContent = user.ObjectToStringContent();
                var response = await HttpRequest.PostTaskAsync<JObjectResponse>(url, httpContent);
                await LoginFbCallBack(response);

                await LoadingPopup.Instance.Hide();
            });
        }

        private async Task LoginFbCallBack(JObjectResponse response)
        {
            if (response == null)
            {
                // login fail
                await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                    TranslateExtension.Get(TranslateManager.CheckInternet),
                    null, TranslateExtension.Get(TranslateManager.Close));
                //await MessagePopup.Instance.Show(TranslateExtension.Get("Username_PasswordIncorrect"));
            }
            else
            {
                if (!response.Error.StringToBool())
                {
                    var result = response.Result.ToYourObject<AccountUser>();

                    UpdateAppSettings(result);

                    await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
                    {
                        await NavigationService.NavigateAsync(PageManager.MainPageUri());
                    });
                }
                else
                {
                    UserName = "";
                    Password = "";
                   
                    await DeviceExtension.BeginInvokeOnMainThreadAsync(async () =>
                    {
                        await DialogService.DisplayAlertAsync(TranslateExtension.Get(TranslateManager.Caution),
                            TranslateExtension.Get(TranslateManager.IncorrectAndAgain),
                            null, TranslateExtension.Get(TranslateManager.Close));
                    });
                }
            }
        }

        #endregion

        #endregion

        #region Login Via Facebook

        //public ICommand LoginViaFacebookCommand { get; }

        //private async Task LoginViaFacebookExecute()
        //{
        //    await Task.Run(async () =>
        //    {
        //        WebViewOverlay = await FacebookApi.Instance.GetFacebookAuthDialog();
        //    });
        //}

        //public override async Task OnFacebookLoginCallBack(FacebookProfileModel profile)
        //{
        //    await MessagePopup.Instance.Show(
        //        "Username = " + profile.FirstName + " " + profile.LastName
        //        + "\nID = " + profile.Id
        //        + "\nGender = " + profile.Gender
        //        + "\nAvatar = " + profile.Picture.Data.Url);
        //}

        #endregion

        #region Login Via Google+

        //public ICommand LoginViaGooglePlusCommand { get; }

        //private async Task LoginViaGooglePlusExecute()
        //{
        //    await Task.Run(async () =>
        //    {
        //        WebViewOverlay = await GoogleApi.Instance.GetGooglePlusAuthDialog();
        //    });
        //}

        //public override async Task OnGooglePlusLoginCallBack(GoogleProfileModel profile)
        //{
        //    //await MessagePopup.Instance.Show(
        //    //    "Username = " + profile.FirstName + " " + profile.LastName
        //    //    + "\nID = " + profile.Id
        //    //    + "\nGender = " + profile.Gender
        //    //    + "\nAvatar = " + profile.Picture.Data.Url);
        //}

        #endregion

        #region SkipCommand

        public DelegateCommand TapSkipCommand { get; set; }

        private async void SkipExecute()
        {
            await NavigationService.NavigateAsync(PageManager.MainPageUri());
        }

        #endregion

        #region SignUpCommand

        public Command SignUpCommand { get; set; }
        private async Task SignUpCommandExecute()
        {
            await NavigationService.NavigateAsync(PageManager.SignUpPage);
        }

        #endregion

    }
}

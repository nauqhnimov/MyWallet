﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using MyWallet.Interfaces.HttpService;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using MyWallet.Interfaces.LocalDatabase;
using MyWallet.Services.FacebookApiService;
using MyWallet.Services.GoogleApiService;
using Prism.Commands;

namespace MyWallet.ViewModels
{
    public class BaseViewModel : BindableBase, INavigationAware, INotifyPropertyChanged
    {
        public INavigationService NavigationService { get; private set; }
        public IPageDialogService DialogService { get; private set; }
        public ISQLiteService Database { get; private set; }
        public IHttpRequest HttpRequest { get; private set; }

        public BaseViewModel(
            INavigationService navigationService = null,
            IPageDialogService dialogService = null,
            IHttpRequest httpRequest = null,
            ISQLiteService sqLiteService = null)
        {
            if (navigationService != null) NavigationService = navigationService;
            if (httpRequest != null) HttpRequest = httpRequest;
            if (dialogService != null) DialogService = dialogService;
            if (sqLiteService != null) Database = sqLiteService;

            BackCommand = new DelegateCommand(async () => await BackExecute());
        }

        #region Properties
       
        private string _title = string.Empty;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private bool _isBusy = false;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        #endregion

        #region OnAppear / Disappear

        public virtual void OnAppearing()
        {

        }

        public virtual void OnFirstTimeAppear()
        {

        }

        public virtual void OnDisappearing()
        {

        }

        #endregion

        #region OnNavigation

        public virtual void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public virtual void OnNavigatingTo(NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters != null)
            {
                if (parameters.ContainsKey("__NavigationMode"))
                {
                    var navMode = (NavigationMode)parameters["__NavigationMode"];
                    switch (navMode)
                    {
                        case NavigationMode.New: OnNavigatedNewTo(parameters); break;
                        case NavigationMode.Back: OnNavigatedBackTo(parameters); break;
                    }
                }
            }
        }

        public virtual void OnNavigatedNewTo(NavigationParameters parameters)
        {

        }

        public virtual void OnNavigatedBackTo(NavigationParameters parameters)
        {

        }

        #endregion

        #region Login via Facebook

        public virtual Task OnFacebookLoginCallBack(FacebookProfileModel profile)
        {
            return Task.Factory.StartNew(() =>
            {
                Debug.WriteLine("Virtual function OnFacebookLoginCallBack called");
            });
        }

        #endregion

        #region Login via Google+

        public virtual Task OnGooglePlusLoginCallBack(GoogleProfileModel profile)
        {
            return Task.Factory.StartNew(() =>
            {
                Debug.WriteLine("Virtual function OnGooglePlusLoginCallBack called");
            });
        }


        #endregion

        #region CheckBusy

        protected async Task CheckBusy(Func<Task> function)
        {
            if (App.IsBusy)
            {
                App.IsBusy = false;
                try
                {
                    await function();
                }
                catch (Exception e)
                {
#if DEBUG
                    Debug.WriteLine(e);
#endif
                }
                finally
                {
                    App.IsBusy = true;
                }
            }
        }

        #endregion

        #region BackCommand

        public ICommand BackCommand { get; }

        protected virtual async Task BackExecute()
        {
            await CheckBusy(async () =>
            {
                await NavigationService.GoBackAsync();
            });
        }

        #endregion

        #region BackButtonPress

        /// <summary>
        /// //false is default value when system call back press
        /// </summary>
        /// <returns></returns>
        public virtual bool OnBackButtonPressed()
        {
            //false is default value when system call back press
            return false;
            //Task.Run(async () =>
            //{
            //    await BackExecute();
            //});

            //return false;
        }

        /// <summary>
        /// called when page need override soft back button
        /// </summary>
        public virtual void OnSoftBackButtonPressed()
        {
            
        }

        #endregion


    }


    #region CodeCrossPlatform

    //public class BaseViewModel : INotifyPropertyChanged
    //{
    //    public IDataStore<Item> DataStore => DependencyService.Get<IDataStore<Item>>() ?? new MockDataStore();

    //    bool isBusy = false;
    //    public bool IsBusy
    //    {
    //        get { return isBusy; }
    //        set { SetProperty(ref isBusy, value); }
    //    }

    //    string title = string.Empty;
    //    public string Title
    //    {
    //        get { return title; }
    //        set { SetProperty(ref title, value); }
    //    }

    //    protected bool SetProperty<T>(ref T backingStore, T value,
    //        [CallerMemberName]string propertyName = "",
    //        Action onChanged = null)
    //    {
    //        if (EqualityComparer<T>.Default.Equals(backingStore, value))
    //            return false;

    //        backingStore = value;
    //        onChanged?.Invoke();
    //        OnPropertyChanged(propertyName);
    //        return true;
    //    }

    //    #region INotifyPropertyChanged
    //    public event PropertyChangedEventHandler PropertyChanged;
    //    protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
    //    {
    //        var changed = PropertyChanged;
    //        if (changed == null)
    //            return;

    //        changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
    //    }
    //    #endregion
    //}

    #endregion


}

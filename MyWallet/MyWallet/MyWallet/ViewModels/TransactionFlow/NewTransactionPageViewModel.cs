﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MyWallet.ResourcesTranslation;
using Prism.Navigation;
using Prism.Services;

namespace MyWallet.ViewModels.TransactionFlow
{
    public class NewTransactionPageViewModel : BaseViewModel
    {
        public NewTransactionPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            Title = TranslateExtension.Get("AddNew");
            Instance = this;
        }

        public static NewTransactionPageViewModel Instance { get; private set; }

        #region GoBackAsync

        public async Task GoBackAsync(NavigationParameters parameters = null)
        {
            if (parameters != null)
            {
                await NavigationService.GoBackAsync(parameters);
            }
            else
            {
                await NavigationService.GoBackAsync();
            }
            
        }

        #endregion

        #region

        public async void NavigateAsync(string destinatedPage, NavigationParameters parameters = null)
        {
            if (parameters != null)
            {
                await NavigationService.NavigateAsync(destinatedPage);
            }
            else
            {
                await NavigationService.NavigateAsync(destinatedPage, parameters);
            }
        }

        #endregion
    }
}

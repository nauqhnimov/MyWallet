﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Input;
using MyWallet.Enums;
using MyWallet.Interfaces.LocalDatabase;
using MyWallet.Managers;
using MyWallet.Models;
using MyWallet.ResourcesTranslation;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace MyWallet.ViewModels.TransactionFlow
{
    public class DetailTransactionPageViewModel : BaseViewModel
    {
        private bool _isLoadingImage;

        public bool IsLoadingImage
        {
            get { return _isLoadingImage; }
            set { SetProperty(ref _isLoadingImage, value); }
        }

        public ImageSource AnhMinhHoaSourcePhu;
        private ImageSource _anhMinhHoaSource;

        public ImageSource AnhMinhHoaSource
        {
            get { return _anhMinhHoaSource; }
            set { SetProperty(ref _anhMinhHoaSource, value); }
        }

        public Transaction ThongTinPhu;

        private Transaction _thongTin;

        public Transaction ThongTin
        {
            get { return _thongTin; }
            set { SetProperty(ref _thongTin, value); }
        }

        private bool ImageChanged { get; set; }

        private bool _transparents;

        public bool Transparents
        {
            get { return _transparents; }
            set
            {
                SetProperty(ref _transparents, value);
            }
        }

        private bool _isEit;

        public bool IsEdit
        {
            get { return _isEit; }
            set
            {
                SetProperty(ref _isEit, value);
                PropertyChanged += ChiTietViewModel_PropertyChanged;
            }
        }

        private TransactionType _transactionType;
        public TransactionType TransactionType
        {
            get => _transactionType;
            set => SetProperty(ref _transactionType, value);
        }

        private string _category;
        public string Category
        {
            get => _category;
            set => SetProperty(ref _category, value);
        }

        private void ChiTietViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Transparents = !((DetailTransactionPageViewModel)sender).IsEdit;

        }

        public DetailTransactionPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, ISQLiteService databaService)
            : base(navigationService: navigationService, dialogService: pageDialogService, sqLiteService: databaService)
        {
            Title = ResourcesTranslation.TranslateExtension.Get("Income");
            ThongTinPhu = new Transaction();
            AnhMinhHoaSourcePhu = new StreamImageSource();

            PickPhotoCommand = new DelegateCommand(OnPickPhotoExcute);
            TakePhotoCommand = new DelegateCommand(OnTakePhotoExcute);
            SaveCommand = new DelegateCommand(OnSaveExcute);
            CloseEditCommand = new DelegateCommand(OnCloseEditCommandExcute);
            StartEditCommand = new DelegateCommand(OnStartEditExcute);
            CreateQRCodeCommand = new DelegateCommand(OnCreateQRCodeExcute);
            CustomCategoryCommand = new DelegateCommand(OnCustomCategoryExcute);

            IsEdit = false;
            //Transparents = !IsEdit;
            ImageChanged = false;
        }

        private async void OnCreateQRCodeExcute()
        {
            string mess;
            if (ThongTin.TransactionType == TransactionType.Outcome)
            {
                mess = "1";
            }
            else
            {
                mess = "0";
            }
            mess += ThongTin.Amount + "-";
            mess += ThongTin.Category + "-";
            mess += ThongTin.Note + "-";

            mess += ThongTin.CreatedDate.ToString("ddMMyyyy");

            //var qrCode = new ZXingBarcodeImageView()
            //{
            //    BarcodeFormat = BarcodeFormat.QR_CODE,
            //    BarcodeOptions = new QrCodeEncodingOptions
            //    {
            //        Height = 50,
            //        Width = 50
            //    },
            //    BarcodeValue = mess,
            //    VerticalOptions = LayoutOptions.CenterAndExpand,
            //    HorizontalOptions = LayoutOptions.CenterAndExpand

            //};




            var p = new NavigationParameters();

            p.Add("message", mess);
            await NavigationService.NavigateAsync("QRCode", p);
        }

        private void OnStartEditExcute()
        {
            IsEdit = true;
        }

        private void OnCloseEditCommandExcute()
        {
            ThongTin = ThongTinPhu.Duplicate();

            if (ImageChanged == true)
            {
                Stream stream = new MemoryStream(ThongTin.DescriptionPhoto);
                AnhMinhHoaSource = ImageSource.FromStream(() => stream);

                ImageChanged = false;
            }

            IsEdit = false;

        }
        private void OnSaveExcute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            Database.Insert(ThongTin);
            Debug.WriteLine("Saved into database");

            var param = new NavigationParameters()
            {
                {KeyParameters.UpdateTransaction.ToString(), ThongTin},
            };
            NavigationService.GoBackAsync(param);

            IsBusy = false;
        }

        private async void OnTakePhotoExcute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DialogService.DisplayAlertAsync(TranslateExtension.Get("Message"),
                    TranslateExtension.Get("NoCameraAvailable"), TranslateExtension.Get("Dismiss"));
            }
            else
            {
                var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
                {
                    var results =
                        await
                            CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera, Permission.Storage);
                    cameraStatus = results[Permission.Camera];
                    storageStatus = results[Permission.Storage];
                }

                if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
                {
                    IsLoadingImage = true;
                    //Take and compress image
                    var photo = await Plugin.Media.CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions()
                    {
                        CompressionQuality = 10,
                    });
                    if (photo != null)
                    {
                        //Compress a valid image stream 
                        //with 20% compression, and return the new image as a MemoryStream.
                        //var stream = EZCompress1.Plugin.CrossEZCompress1.Current.compressImage(photo.GetStream(), 20);

                        //set image as bye[] into KhoangThu
                        ThongTin.DescriptionPhoto = GetBytesFromStream(photo.GetStream());

                        AnhMinhHoaSource = ImageSource.FromStream(() => photo.GetStream());

                        ImageChanged = true;
                    }
                    IsLoadingImage = false;
                }
            }

            IsBusy = false;
        }

        private async void OnPickPhotoExcute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await DialogService.DisplayAlertAsync(TranslateExtension.Get("Message"),
                    TranslateExtension.Get("CannotPickPhoto"), TranslateExtension.Get("Dismiss"));
            }
            else
            {
                var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
                {
                    var results =
                        await
                            CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera, Permission.Storage);
                    cameraStatus = results[Permission.Camera];
                    storageStatus = results[Permission.Storage];
                }

                if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
                {
                    IsLoadingImage = true;

                    //Pick and Compress Image
                    var photo = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
                    {
                        CompressionQuality = 10,
                    });
                    if (photo != null)
                    {
                        //Compress a valid image stream 
                        //with 20% compression, and return the new image as a MemoryStream.
                        //var stream = EZCompress1.Plugin.CrossEZCompress1.Current.compressImage(photo.GetStream(), 20);

                        //set image as bye[] into KhoangThu
                        ThongTin.DescriptionPhoto = GetBytesFromStream(photo.GetStream());

                        AnhMinhHoaSource = ImageSource.FromStream(() => photo.GetStream());

                        ImageChanged = true;
                    }

                    IsLoadingImage = false;
                }
            }

            IsBusy = false;
        }

        public static byte[] GetBytesFromStream(Stream fileContentStream)
        {
            using (var memoryStreamHandler = new MemoryStream())
            {
                fileContentStream.CopyTo(memoryStreamHandler);
                return memoryStreamHandler.ToArray();
            }
        }


        public ICommand PickPhotoCommand { get; set; }
        public ICommand TakePhotoCommand { get; set; }

        public ICommand SaveCommand { get; set; }
        public ICommand CloseEditCommand { get; set; }
        public ICommand StartEditCommand { get; set; }
        public ICommand CreateQRCodeCommand { get; set; }



        public async override void OnNavigatingTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("id"))
            {
                //int id = (int) parameters["id"];

                int Id = parameters.GetValue<int>("id");
                ThongTin = Database.Get<Transaction>( i => i.Id == Id);
                ThongTinPhu = ThongTin.Duplicate();

                if (ThongTin.TransactionType == TransactionType.Outcome)
                {
                    Title = ResourcesTranslation.TranslateExtension.Get("Outcome");
                }
                if (ThongTin.DescriptionPhoto != null)
                {
                    Stream stream = new MemoryStream(ThongTin.DescriptionPhoto);
                    AnhMinhHoaSource = ImageSource.FromStream(() => stream);
                    AnhMinhHoaSourcePhu = ImageSource.FromStream(() => stream);
                }
                else
                {
                    AnhMinhHoaSource = ImageSource.FromFile("anhmacdinh.png");
                    AnhMinhHoaSourcePhu = ImageSource.FromFile("anhmacdinh.png");
                }

            }

            if (parameters.ContainsKey(nameof(Transaction)))
            {
                //int id = (int) parameters["id"];

                ThongTin = parameters.GetValue<Transaction>(nameof(Transaction));
                ThongTinPhu = ThongTin.Duplicate();

                TransactionType = ThongTin.TransactionType;
                Category = ThongTin.Category;

                if (ThongTin.TransactionType == TransactionType.Outcome)
                {
                    Title = ResourcesTranslation.TranslateExtension.Get("Outcome");
                }
                if (ThongTin.DescriptionPhoto != null)
                {
                    Stream stream = new MemoryStream(ThongTin.DescriptionPhoto);
                    AnhMinhHoaSource = ImageSource.FromStream(() => stream);
                    AnhMinhHoaSourcePhu = ImageSource.FromStream(() => stream);
                }
                else
                {
                    AnhMinhHoaSource = ImageSource.FromFile("anhmacdinh.png");
                    AnhMinhHoaSourcePhu = ImageSource.FromFile("anhmacdinh.png");
                }

            }
        }

        public override void OnNavigatedBackTo(NavigationParameters parameters)
        {
            if (parameters != null && parameters.ContainsKey(KeyParameters.CategorySelected.ToString()))
            {
                Category = (string)parameters[KeyParameters.CategorySelected.ToString()];
                ThongTin.Category = Category;
            }

            if (parameters != null && parameters.ContainsKey(KeyParameters.TransactionType.ToString()))
            {
                TransactionType = (TransactionType)parameters[KeyParameters.TransactionType.ToString()];
                ThongTin.TransactionType = TransactionType;
            }
        }

        #region CustomCategoryCommand

        public ICommand CustomCategoryCommand { get; set; }

        private async void OnCustomCategoryExcute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            var navparameters = new NavigationParameters
            {
                {
                    KeyParameters.Title.ToString(), TranslateExtension.Get(TranslateManager.CustomCategory)
                },
            };
            await NavigationService.NavigateAsync(PageManager.CategoriesTabbedPage, parameters: navparameters);

            IsBusy = false;
        }

        #endregion
    }
}

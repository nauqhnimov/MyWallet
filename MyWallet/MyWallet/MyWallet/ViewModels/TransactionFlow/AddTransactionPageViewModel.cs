﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using MyWallet.Enums;
using MyWallet.Helpers;
using MyWallet.Interfaces.HttpService;
using MyWallet.Interfaces.LocalDatabase;
using MyWallet.Managers;
using MyWallet.Models;
using MyWallet.Models.Servers;
using MyWallet.ResourcesTranslation;
using MyWallet.Services.HttpService;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace MyWallet.ViewModels.TransactionFlow
{
    public class AddTransactionPageViewModel : BaseViewModel
    {
        private IDependencyService DependencyService { get; set; }

        public AddTransactionPageViewModel(INavigationService navigationService, IDependencyService dependencyService,
            IPageDialogService pageDialogService, ISQLiteService databaService, IHttpRequest httpRequest) :
            base(navigationService: navigationService, dialogService: pageDialogService, sqLiteService: databaService, httpRequest: httpRequest)
        {
            Title = TranslateExtension.Get(TranslateManager.AddTransaction);
            DependencyService = dependencyService;

            CustomCategoryCommand = new DelegateCommand(OnCustomCategoryExcute);
            PickPhotoCommand = new DelegateCommand(OnPickPhotoExcute);
            TakePhotoCommand = new DelegateCommand(OnTakePhotoExcute);
            ResetCommand = new DelegateCommand(OnResetExcute);
            SaveCommand = new DelegateCommand(OnSaveExcute);
            ScanCodeCommand = new DelegateCommand(OnScanCodeExcute);

        }

        #region Properties

        private int _nextId;

        private bool _isLoadingImage;
        public bool IsLoadingImage
        {
            get { return _isLoadingImage; }
            set { SetProperty(ref _isLoadingImage, value); }
        }

        private string _amount;
        public string Amount
        {
            get => _amount;
            set => SetProperty(ref _amount, value);
        }

        private TransactionType _transactionType;
        public TransactionType TransactionType
        {
            get => _transactionType;
            set => SetProperty(ref _transactionType, value);
        }

        private string _category;
        public string Category
        {
            get => _category;
            set => SetProperty(ref _category, value);
        }

        private string _categoryKey;
        public string CategoryKey
        {
            get => _categoryKey;
            set => SetProperty(ref _categoryKey, value);
        }

        private string _note;
        public string Note
        {
            get => _note;
            set => SetProperty(ref _note, value);
        }

        private DateTime _createdDate = DateTime.Today;
        public DateTime CreatedDate
        {
            get => _createdDate;
            set => SetProperty(ref _createdDate, value);
        }
        
        private ImageSource _anhMinhHoaSource;
        public ImageSource AnhMinhHoaSource
        {
            get { return _anhMinhHoaSource; }
            set { SetProperty(ref _anhMinhHoaSource, value); }
        }

        private Transaction _transaction;
        public Transaction Transaction
        {
            get { return _transaction; }
            set { SetProperty(ref _transaction, value); }
        }

        #endregion

        #region Override Navigate new to

        public override void OnNavigatedNewTo(NavigationParameters parameters)
        {

        }

        #endregion

        #region Override Navigate back to

        public override void OnNavigatedBackTo(NavigationParameters parameters)
        {
            if (parameters != null && parameters.ContainsKey(KeyParameters.CategorySelected.ToString()))
            {
                Category = CategoryKey = (string)parameters[KeyParameters.CategorySelected.ToString()];
            }

            if (parameters != null && parameters.ContainsKey(KeyParameters.TransactionType.ToString()))
            {
                TransactionType = (TransactionType)parameters[KeyParameters.TransactionType.ToString()];
            }

        }

        #endregion

        #region OnAppearing

        public override void OnAppearing()
        {
            
        }

        #endregion

        #region OnFirstTimeAppear

        public override void OnFirstTimeAppear()
        {
            Transaction = new Transaction();

            AnhMinhHoaSource = ImageSource.FromFile("anhmacdinh.png");
            
            IsLoadingImage = false;
        }

        #endregion

        #region CustomCategoryCommand

        public ICommand CustomCategoryCommand { get; set; }

        private async void OnCustomCategoryExcute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            var navparameters = new NavigationParameters
            {
                {
                    KeyParameters.Title.ToString(), TranslateExtension.Get(TranslateManager.CustomCategory)
                },
            };
            await NavigationService.NavigateAsync(PageManager.CategoriesTabbedPage, parameters: navparameters);

            IsBusy = false;
        }

        #endregion

        #region PickPhotoCommand

        public ICommand PickPhotoCommand { get; set; }

        private async void OnPickPhotoExcute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            CheckPermissionAndUpdateSource(TranslateManager.PickPhoto);

            IsBusy = false;
        }

        #endregion

        #region TakePhotoCommand

        public ICommand TakePhotoCommand { get; set; }

        private async void OnTakePhotoExcute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            CheckPermissionAndUpdateSource(TranslateManager.TakePhoto);

            IsBusy = false;
        }

        #endregion

        #region CheckPermissionAndUpdateSource

        private async void CheckPermissionAndUpdateSource(string methodChoosePicture)
        {
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                var message = (methodChoosePicture.Equals(TranslateManager.TakePhoto))
                    ? TranslateExtension.Get(TranslateManager.NoCameraAvailable)
                    : TranslateExtension.Get(TranslateManager.CannotPickPhoto);
                await DialogService.DisplayAlertAsync(title: TranslateExtension.Get(TranslateManager.Message),
                    message: message, cancelButton: TranslateExtension.Get(TranslateManager.Dismiss));
            }
            else
            {
                var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
                {
                    var results =
                        await
                            CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera, Permission.Storage);
                    cameraStatus = results[Permission.Camera];
                    storageStatus = results[Permission.Storage];
                }

                if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
                {
                    IsLoadingImage = true;

                    //Take or pick and compress image
                    var photo = (methodChoosePicture.Equals(TranslateManager.TakePhoto))
                        ? await Plugin.Media.CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions()
                        {
                            SaveToAlbum = true,
                            Directory = "MyWallet",
                            CompressionQuality = 10,
                        })
                        : await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
                        {
                            CompressionQuality = 10,
                        });


                    if (photo != null)
                    {
                        AnhMinhHoaSource = ImageSource.FromStream(() => photo.GetStream());

                        //set image as bye[] into Transaction
                        Transaction.DescriptionPhoto = GetBytesFromStream(photo.GetStream());
                    }
                    IsLoadingImage = false;

                    //dispose mediafile
                    photo.Dispose();
                }
                else
                {
                    await DialogService.DisplayAlertAsync(title: TranslateExtension.Get(TranslateManager.Message),
                        message: TranslateExtension.Get("PermissionsDenied") + ". " +
                                 TranslateExtension.Get("UnableToTakePhotos"),
                        cancelButton: TranslateExtension.Get(TranslateManager.Dismiss));

                    //On iOS you may want to send your user to the settings screen.
                    await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Camera);
                    await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Storage);
                }
            }

            await CrossMedia.Current.Initialize();
        }

        #endregion

        #region GetBytesFromStream

        /// <summary>
        /// convert stream to byte[]
        /// </summary>
        /// <param name="fileContentStream"></param>
        /// <returns></returns>
        public static byte[] GetBytesFromStream(Stream fileContentStream)
        {
            using (var memoryStreamHandler = new MemoryStream())
            {
                fileContentStream.CopyTo(memoryStreamHandler);
                return memoryStreamHandler.ToArray();
            }
        }

        #endregion

        #region ResetCommand

        public ICommand ResetCommand { get; set; }

        private void OnResetExcute()
        {
            Transaction = new Transaction(Transaction.Id);
            Transaction.CreatedDate = DateTime.Today;
            AnhMinhHoaSource = ImageSource.FromFile("anhmacdinh.png");
        }

        #endregion

        #region SaveCommand

        public ICommand SaveCommand { get; set; }

        private async void OnSaveExcute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            if (!string.IsNullOrEmpty(Amount) && !string.IsNullOrEmpty(Category))
            {
                // Update Transaction model
                UpdateTransactionModel();

                Database.Insert(Transaction);
                Debug.WriteLine("Saved into database");

                // Call API - not wait
                CallApiPostTrans();

                var param = new NavigationParameters()
                {
                    {KeyParameters.UpdateTransaction.ToString(), Transaction},
                };

                await NavigationService.GoBackAsync(parameters: param);
            }
            else
            {
                string mess = TranslateExtension.Get("InputMoneyAndCategory");
                string close = TranslateExtension.Get("Close");
                await DialogService.DisplayAlertAsync(null, mess, null, close);
            }

            IsBusy = false;

        }

        #endregion

        #region UpdateTransactionModel

        private void UpdateTransactionModel()
        {
            _nextId = Database.GetList<Transaction>(i => i.Id >= 0).Count();

            Transaction.Id = _nextId;
            Transaction.Amount = Amount;
            Transaction.Category = CategoryKey;
            Transaction.Note = Note;
            Transaction.CreatedDate = CreatedDate;
            Transaction.TransactionType = TransactionType;

            Transaction.UserName = App.Settings.UserName;
        }

        #endregion
        
        #region Call API Post Transaction

        private async void CallApiPostTrans()
        {
            await Task.Run(async () =>
            {
                //await LoadingPopup.Instance.Show();

                var url = ApiUrl.PostTrans();

                var httpContent = Transaction.ObjectToStringContent();
                var response = await HttpRequest.PostTaskAsync<JObjectResponse>(url, httpContent);

                CallApiPostTransCallBack(response);

                //await LoadingPopup.Instance.Hide();
            });
        }

        private void CallApiPostTransCallBack(JObjectResponse response)
        {
            if (response == null)
            {
                var temp = new TransactionTemp(trans: Transaction);
                App.TempTransactions.Add(temp);
            }
            else
            {
                if (!response.Error.StringToBool())
                {
                    // do nothing
                }
                else
                {
                    var temp = new TransactionTemp(trans: Transaction);
                    App.TempTransactions.Add(temp);
                }
            }
        }

        #endregion

        #region ScanCodeCommand

        public ICommand ScanCodeCommand { get; set; }

        private async void OnScanCodeExcute()
        {
            var scanner = new ZXing.Mobile.MobileBarcodeScanner();
            var result = await scanner.Scan();

            if (result != null)
            {
                Debug.WriteLine("Scanned Message: " + result.Text);
                string Result = result.Text.Substring(0);

                //phần tử đầu tiền = 0 thì là code của khoảng thu
                if (Result.Substring(0, 1).Equals("0"))
                {
                    Transaction = GetTransactionFromScan(Result);
                }
                else
                {
                    string mess = TranslateExtension.Get("ThisIsIncomeCode");
                    string rescan = TranslateExtension.Get("ReScan");
                    string cancel = TranslateExtension.Get("CancelScanCode");
                    bool t = await DialogService.DisplayAlertAsync(null, mess, rescan, cancel);
                    if (t == true)
                    {
                        OnScanCodeExcute();
                    }
                }

            }
        }

        #endregion

        #region GetTransactionFromScan

        private Transaction GetTransactionFromScan(string result)
        {

            int l = result.IndexOf("-");
            string sotien = result.Substring(1, l - 1);
            result = result.Remove(0, l + 1);

            l = result.IndexOf("-");
            string loai = result.Substring(0, l);
            result = result.Remove(0, l + 1);

            l = result.IndexOf("-");
            string ghichu = result.Substring(0, l);
            result = result.Remove(0, l + 1);

            string ngay = result.Substring(0);
            int day = Int32.Parse(ngay.Substring(0, 2));
            int month = Int32.Parse(ngay.Substring(2, 2));
            int year = Int32.Parse(ngay.Substring(4, 4));
            DateTime date = new DateTime(year, month, day);

            //Transaction Transaction = new Transaction(sotien, loai, ghichu, date, null, false);

            return Transaction;
        }

        #endregion
        
    }
}


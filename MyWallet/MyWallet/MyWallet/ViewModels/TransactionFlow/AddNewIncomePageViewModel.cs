﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Android.Security.Keystore;
using MyWallet.Enums;
using MyWallet.Interfaces.LocalDatabase;
using MyWallet.Models;
using MyWallet.ResourcesTranslation;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace MyWallet.ViewModels.TransactionFlow
{
    public class AddNewIncomePageViewModel: BaseViewModel
    {
        private IDependencyService DependencyService { get; set; }

        private bool _isLoadingImage;
        private int _nextId;

        public bool IsLoadingImage
        {
            get { return _isLoadingImage; }
            set { SetProperty(ref _isLoadingImage, value); }
        }

        private Transaction _khoangThu;

        public Transaction KhoangThu
        {
            get { return _khoangThu; }
            set { SetProperty(ref _khoangThu, value); }
        }

        private ImageSource _anhMinhHoaSource;

        public ImageSource AnhMinhHoaSource
        {
            get { return _anhMinhHoaSource; }
            set { SetProperty(ref _anhMinhHoaSource, value); }
        }

        public AddNewIncomePageViewModel(INavigationService navigationService, IDependencyService ds, IPageDialogService pageDialogService, ISQLiteService databaService) : 
            base(navigationService, pageDialogService, databaService)
        {
            Title = TranslateExtension.Get("Income");
            PickPhotoCommand = new DelegateCommand(OnPickPhotoExcute);
            TakePhotoCommand = new DelegateCommand(OnTakePhotoExcute);
            ResetCommand = new DelegateCommand(OnResetExcute);
            SaveCommand = new DelegateCommand(OnSaveExcute);
            ScanCodeCommand = new DelegateCommand(OnScanCodeExcute);
            DependencyService = ds;
            KhoangThu = new Transaction();
            KhoangThu.CreatedDate = DateTime.Today;
            AnhMinhHoaSource = ImageSource.FromFile("anhmacdinh.png");
            KhoangThu.IsAOutcome = false;


            IsLoadingImage = false;

        }

        #region Override Navigate new to

        public override void OnNavigatedNewTo(NavigationParameters parameters)
        {
            
        }

        #endregion

        private async void OnScanCodeExcute()
        {
            var scanner = new ZXing.Mobile.MobileBarcodeScanner();
            var result = await scanner.Scan();

            if (result != null)
            {
                Debug.WriteLine("Scanned Message: " + result.Text);
                string Result = result.Text.Substring(0);

                //phần tử đầu tiền = 0 thì là code của khoảng thu
                if (Result.Substring(0, 1).Equals("0"))
                {
                    KhoangThu = GetKhoangThuFromScan(Result);
                }
                else
                {
                    string mess = TranslateExtension.Get("ThisIsIncomeCode");
                    string rescan = TranslateExtension.Get("ReScan");
                    string cancel = TranslateExtension.Get("CancelScanCode");
                    bool t = await DialogService.DisplayAlertAsync(null, mess, rescan, cancel);
                    if (t == true)
                    {
                        OnScanCodeExcute();
                    }
                }



            }
        }

        private Transaction GetKhoangThuFromScan(string result)
        {

            int l = result.IndexOf("-");
            string sotien = result.Substring(1, l - 1);
            result = result.Remove(0, l + 1);

            l = result.IndexOf("-");
            string loai = result.Substring(0, l);
            result = result.Remove(0, l + 1);

            l = result.IndexOf("-");
            string ghichu = result.Substring(0, l);
            result = result.Remove(0, l + 1);

            string ngay = result.Substring(0);
            int day = Int32.Parse(ngay.Substring(0, 2));
            int month = Int32.Parse(ngay.Substring(2, 2));
            int year = Int32.Parse(ngay.Substring(4, 4));
            DateTime date = new DateTime(year, month, day);

            Transaction KhoangThu = new Transaction(sotien, loai, ghichu, date, null, false);

            return KhoangThu;
        }

        private async void OnSaveExcute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            if (!KhoangThu.IsNull())
            {
                if (!KhoangThu.Amount.EndsWith("Đ"))
                {
                    KhoangThu.Amount += " Đ";
                }

                _nextId = Database.GetList<Transaction>(i => i.Id >= 0).Count();
                KhoangThu.Id = _nextId;

                Database.Insert(KhoangThu);
                Debug.WriteLine("Saved into database");
                //await NavigationService.GoBackAsync();

                var param = new NavigationParameters()
                {
                    {KeyParameters.UpdateTransaction.ToString(), KhoangThu },
                };

                await NewTransactionPageViewModel.Instance.GoBackAsync(parameters: param);
            }
            else
            {
                string mess = TranslateExtension.Get("InputMoneyAndCategory");
                string close = TranslateExtension.Get("Close");
                await DialogService.DisplayAlertAsync(null, mess, null, close);
            }

            IsBusy = false;

        }

        /// <summary>
        /// convert stream to byte[]
        /// </summary>
        /// <param name="fileContentStream"></param>
        /// <returns></returns>
        public static byte[] GetBytesFromStream(Stream fileContentStream)
        {
            using (var memoryStreamHandler = new MemoryStream())
            {
                fileContentStream.CopyTo(memoryStreamHandler);
                return memoryStreamHandler.ToArray();
            }
        }

        private void OnResetExcute()
        {

            KhoangThu = new Transaction(KhoangThu.Id);
            KhoangThu.CreatedDate = DateTime.Today;
            AnhMinhHoaSource = ImageSource.FromFile("anhmacdinh.png");

        }


        private async void OnTakePhotoExcute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DialogService.DisplayAlertAsync(TranslateExtension.Get("Message"),
                    TranslateExtension.Get("NoCameraAvailable"), TranslateExtension.Get("Dismiss"));
            }
            else
            {
                var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
                {
                    var results =
                        await
                            CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera, Permission.Storage);
                    cameraStatus = results[Permission.Camera];
                    storageStatus = results[Permission.Storage];
                }

                if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
                {
                    IsLoadingImage = true;

                    //Take and compress image
                    var photo = await Plugin.Media.CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions()
                    {
                        SaveToAlbum = true,
                        Directory = "MyWallet",
                        CompressionQuality = 10,
                    });
                    if (photo != null)
                    {
                        //Compress a valid image stream 
                        //with 20% compression, and return the new image as a MemoryStream.
                        //var stream = EZCompress1.Plugin.CrossEZCompress1.Current.compressImage(photo.GetStream(), 20);

                        AnhMinhHoaSource = ImageSource.FromStream(() => photo.GetStream());


                        //set image as bye[] into KhoangThu
                        KhoangThu.DescriptionPhoto = GetBytesFromStream(photo.GetStream());
                    }
                    IsLoadingImage = false;
                }
            }

            IsBusy = false;
        }

        private async void OnPickPhotoExcute()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await DialogService.DisplayAlertAsync(TranslateExtension.Get("Message"),
                    TranslateExtension.Get("CannotPickPhoto"), TranslateExtension.Get("Dismiss"));
            }
            else
            {
                var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
                {
                    var results =
                        await
                            CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera, Permission.Storage);
                    cameraStatus = results[Permission.Camera];
                    storageStatus = results[Permission.Storage];
                }

                if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
                {

                    IsLoadingImage = true;

                    //Pick and Compress Image
                    var photo = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
                    {
                        CompressionQuality = 10,
                    });
                    if (photo != null)
                    {
                        //Compress a valid image stream 
                        //with 20% compression, and return the new image as a MemoryStream.
                        //var stream = EZCompress1.Plugin.CrossEZCompress1.Current.compressImage(photo.GetStream(), 20);

                        AnhMinhHoaSource = ImageSource.FromStream(() => photo.GetStream());


                        //set image as bye[] into KhoangThu
                        KhoangThu.DescriptionPhoto = GetBytesFromStream(photo.GetStream());
                    }

                    IsLoadingImage = false;
                }

            }

            IsBusy = false;
        }

        public ICommand PickPhotoCommand { get; set; }
        public ICommand TakePhotoCommand { get; set; }

        public ICommand ResetCommand { get; set; }
        public ICommand SaveCommand { get; set; }

        public ICommand ScanCodeCommand { get; set; }

    }
}


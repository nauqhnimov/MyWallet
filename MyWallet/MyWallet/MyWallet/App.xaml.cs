﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using DryIoc;
using MyWallet.Interfaces.HttpService;
using Prism.DryIoc;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MyWallet.Interfaces.LocalDatabase;
using MyWallet.Managers;
using MyWallet.Models;
using MyWallet.ResourcesTranslation;
using MyWallet.Services.HttpService;
using MyWallet.Services.LocalDatabase;
using MyWallet.Views;
using MyWallet.Views.LoginPage;
using MyWallet.Views.MasterDetailPage;
using MyWallet.Views.SearchFilterFlow;
using MyWallet.Views.TransactionFlow;
using MyWallet.Views.SettingsPage;
using MyWallet.Views.StatisticsFlow;
using MyWallet.Views.CommonPages;
using Plugin.Connectivity;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MyWallet
{
    public partial class App : PrismApplication
    {
        //TODO: Replace with *.azurewebsites.net url after deploying backend to Azure
        public static string AzureBackendUrl = "http://localhost:5000";
        public static bool UseMockDataStore = true;
        public static DateTime StartDateForStatistic;
        public static DateTime EndDateForStatistic;

        #region CrossPlatform

        //public App()
        //{
        //    InitializeComponent();

        //    if (UseMockDataStore)
        //        DependencyService.Register<MockDataStore>();
        //    else
        //        DependencyService.Register<AzureDataStore>();

        //    MainPage = new MainPage();
        //}

        #endregion

        #region Properties

        private ISQLiteService _sqLiteService;
        public static AppSettings Settings { get; set; }

        public static bool IsBusy = true;

        public static ObservableCollection<TransactionTemp> TempTransactions = new ObservableCollection<TransactionTemp>();

        //public INavService Navigator { get; internal set; }

        #endregion

        public App()
        {

        }

        /// <summary>
        /// handle network changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void ConnectivityChanged(object sender,
            Plugin.Connectivity.Abstractions.ConnectivityChangedEventArgs e)
        {
            if (e.IsConnected)
            {
                //show popup overlay here
                //MessagePopup.Instance.Show(TranslateExtension.Get("NetworkError"));
            }
            else
            {
                //MessagePopup.Instance.Show(TranslateExtension.Get("NetworkError"));
            }
        }

        protected override async void OnInitialized()
        {
            InitDatabase();
            //InitMockData();
            InitializeComponent();

            CrossConnectivity.Current.ConnectivityChanged += ConnectivityChanged;

            await StartApp();
        }

        #region RegisterTypes

        protected override void RegisterTypes()
        {
            Container.RegisterTypeForNavigation<TestPage>();

            // Register Pages
            Container.RegisterTypeForNavigation<NavigationPage>(PageManager.NavigationPage);

            // Master Detail Page
            Container.RegisterTypeForNavigation<MenuPage>(PageManager.MenuPage);
            Container.RegisterTypeForNavigation<MainPage>(PageManager.MainPage);

            // Login Flow
            Container.RegisterTypeForNavigation<LoginPage>(PageManager.LoginPage);
            Container.RegisterTypeForNavigation<SignUpPage>(PageManager.SignUpPage);

            // Settings Flow
            Container.RegisterTypeForNavigation<SettingsPage>(PageManager.SettingsPage);

            //Transactionflow
            Container.RegisterTypeForNavigation<AddTransactionPage>(PageManager.AddTransactionPage);
            //Container.RegisterTypeForNavigation<AddNewIncomePage>(PageManager.AddNewIncomePage);
            //Container.RegisterTypeForNavigation<AddNewOutcomePage>(PageManager.AddNewOutcomePage);
            //Container.RegisterTypeForNavigation<NewTransactionPage>(PageManager.NewTransactionPage);
            Container.RegisterTypeForNavigation<DetailTransactionPage>(PageManager.DetailTransactionPage);

            //Statistics flow
            Container.RegisterTypeForNavigation<OutcomeStatisticsPage>(PageManager.OutcomeStatisticsPage);
            Container.RegisterTypeForNavigation<IncomeStatisticsPage>(PageManager.IncomeStatisticsPage);
            Container.RegisterTypeForNavigation<GeneralStatisticsPage>(PageManager.GeneralStatisticsPage);
            Container.RegisterTypeForNavigation<StatisticsPage>(PageManager.StatisticsPage);
            Container.RegisterTypeForNavigation<ChooseDatePage>(PageManager.ChooseDatePage);

            // Search Filter Flow
            Container.RegisterTypeForNavigation<SearchFilterPage>(PageManager.SearchFilterPage);
            Container.RegisterTypeForNavigation<ViewSearchFilterPage>(PageManager.ViewSearchFilterPage);

            //Common Pages
            Container.RegisterTypeForNavigation<CategoriesTabbedPage>(PageManager.CategoriesTabbedPage);
            Container.RegisterTypeForNavigation<CategoriesIncomePage>(PageManager.CategoriesIncomePage);
            Container.RegisterTypeForNavigation<CategoriesOutcomePage>(PageManager.CategoriesOutcomePage);



            // Register Services
            Container.Register<ISQLiteService, SQLiteService>(Reuse.ScopedOrSingleton);
            Container.Register<IHttpRequest, HttpRequest>(Reuse.ScopedOrSingleton);


        }

        #endregion



        #region InitDatabase

        private void InitDatabase()
        {
            var connectionService = DependencyService.Get<IDatabaseConnection>();
            _sqLiteService = new SQLiteService(connectionService);
        }

        #endregion

        #region StartApp

        private async Task StartApp()
        {
            Settings = new AppSettings();
            Settings = _sqLiteService.GetSettings();


            //await NavigationService.NavigateAsync($"{PageManager.NavigationPage}/{PageManager.StatisticsPage}");
            //return;

            if (string.IsNullOrEmpty(Settings.FullName))
                await NavigationService.NavigateAsync(new Uri($"{PageManager.NavigationHomeUri}{PageManager.LoginPage}"));
            else
                await NavigationService.NavigateAsync(PageManager.MainPageUri());

        }

        #endregion

        //protected override void OnStart()
        //{
        //    // Handle when your app starts
        //}

        //protected override void OnSleep()
        //{
        //    // Handle when your app sleeps
        //}

        //protected override void OnResume()
        //{
        //    // Handle when your app resumes
        //}
    }
}

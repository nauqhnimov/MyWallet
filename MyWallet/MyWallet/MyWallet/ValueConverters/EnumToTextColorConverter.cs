﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using MyWallet.Enums;
using MyWallet.ResourcesTranslation;
using Xamarin.Forms;

namespace MyWallet.ValueConverters
{
    public class EnumToTextColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var transactionType = (TransactionType)value;

            switch (transactionType)
            {
                case TransactionType.None:
                    return Xamarin.Forms.Color.FromHex("#00A6FF");
                case TransactionType.Income:
                    return Xamarin.Forms.Color.FromHex("#13CE66");
                case TransactionType.Outcome:
                    return Xamarin.Forms.Color.FromHex("#E81120");
            }

            return Xamarin.Forms.Color.FromHex("#00A6FF");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using MyWallet.ResourcesTranslation;
using Xamarin.Forms;

namespace MyWallet.ValueConverters
{
    public class EnumToTranslationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return TranslateExtension.Get(string.IsNullOrEmpty((string) value) ? string.Empty : (string) value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return TranslateExtension.Get((string)value);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using MyWallet.Enums;
using MyWallet.ResourcesTranslation;
using Xamarin.Forms;

namespace MyWallet.ValueConverters
{
    public class FormatDateCreateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((DateTime)value).ToString(App.Settings.FormatDate);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return "";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using MyWallet.Enums;
using MyWallet.ResourcesTranslation;
using Xamarin.Forms;

namespace MyWallet.ValueConverters
{
    public class CurrencyUnitConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var unit = (App.Settings.Language == (int) LanguageType.Vietnamese) ? "₫" : "$";
            return $"{(string)value} {unit}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var unit = (App.Settings.Language == (int)LanguageType.Vietnamese) ? "₫" : "$";
            return $"{(string)value} {unit}";
        }
    }
}

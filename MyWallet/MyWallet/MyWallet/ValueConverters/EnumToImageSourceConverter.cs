﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using MyWallet.ResourcesTranslation;
using Xamarin.Forms;

namespace MyWallet.ValueConverters
{
    public class EnumToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return $"ic_{((string)value)?.ToLower()}.png";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return $"ic_{((string)value)?.ToLower()}.png";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using MyWallet.Enums;
using Xamarin.Forms;

namespace MyWallet.Controls
{
    public class CustomEntry : Entry
    {
        public CustomEntry()
        {
            
        }

        #region AllCaps

        public static readonly BindableProperty AllCapsProperty = BindableProperty.Create(nameof(AllCaps),
            typeof(bool), typeof(CustomEntry), true);

        public bool AllCaps
        {
            get => (bool)GetValue(AllCapsProperty);
            set => SetValue(AllCapsProperty, value);
        }

        #endregion

        #region Borderless

        public static readonly BindableProperty BorderlessProperty = BindableProperty.Create(nameof(Borderless),
            typeof(bool), typeof(CustomEntry), false);

        public bool Borderless
        {
            get => (bool)GetValue(BorderlessProperty);
            set => SetValue(BorderlessProperty, value);
        }

        #endregion

        #region AlignIcon

        public static readonly BindableProperty AlignIconProperty =
            BindableProperty.Create(
                nameof(AlignIcon),
                typeof(AlignIconType),
                typeof(CustomEntry),
                AlignIconType.Left,
                propertyChanged: (bindable, value, newValue) =>
                {
                    var control = (CustomEntry)bindable;
                    control.AlignIcon = (AlignIconType)newValue;
                });

        public AlignIconType AlignIcon
        {
            get => (AlignIconType)GetValue(AlignIconProperty);
            set => SetValue(AlignIconProperty, value);
        }

        #endregion

        #region Icon

        public static readonly BindableProperty IconProperty =
            BindableProperty.Create(nameof(Icon), typeof(string), typeof(CustomEntry), string.Empty);

        public string Icon
        {
            get => (string)GetValue(IconProperty);
            set => SetValue(IconProperty, value);
        }

        #endregion

        #region IconHeight

        public int IconHeight
        {
            get => (int)GetValue(IconHeightProperty);
            set => SetValue(IconHeightProperty, value);
        }

        public static readonly BindableProperty IconHeightProperty =
            BindableProperty.Create(nameof(IconHeight), typeof(int), typeof(CustomEntry), 40);

        #endregion

        #region IconWidth

        public int IconWidth
        {
            get => (int)GetValue(IconWidthProperty);
            set => SetValue(IconWidthProperty, value);
        }

        public static readonly BindableProperty IconWidthProperty =
            BindableProperty.Create(nameof(IconWidth), typeof(int), typeof(CustomEntry), 40);

        #endregion

        #region WhiteCursor

        public static readonly BindableProperty WhiteCursorProperty = BindableProperty.Create(nameof(WhiteCursor),
            typeof(bool), typeof(CustomEntry), false);

        public bool WhiteCursor
        {
            get => (bool)GetValue(WhiteCursorProperty);
            set => SetValue(WhiteCursorProperty, value);
        }

        #endregion
    }
}

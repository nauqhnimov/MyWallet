﻿using Xamarin.Forms;

namespace MyWallet.Controls
{
    public class CustomDatePicker : DatePicker
    {
        public CustomDatePicker()
        {

        }

        #region Borderless

        public static readonly BindableProperty BorderlessProperty = BindableProperty.Create(nameof(Borderless),
            typeof(bool), typeof(CustomDatePicker), false);

        public bool Borderless
        {
            get => (bool)GetValue(BorderlessProperty);
            set => SetValue(BorderlessProperty, value);
        }

        #endregion

        #region SetFontSize

        public static readonly BindableProperty SetFontSizeProperty = BindableProperty.Create(nameof(SetFontSize),
            typeof(bool), typeof(CustomDatePicker), false);

        public bool SetFontSize
        {
            get => (bool)GetValue(SetFontSizeProperty);
            set => SetValue(SetFontSizeProperty, value);
        }

        #endregion
    }
}

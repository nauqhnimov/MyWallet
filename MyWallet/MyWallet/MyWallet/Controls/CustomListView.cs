﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyWallet.Controls
{
    public class CustomListView : ListView
    {
        public CustomListView()
        {

        }

        #region ScrollEnabled

        public static readonly BindableProperty ScrollEnabledProperty = BindableProperty.Create(nameof(ScrollEnabled),
            typeof(bool), typeof(CustomListView), true);

        public bool ScrollEnabled
        {
            get => (bool)GetValue(ScrollEnabledProperty);
            set => SetValue(ScrollEnabledProperty, value);
        }

        #endregion
    }
}

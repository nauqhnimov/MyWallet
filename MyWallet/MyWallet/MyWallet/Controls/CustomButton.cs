﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyWallet.Controls
{
    public class CustomButton : Button
    {
        public CustomButton()
        {
            
        }

        #region AllCaps

        public static readonly BindableProperty AllCapsProperty = BindableProperty.Create(nameof(AllCaps),
            typeof(bool), typeof(CustomButton), true);

        public bool AllCaps
        {
            get => (bool)GetValue(AllCapsProperty);
            set => SetValue(AllCapsProperty, value);
        }

        #endregion

        #region IconHeight

        public int IconHeight
        {
            get => (int)GetValue(IconHeightProperty);
            set => SetValue(IconHeightProperty, value);
        }

        public static readonly BindableProperty IconHeightProperty =
            BindableProperty.Create(nameof(IconHeight), typeof(int), typeof(CustomButton), 40);

        #endregion

        #region IconWidth

        public int IconWidth
        {
            get => (int)GetValue(IconWidthProperty);
            set => SetValue(IconWidthProperty, value);
        }

        public static readonly BindableProperty IconWidthProperty =
            BindableProperty.Create(nameof(IconWidth), typeof(int), typeof(CustomButton), 40);

        #endregion

        #region IconSpacing

        public static readonly BindableProperty IconSpacingProperty =
            BindableProperty.Create(
                nameof(IconSpacing),
                typeof(int),
                typeof(CustomButton),
                0,
                BindingMode.TwoWay);

        public int IconSpacing
        {
            get => (int)GetValue(IconSpacingProperty);
            set => SetValue(IconSpacingProperty, value);
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using MyWallet.Enums;
using Xamarin.Forms;

namespace MyWallet.Controls
{
    public class CustomLabel : Label
    {
        public CustomLabel()
        {
            
        }

        #region Underline

        public static readonly BindableProperty UnderlineProperty = BindableProperty.Create(nameof(Underline),
            typeof(bool), typeof(CustomLabel), false);

        public bool Underline
        {
            get => (bool)GetValue(UnderlineProperty);
            set => SetValue(UnderlineProperty, value);
        }

        #endregion

        #region Icon

        public static readonly BindableProperty IconProperty =
            BindableProperty.Create(nameof(Icon), typeof(string), typeof(CustomLabel), string.Empty);

        public string Icon
        {
            get => (string)GetValue(IconProperty);
            set => SetValue(IconProperty, value);
        }

        #endregion

        #region IconHeight

        public int IconHeight
        {
            get => (int)GetValue(IconHeightProperty);
            set => SetValue(IconHeightProperty, value);
        }

        public static readonly BindableProperty IconHeightProperty =
            BindableProperty.Create(nameof(IconHeight), typeof(int), typeof(CustomLabel), 40);

        #endregion

        #region IconWidth

        public int IconWidth
        {
            get => (int)GetValue(IconWidthProperty);
            set => SetValue(IconWidthProperty, value);
        }

        public static readonly BindableProperty IconWidthProperty =
            BindableProperty.Create(nameof(IconWidth), typeof(int), typeof(CustomLabel), 40);

        #endregion

        #region AlignIcon

        public static readonly BindableProperty AlignIconProperty =
            BindableProperty.Create(
                nameof(AlignIcon),
                typeof(AlignIconType),
                typeof(CustomLabel),
                AlignIconType.Left,
                propertyChanged: (bindable, value, newValue) =>
                {
                    var control = (CustomLabel)bindable;
                    control.AlignIcon = (AlignIconType)newValue;
                });

        public AlignIconType AlignIcon
        {
            get => (AlignIconType)GetValue(AlignIconProperty);
            set => SetValue(AlignIconProperty, value);
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyWallet.Controls
{
    public class CustomPicker : Picker
    {
        public CustomPicker()
        {

        }

        #region Borderless

        public static readonly BindableProperty BorderlessProperty = BindableProperty.Create(nameof(Borderless),
            typeof(bool), typeof(CustomPicker), false);

        public bool Borderless
        {
            get => (bool)GetValue(BorderlessProperty);
            set => SetValue(BorderlessProperty, value);
        }

        #endregion

        #region SetFontSize

        public static readonly BindableProperty SetFontSizeProperty = BindableProperty.Create(nameof(SetFontSize),
            typeof(bool), typeof(CustomPicker), false);

        public bool SetFontSize
        {
            get => (bool)GetValue(SetFontSizeProperty);
            set => SetValue(SetFontSizeProperty, value);
        }

        #endregion
    }
}

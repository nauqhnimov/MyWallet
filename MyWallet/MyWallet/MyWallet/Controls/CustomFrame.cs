﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MyWallet.Controls
{
    public class CustomFrame : Frame
    {
        public CustomFrame()
        {
            base.Padding = new Size(20, 20);
        }

        
        #region BorderWidth

        public int BorderWidth
        {
            get => (int) base.GetValue(CustomFrame.BorderWidthProperty);
            set => base.SetValue(CustomFrame.BorderWidthProperty, value);
        }

        public readonly static BindableProperty BorderWidthProperty = 
            BindableProperty.Create("BorderWidth",
                typeof(int), 
                typeof(CustomFrame), 
                2, 
                BindingMode.OneWay, 
                null, null, null, null, null);

        #endregion

        #region BorderRadius

        public int BorderRadius
        {
            get => (int)base.GetValue(CustomFrame.BorderRadiusProperty);
            set => base.SetValue(CustomFrame.BorderRadiusProperty, value);
        }

        public readonly static BindableProperty BorderRadiusProperty = 
            BindableProperty.Create("BorderRadius",
                typeof(int), 
                typeof(CustomFrame), 
                5, 
                BindingMode.OneWay, 
                null, null, null, null, null);

        #endregion

        #region OutlineColor

        public readonly static BindableProperty OutlineColorProperty = 
            BindableProperty.Create("OutlineColor",
                typeof(Color), 
                typeof(CustomFrame), 
                Color.Default, 
                BindingMode.OneWay, 
                null, null, null, null, null);

        public Color OutlineColor
        {
            get => (Color)base.GetValue(CustomFrame.OutlineColorProperty);
            set => base.SetValue(CustomFrame.OutlineColorProperty, value);
        }

        #endregion

    }
}

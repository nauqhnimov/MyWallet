﻿using MyWallet.API.Models;

namespace MyWallet.API.Interfaces
{
    interface ITransactionService
    {
        object CreateTransaction(TransactionModel input);
        object GetListTransactions(string userName);
    }
}

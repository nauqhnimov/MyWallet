﻿using MyWallet.API.Models;

namespace MyWallet.API.Interfaces
{
    interface IUserServices
    {
        object CreateUser(UserModel input);
        object UpdateUser(UserModel input);
        object Login(LoginModel input);
    }
}

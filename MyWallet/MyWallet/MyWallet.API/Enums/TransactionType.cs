﻿namespace MyWallet.API.Enums
{
    public enum TransactionType
    {
        None = 0,
        Income = 1,
        Outcome = 2,
    }
}
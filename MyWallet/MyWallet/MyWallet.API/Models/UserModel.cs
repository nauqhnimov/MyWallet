﻿namespace MyWallet.API.Models
{
    public class UserModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public object Info { get; set; }
    }
}
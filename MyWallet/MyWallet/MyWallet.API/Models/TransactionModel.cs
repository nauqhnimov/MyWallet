﻿using System;
using MyWallet.API.Enums;

namespace MyWallet.API.Models
{
    public class TransactionModel
    {
        public int Id { get; set; }
        public string Amount { get; set; }
        public string ColorText { get; set; }
        public string Category { get; set; }
        public string Note { get; set; }

        public DateTime CreatedDate { get; set; }

        public byte[] DescriptionPhoto { get; set; }

        public TransactionType TransactionType { get; set; }// = (int)TransactionType.None;

        public string UserName { get; set; }
    }
}
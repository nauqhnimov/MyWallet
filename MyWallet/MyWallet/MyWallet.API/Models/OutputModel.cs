﻿namespace MyWallet.API.Models
{
    public class OutputModel
    {
        public bool Error { get; set; }
        public int ErrorCode { get; set; }
        public string Message { get; set; }
        public object Result { get; set; }
    }
}
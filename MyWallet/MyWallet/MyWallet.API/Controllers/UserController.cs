﻿using System.Web.Http;
using MyWallet.API.Interfaces;
using MyWallet.API.Models;
using MyWallet.API.Services;

namespace MyWallet.API.Controllers
{
    public class UserController : ApiController
    {
        private readonly IUserServices _userServices; 
        public UserController()
        {
            _userServices = new UserService();
        }

        [Route("api/SignUp")]
        [HttpPost]
        public IHttpActionResult CreateUser([FromBody]UserModel input)
        {
            var result = _userServices.CreateUser(input);
            return Ok(result);
        }

        [Route("api/User")]
        [HttpPut]
        public IHttpActionResult UpdateUser([FromBody]UserModel input)
        {
            var result = _userServices.UpdateUser(input);
            return Ok(result);
        }

        [Route("api/Login")]
        [HttpPost]
        public IHttpActionResult Login([FromBody]LoginModel input)
        {
            var result = _userServices.Login(input);
            return Ok(result);
        }
    }
}

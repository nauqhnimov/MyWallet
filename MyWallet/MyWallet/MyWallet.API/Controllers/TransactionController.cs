﻿using System.Web.Http;
using MyWallet.API.Interfaces;
using MyWallet.API.Models;
using MyWallet.API.Services;

namespace MyWallet.API.Controllers
{
    public class TransactionController : ApiController
    {
        private readonly ITransactionService _transactionService;
        public TransactionController()
        {
            _transactionService = new TransactionService();
        }

        [Route("api/PostTrans")]
        [HttpPost]
        public IHttpActionResult CreateTrans([FromBody]TransactionModel input)
        {
            var result = _transactionService.CreateTransaction(input);
            return Ok(result);
        }

        //  [HttpGet]
        public IHttpActionResult GetListTrans(string id)
        {
            var result = _transactionService.GetListTransactions(id);
            return Ok(result);
        }



    }
}

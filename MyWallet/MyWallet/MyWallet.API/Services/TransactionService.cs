﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using MyWallet.API.Interfaces;
using MyWallet.API.Models;
using Newtonsoft.Json;

namespace MyWallet.API.Services
{
    public class TransactionService : ITransactionService
    {
        private List<TransactionModel> _listTrans;
        private OutputModel _output;

        private string _filePath = HttpContext.Current.Request.MapPath("~/listTransactions.txt");

        public TransactionService()
        {
            _listTrans = new List<TransactionModel>();
            _output = new OutputModel();
        }

        private void ReadFileListTrans()
        {
            try
            {
                using (StreamReader sr = new StreamReader(_filePath))
                {
                    string json = sr.ReadToEnd();
                    _listTrans = JsonConvert.DeserializeObject<List<TransactionModel>>(json);
                    if (_listTrans == null)
                        _listTrans = new List<TransactionModel>();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void WriteFileListTrans()
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(_filePath))
                {
                    sw.Write(JsonConvert.SerializeObject(_listTrans).ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void SetOutput(bool error, int errorCode, string message, object result)
        {
            _output.Error = error;
            _output.ErrorCode = errorCode;
            _output.Message = message;
            _output.Result = result;
        }

        public object CreateTransaction(TransactionModel input)
        {
            ReadFileListTrans();
            if (_listTrans == null)
                _listTrans = new List<TransactionModel>();
            try
            {
                _listTrans.Add(input);
                SetOutput(
                    error: false,
                    errorCode: 200,
                    message: "",
                    result: input
                );
                WriteFileListTrans();
            }
            catch (Exception ex)
            {
                SetOutput(
                    error: true,
                    errorCode: ex.GetHashCode(),
                    message: ex.Message,
                    result: null
                );
            }
            return _output;
        }

        public object GetListTransactions(string userName)
        {
            ReadFileListTrans();
            List<object> trans = new List<object>();
            foreach (var t in _listTrans)
            {
                if (t.UserName == userName)
                    trans.Add(t);
            }
            if (trans.Any())
            {
                SetOutput(
                    error: false,
                    errorCode: 200,
                    message: "",
                    result: trans
                );
            }
            else
            {
                SetOutput(
                    error: true,
                    errorCode: 404,
                    message: "Not found transaction",
                    result: null);
            }
            return _output;
        }

    }
}
﻿using System;
using System.IO;
using MyWallet.Droid.Services.LocalDatabase;
using MyWallet.Interfaces.LocalDatabase;
using SQLite.Net;
using SQLite.Net.Platform.XamarinAndroid;
//using Environment = System.Environment;

[assembly: Xamarin.Forms.Dependency(typeof(DatabaseConnection))]
namespace MyWallet.Droid.Services.LocalDatabase
{
    public class DatabaseConnection : IDatabaseConnection
    {
        string GetPath(string databaseName)
        {
            //var sqliteFilename = $"{databaseName}.db3";
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var path = Path.Combine(documentsPath, databaseName);
            return path;
        }

        public string GetDatabasePath()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        }

        public SQLiteConnection DbConnection(string databaseName)
        {
            return new SQLiteConnection(new SQLitePlatformAndroid(), GetPath(databaseName));
        }

        public long GetSize(string databaseName)
        {
            var fileInfo = new FileInfo(GetPath(databaseName));
            return fileInfo.Length;
        }
    }
}
﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace MyWallet.Droid
{
    [Activity(Label = "MyWallet",
        Icon = "@drawable/wallet",
        Theme = "@style/MainTheme",
        MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.KeyboardHidden,
        ScreenOrientation = ScreenOrientation.Portrait, WindowSoftInputMode = SoftInput.AdjustResize)]

    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App());
        }

        //protected override void OnPostCreate(Bundle savedInstanceState)
        //{
        //    var toolBar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
        //    SetSupportActionBar(toolBar);
        //    base.OnPostCreate(savedInstanceState);

        //    Plugin.CurrentActivity.CrossCurrentActivity.Current.Init(this, savedInstanceState);
        //}

        #region Permissions
        /// <summary>
        /// Permission request
        /// </summary>
        /// <param name="requestCode"></param>
        /// <param name="permissions"></param>
        /// <param name="grantResults"></param>
        public override void OnRequestPermissionsResult(int requestCode,
            string[] permissions, Permission[] grantResults)
        {
            Plugin.Permissions.PermissionsImplementation
                .Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            //barcode scanner
            ZXing.Net.Mobile.Android.PermissionsHandler.OnRequestPermissionsResult(
                requestCode, permissions, grantResults);
        }
        #endregion

        //public override bool OnOptionsItemSelected(IMenuItem item)
        //{
        //    //if we are not hitting the internal "home" button, just return without any action
        //    if (item.ItemId != Android.Resource.Id.Home)
        //        return base.OnOptionsItemSelected(item);
        //    //this one triggers the hardware back button press handler - so we are back in XF without even mentioning it
        //    this.OnBackPressed();
        //    // return true to signal we have handled everything fine
        //    return true;
        //}
    }
}
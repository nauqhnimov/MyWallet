﻿

using System;
using System.ComponentModel;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;
using Android.Util;
using Android.Views;
using Java.Util;
using MyWallet.Controls;
using MyWallet.Droid.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace MyWallet.Droid.Controls
{
    public class CustomButtonRenderer : ButtonRenderer
    {
        public CustomButtonRenderer() : base()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                var element = (CustomButton)e.NewElement;

                #region icon

                var editText = this.Control;

                if (!string.IsNullOrEmpty(element.Image))
                {
                    editText.SetCompoundDrawablesWithIntrinsicBounds(
                        GetDrawable(imageButton: element.Image, imageHeight: element.IconHeight,
                            imageWidth: element.IconWidth), null,
                        null, null);

                    //editText.Gravity = GravityFlags.CenterVertical;
                    editText.Gravity = GravityFlags.Center;

                    editText.CompoundDrawablePadding = (int)(element.IconSpacing);

                }

                #endregion

                // Set text in ALL CAPS mode or not.
                this.Control.SetAllCaps(element.AllCaps);
                this.Control.SetPadding(0, 0, 0, 0);

            }

            if (e.OldElement != null)
            {

            }

        }


        private BitmapDrawable GetDrawable(string imageButton, int imageHeight, int imageWidth)
        {
            var resId = (int)typeof(Resource.Drawable)
                .GetField(imageButton.Replace(".jpg", "").Replace(".png", "")).GetValue(null);
            var drawable = ContextCompat.GetDrawable(this.Context, resId);
            var bitmap = ((BitmapDrawable)drawable).Bitmap;

            return new BitmapDrawable(Resources,
                Bitmap.CreateScaledBitmap(bitmap, (int)(imageWidth), (int)(imageHeight), true));
        }
    }
}
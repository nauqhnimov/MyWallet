﻿using Android.Views;
using MyWallet.Controls;
using MyWallet.Droid.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomListView), typeof(CustomListViewRenderer))]
namespace MyWallet.Droid.Controls
{
    public class CustomListViewRenderer : ListViewRenderer
    {
        public CustomListViewRenderer() : base()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                var element = (CustomListView)e.NewElement;

                #region ScrollEnabled

                Control.VerticalScrollBarEnabled = element.ScrollEnabled;

                #endregion


            }

            if (e.OldElement != null)
            {

            }

        }

        //private int _mPosition;

        //public override bool DispatchTouchEvent(MotionEvent e)
        //{
        //    if (e.ActionMasked == MotionEventActions.Down)
        //    {
        //        // Record the position the list the touch landed on
        //        _mPosition = this.Control.PointToPosition((int)e.GetX(), (int)e.GetY());
        //        return base.DispatchTouchEvent(e);
        //    }

        //    if (e.ActionMasked == MotionEventActions.Move)
        //    {
        //        // Ignore move eents
        //        return true;
        //    }

        //    if (e.ActionMasked == MotionEventActions.Up)
        //    {
        //        // Check if we are still within the same view
        //        if (this.Control.PointToPosition((int)e.GetX(), (int)e.GetY()) == _mPosition)
        //        {
        //            base.DispatchTouchEvent(e);
        //        }
        //        else
        //        {
        //            // Clear pressed state, cancel the action
        //            Pressed = false;
        //            Invalidate();
        //            return true;
        //        }
        //    }

        //    return base.DispatchTouchEvent(e);
        //}
    }

}

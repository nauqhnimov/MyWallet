﻿using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;
using Android.Views;
using MyWallet.Controls;
using MyWallet.Droid.Controls;
using MyWallet.Enums;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomLabel), typeof(CustomLabelRenderer))]
namespace MyWallet.Droid.Controls
{
    public class CustomLabelRenderer : LabelRenderer
    {
        private double _ratio;

        public CustomLabelRenderer() : base()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            // Ratio for image standard size
            _ratio = Resources.DisplayMetrics.HeightPixels / 960d;

            if (this.Control != null)
            {
                if (e.NewElement != null)
                {
                    var element = (CustomLabel)e.NewElement;

                    #region Underline

                    if (element.Underline)
                        this.Control.PaintFlags = Android.Graphics.PaintFlags.UnderlineText;

                    #endregion

                    #region icon

                    var editText = this.Control;

                    if (!string.IsNullOrEmpty(element.Icon))
                    {
                        switch (element.AlignIcon)
                        {
                            case AlignIconType.Left:
                                editText.SetCompoundDrawablesWithIntrinsicBounds(
                                    GetDrawable(element.Icon, element.IconHeight, imageWidth: element.IconWidth), null, null, null);
                                break;
                            case AlignIconType.Right:
                                editText.SetCompoundDrawablesWithIntrinsicBounds(
                                    null, null, GetDrawable(element.Icon, element.IconHeight, imageWidth: element.IconWidth), null);
                                break;
                        }

                        //test vertical
                        editText.Gravity = GravityFlags.CenterVertical;
                    }

                    #endregion

                }

                if (e.OldElement != null)
                {

                }

            }

        }

        private BitmapDrawable GetDrawable(string imageEntryImage, int imageHeight, int imageWidth)
        {
            var resId = (int)typeof(Resource.Drawable)
                .GetField(imageEntryImage.Replace(".jpg", "").Replace(".png", "")).GetValue(null);
            var drawable = ContextCompat.GetDrawable(this.Context, resId);
            var bitmap = ((BitmapDrawable)drawable).Bitmap;

            return new BitmapDrawable(Resources,
                Bitmap.CreateScaledBitmap(bitmap, (int)(imageWidth * 2 * _ratio), (int)(imageHeight * 2 * _ratio), true));
        }

    }
}
﻿using MyWallet.Controls;
using MyWallet.Droid.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]
namespace MyWallet.Droid.Controls
{
    public class CustomPickerRenderer : PickerRenderer
    {
        public CustomPickerRenderer() : base()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                var element = (CustomPicker)e.NewElement;

                #region BorderLess

                // Set background is borderless
                if (element.Borderless)
                    this.Control.Background = null;

                #endregion

                var layoutParams = new MarginLayoutParams(Control.LayoutParameters);
                layoutParams.SetMargins(0, 0, 0, 0);
                LayoutParameters = layoutParams;
                Control.LayoutParameters = layoutParams;
                Control.SetPadding(0, 0, 0, 0);
                SetPadding(0, 0, 0, 0);

                #region SetFontSize

                if (element.SetFontSize)
                    Control.TextSize *= 0.35f;

                #endregion

            }

            if (e.OldElement != null || e.NewElement != null)
            {
                //Control.TextSize *= 0.35f;
            }

            //if (this.Control != null)
            //{
            //    Control.TextSize = 30;
            //}


        }
    }
}
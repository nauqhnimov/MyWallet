﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Content;
using Android.Text;
using Android.Views;
using Android.Widget;
using MyWallet.Controls;
using MyWallet.Droid.Controls;
using MyWallet.Enums;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace MyWallet.Droid.Controls
{
    public class CustomEntryRenderer : EntryRenderer
    {
        private double _ratio;

        public CustomEntryRenderer() : base()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            // Ratio for image standard size
            _ratio = Resources.DisplayMetrics.HeightPixels / 960d;


            if (e.NewElement != null)
            {
                var element = (CustomEntry)e.NewElement;

                #region BorderLess

                // Set background is borderless
                if (element.Borderless)
                    this.Control.Background = null;

                #endregion

                #region AllCaps

                if (!(element.Keyboard == Keyboard.Numeric || element.Keyboard == Keyboard.Telephone || element.IsPassword))
                    // IsPassword = true will NOT work if use the function SetAllCaps
                    this.Control.SetAllCaps(element.AllCaps);

                #endregion

                #region icon

                var editText = this.Control;

                if (!string.IsNullOrEmpty(element.Icon))
                {
                    switch (element.AlignIcon)
                    {
                        case AlignIconType.Left:
                            editText.SetCompoundDrawablesWithIntrinsicBounds(
                                GetDrawable(element.Icon, element.IconHeight, imageWidth: element.IconWidth), null, null, null);
                            break;
                        case AlignIconType.Right:
                            editText.SetCompoundDrawablesWithIntrinsicBounds(
                                null, null, GetDrawable(element.Icon, element.IconHeight, imageWidth: element.IconWidth), null);
                            break;
                    }

                    //test vertical
                    editText.Gravity = GravityFlags.CenterVertical;
                }

                #endregion

                #region WhiteCursor

                if (element.WhiteCursor)
                {
                    // set the cursor color the same as the entry TextColor
                    var IntPtrtextViewClass = JNIEnv.FindClass(typeof(TextView));
                    var mCursorDrawableResProperty =
                        JNIEnv.GetFieldID(IntPtrtextViewClass, "mCursorDrawableRes", "I");
                    // replace 0 with a Resource.Drawable.my_cursor 
                    JNIEnv.SetField(Control.Handle, mCursorDrawableResProperty, 0);
                }

                #endregion

            }

            if (e.OldElement != null)
            {
                
            }

        }


        private BitmapDrawable GetDrawable(string imageEntryImage, int imageHeight, int imageWidth)
        {
            var resId = (int)typeof(Resource.Drawable)
                .GetField(imageEntryImage.Replace(".jpg", "").Replace(".png", "")).GetValue(null);
            var drawable = ContextCompat.GetDrawable(this.Context, resId);
            var bitmap = ((BitmapDrawable)drawable).Bitmap;

            return new BitmapDrawable(Resources,
                Bitmap.CreateScaledBitmap(bitmap, (int)(imageWidth * 2 * _ratio), (int)(imageHeight * 2 * _ratio), true));
        }
    }
}
﻿using MyWallet.Controls;
using MyWallet.Droid.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomDatePicker), typeof(CustomDatePickerRenderer))]
namespace MyWallet.Droid.Controls
{
    class CustomDatePickerRenderer : DatePickerRenderer
    {
        public CustomDatePickerRenderer() : base()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                var element = (CustomDatePicker)e.NewElement;

                #region BorderLess

                // Set background is borderless
                if (element.Borderless)
                    this.Control.Background = null;

                #endregion

                var layoutParams = new MarginLayoutParams(Control.LayoutParameters);
                layoutParams.SetMargins(0, 0, 0, 0);
                LayoutParameters = layoutParams;
                Control.LayoutParameters = layoutParams;
                Control.SetPadding(0, 0, 0, 0);
                SetPadding(0, 0, 0, 0);

                #region SetFontSize

                if (element.SetFontSize)
                    Control.TextSize *= 0.35f;

                #endregion

            }

            if (e.OldElement != null || e.NewElement != null)
            {
                //Control.TextSize *= 0.35f;
            }
        }
    }
}